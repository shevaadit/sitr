<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Frontpage\NewsController;
use App\Http\Controllers\Frontpage\HomeController;
use App\Http\Controllers\Frontpage\FaqController;
use App\Http\Controllers\Frontpage\ContactController;
use App\Http\Controllers\Frontpage\GalleryImageController;
use App\Http\Controllers\Frontpage\GalleryVideoController;

// Route::get('/test', function () {
//     App\Models\Geometry::whereDoesntHave('layerGroup')
//         ->delete();
// });

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/faq', [FaqController::class, 'index'])->name('faq');
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::get('/gallery-image', [GalleryImageController::class, 'index'])->name('gallery.image');
Route::get('/gallery-video', [GalleryVideoController::class, 'index'])->name('gallery.video');

Route::get('/library', function () {
    return view('library');
});
Route::get('/webgis', function () {
    return view('webgis');
});
Route::get('/kesesuaian-tata-ruang', function () {
    return view('suitability');
});
Route::get('/progress', function () {
    return view('progress');
});
Route::get('/indikator', function () {
    return view('indicator');
});

Route::get('/berita', [NewsController::class, 'index'])->name('news');
Route::get('/blog/{id}', [NewsController::class, 'show'])->name("news.detail");

Route::group(['namespace' => 'App\Http\Controllers'], function () {

    Route::get('/api/indicator', 'Frontpage\IndicatorController@index');
    Route::get('/api/indicator/chart-data', 'Frontpage\IndicatorController@getChartData');
    Route::get('/api/indicator/{key}/chart-data-by-key', 'Frontpage\IndicatorController@getChartDataByKey');

    Route::get('/api/library/filter', 'Frontpage\LibraryController@getFilter');
    Route::get('/api/library', 'Frontpage\LibraryController@index');

    Route::get('/api/progress/features/{x}/{y}', 'Frontpage\ProgressController@getFeature');

    Route::get('/api/progress-regionals/progress', 'Frontpage\ProgressRegionalController@progress');
    Route::get('/api/progress-regionals/region-by-status/{id}', 'Frontpage\ProgressRegionalController@getRegionByStatus')->where('id', '[0-9]+');
    Route::get('/api/progress-regionals/{id}/attributes', 'Frontpage\ProgressRegionalController@getAttributes')->where('id', '[0-9]+');
    Route::get('/api/progress-regionals/{id}/histories', 'Frontpage\ProgressRegionalController@getHistories')->where('id', '[0-9]+');
    Route::get('/api/progress-regionals', 'Frontpage\ProgressRegionalController@index');
    // Route::get('/api/progress-regionals', 'WebgisController@index');
    Route::get('/api/progress-detaileds/progress', 'Frontpage\ProgressDetailedController@progress');
    Route::get('/api/progress-detaileds', 'Frontpage\ProgressDetailedController@index');
    // Route::get('/api/progress-regionals', 'WebgisController@index');
    Route::get('/api/webgis', 'WebgisController@index');
    Route::get('/api/webgis/{id}/zonation-rules', 'WebgisController@getZonationRules');
    Route::get('/api/webgis/regencies', 'WebgisController@getRegencies');
    Route::get('/api/webgis/groups', 'WebgisController@getGroups');
    Route::get('/api/webgis/features/{x}/{y}', 'WebgisController@getFeature');
    Route::get('/api/webgis/{id}/raw-attributes', 'WebgisController@getRawAttributes');
    Route::get('/api/webgis/{id}/attributes', 'WebgisController@getAttributes');
    Route::get('/api/webgis/{id}/documents', 'WebgisController@getDocuments');


    Route::get('/api/suitability', 'SuitabilityController@index');
    Route::get('/api/suitability/srids', 'SuitabilityController@getSrid');
    Route::get('/api/suitability/rules', 'SuitabilityController@getRules');
    Route::get('/api/suitability/attributes', 'SuitabilityController@getAttributes');
    Route::post('/api/suitability/feature-by-area', 'SuitabilityController@getFeatureByArea');
    Route::get('/api/suitability/features/{x}/{y}', 'SuitabilityController@getFeature');
});

Route::group([
    'namespace' => 'App\Http\Controllers',
    'middleware' => ['auth', 'role:admin,superadmin']
], function () {

    Route::group(["middleware" => "auth"], function () {
        Route::get('/user/profiles', 'ProfileController@profile');
    });

    Route::get('/api/user/profiles', 'ProfileController@show');
    Route::post('/api/user/profiles', 'ProfileController@update');

    Route::get('/api/admin/progress-regionals/regencies', 'ProgressRegionalController@getRegencies');
    Route::resource('/api/admin/progress-regionals', 'ProgressRegionalController')->parameters(['progress-regionals' => 'progress']);

    Route::get('/api/admin/progress-submissions/{id}/fetch', 'ProgressSubmissionController@fetch')->where('id', '[0-9]+');
    Route::post('/api/admin/progress-submissions/{id}/insert', 'ProgressSubmissionController@insert')->where('id', '[0-9]+');
    Route::get('/api/admin/progress-submissions/{id}/add', 'ProgressSubmissionController@add')->where('id', '[0-9]+');
    Route::resource('/api/admin/progress-submissions', 'ProgressSubmissionController');

    Route::get('/api/admin/progress-submission-documents/{id}/fetch', 'ProgressSubmissionDocumentController@fetch')->where('id', '[0-9]+');
    Route::post('/api/admin/progress-submission-documents/{id}/insert', 'ProgressSubmissionDocumentController@insert')->where('id', '[0-9]+');

    
    Route::resource('/api/admin/progress-statuses', 'ProgressStatusController');

    Route::resource('/api/admin/regulations', 'RegulationController');

    Route::get('/api/admin/regulation-documents/{id}/fetch', 'RegulationDocumentController@fetch')->where('id', '[0-9]+');
    Route::post('/api/admin/regulation-documents/{id}/insert', 'RegulationDocumentController@insert')->where('id', '[0-9]+');
    Route::resource('/api/admin/regulation-documents', 'RegulationDocumentController');

    Route::get('/admin', function () {
        return view('admin');
    })->name('admin.page');

    Route::get('/admin/{any}', 'Admin1Controller@index')->where('any', '.*');

    Route::get('/gee', function () {
        return view('gee');
    });
    Route::get('/api/earth-engine/get-land-cover-url', 'EarthEngineController@getLandCoverUrl');
    Route::get('/api/earth-engine/download-land-cover', 'EarthEngineController@downloadLandCover');
    Route::get('/api/earth-engine/get-url', 'EarthEngineController@getUrl');
    Route::get('/api/earth-engine/download', 'EarthEngineController@download');

});
Route::group([
    'namespace' => 'App\Http\Controllers',
    'middleware' => ['auth', 'role:superadmin']
], function () {

    Route::resource('/api/admin/earth-engine-tasks', 'EarthEngineTaskController')->only([
        'index', 'show'
    ]);
    Route::resource('/api/admin/libraries', 'LibraryController')->parameters(['libraries' => 'document']);

    // Route::post('/api/admin/posts/upload', 'PostController@upload')->name("post.upload");
    Route::resource('/api/admin/posts', 'PostController', ['names' => 'post']);
    Route::resource('/api/admin/partners', 'PartnerController', ['names' => 'partner']);

    Route::resource('/api/admin/progress-submission-documents', 'ProgressSubmissionDocumentController');

    Route::get('/api/admin/progress-approvals', 'ProgressApprovalController@index');
    Route::post('/api/admin/progress-approvals/{id}/approve', 'ProgressApprovalController@approve')->where('id', '[0-9]+');
    Route::post('/api/admin/progress-approvals/{id}/refuse', 'ProgressApprovalController@refuse')->where('id', '[0-9]+');

    Route::get('/api/admin/progress/regions', 'ProgressController@getRegions');
    Route::get('/api/admin/progress/regencies', 'ProgressController@getRegencies');
    Route::resource('/api/admin/progress', 'ProgressController');

    Route::get('/api/admin/progress-regional-approvals', 'ProgressRegionalApprovalController@index');
    Route::post('/api/admin/progress-regional-approvals/{id}/approve', 'ProgressRegionalApprovalController@approve')->where('id', '[0-9]+');
    Route::post('/api/admin/progress-regional-approvals/{id}/refuse', 'ProgressRegionalApprovalController@refuse')->where('id', '[0-9]+');

    Route::get('/api/admin/progress-detailed-approvals', 'ProgressDetailedApprovalController@index');
    Route::post('/api/admin/progress-detailed-approvals/{id}/approve', 'ProgressDetailedApprovalController@approve')->where('id', '[0-9]+');
    Route::post('/api/admin/progress-detailed-approvals/{id}/refuse', 'ProgressDetailedApprovalController@refuse')->where('id', '[0-9]+');

    Route::get('/api/admin/progress-detaileds/regencies', 'ProgressDetailedController@getRegencies');
    Route::get('/api/admin/progress-detaileds/districts', 'ProgressDetailedController@getDistricts');
    Route::resource('/api/admin/progress-detaileds', 'ProgressDetailedController')->parameters(['progress-detaileds' => 'progress']);

    Route::post('/api/admin/layers/upload', 'LayerController@upload')->name("layer.upload");
    Route::get('/api/admin/layers/{id}/geojson', 'LayerController@geojson')->where('id', '[0-9]+');
    Route::get('/api/admin/layers/{id}/shapefile', 'LayerController@getShapefile')->where('id', '[0-9]+');
    Route::get('/api/admin/layers/srids', 'LayerController@getSrid');
    Route::get('/api/admin/layers/regions', 'LayerController@getRegions');
    Route::resource('/api/admin/layers', 'LayerController');

    Route::resource('/api/admin/layer-categories', 'LayerCategoryController');

    Route::get('/api/admin/indicator-configs/{id}/excel', 'IndicatorConfigController@excel')->where('id', '[0-9]+');
    Route::resource('/api/admin/indicator-configs', 'IndicatorConfigController');

    Route::get('/api/admin/indicator-config-details/{id}/fetch', 'IndicatorConfigDetailController@fetch');
    Route::post('/api/admin/indicator-config-details/{id}/insert', 'IndicatorConfigDetailController@insert');
    Route::resource('/api/admin/indicator-config-details', 'IndicatorConfigDetailController');

    Route::resource('/api/admin/indicator-documents', 'IndicatorDocumentController');

    Route::get('/api/admin/indicators/{id}/fetch', 'IndicatorController@fetch')->where('id', '[0-9]+');
    Route::resource('/api/admin/indicators', 'IndicatorController');


    Route::post('/api/admin/users/{id}/upload', 'UserController@upload')->where('id', '[0-9]+');
    Route::post('/api/admin/users/{id}/change-password', 'UserController@changePassword')->where('id', '[0-9]+');
    Route::resource('/api/admin/users', 'UserController');

    Route::post('/api/admin/zonation-rules/{id}/insert', 'ZonationRuleController@insert')->where('id', '[0-9]+');
    Route::get('/api/admin/zonation-rules/{id}/fetch', 'ZonationRuleController@fetch')->where('id', '[0-9]+');
    Route::resource('/api/admin/zonation-rules', 'ZonationRuleController');

    Route::post('/api/admin/zonation-rule-activities/{id}/insert', 'ZonationRuleActivityController@insert')->where('id', '[0-9]+');
    Route::get('/api/admin/zonation-rule-activities/{id}/fetch', 'ZonationRuleActivityController@fetch')->where('id', '[0-9]+');
    Route::resource('/api/admin/zonation-rule-activities', 'ZonationRuleActivityController');

    Route::resource('/api/admin/layer-groups', 'LayerGroupController');

    Route::get('/api/admin/layer-attributes/{id}/fetch', 'LayerAttributeController@fetch');
    Route::post('/api/admin/layer-attributes/{id}/insert', 'LayerAttributeController@insert');
    Route::resource('/api/admin/layer-attributes', 'LayerAttributeController');

    Route::get('/api/admin/layer-legends/{id}/fetch', 'LayerLegendController@fetch');
    Route::post('/api/admin/layer-legends/{id}/insert', 'LayerLegendController@insert');
    Route::resource('/api/admin/layer-legends', 'LayerLegendController');

    Route::get('/api/admin/geometries/{id}/geojson', 'GeometryController@geojson');
    Route::resource('/api/admin/geometries', 'GeometryController');


    Route::post('/api/admin/geometry-attributes/{id}/insert', 'GeometryAttributeController@insert');
    Route::get('/api/admin/geometry-attributes/{id}/fetch', 'GeometryAttributeController@fetch');
    Route::resource('/api/admin/geometry-attributes', 'GeometryAttributeController');

    Route::resource('/api/admin/regencies', 'RegencyController');

    Route::resource('/api/admin/faqs', 'FaqController', ['names' => 'faq']);

    Route::get('/api/admin/disclaimer', 'DisclaimerController@show');
    Route::put('/api/admin/disclaimer', 'DisclaimerController@update');

    Route::get('/api/admin/disclaimer-webgis', 'DisclaimerWebgisController@show');
    Route::put('/api/admin/disclaimer-webgis', 'DisclaimerWebgisController@update');

    Route::get('/api/admin/setting-contact', 'Settings\ContactController@read');
    Route::post('/api/admin/setting-contact', 'Settings\ContactController@alter');

    Route::get('/api/admin/setting-contact-page', 'Settings\ContactPageController@read');
    Route::post('/api/admin/setting-contact-page', 'Settings\ContactPageController@alter');

    Route::get('/api/admin/setting-live-chat', 'Settings\LiveChatController@read');
    Route::post('/api/admin/setting-live-chat', 'Settings\LiveChatController@alter');

    Route::get('/api/admin/setting-apps', 'Settings\AppsController@read');
    Route::post('/api/admin/setting-apps', 'Settings\AppsController@alter');


    Route::resource('/api/admin/slides', 'SlideController');
    Route::resource('/api/admin/galleries', 'GalleryController');
});
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
