<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('/register', [Api\AuthController::class, 'register']);

    Route::post('/login', [Api\AuthController::class, 'login']);

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('/user', function (Request $request) {
            $user = $request->user();
            $user->load('region');
            return compact('user');
        });

        // Route::post('/change-password', [Api\ChangePasswordController::class, 'update']);
        
        Route::post('/logout', function (Request $request) {
            return [
                'user' => $request->user()->currentAccessToken()->delete(),
            ];
        });
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/posts', Api\PostController::class)->only([
    'index',
    'show',
]);
Route::get('/slides', [Api\SlideController::class, 'index']);
Route::get('/partners', [Api\PartnerController::class, 'index']);
Route::get('/contact', [Api\ContactController::class, 'index']);


Route::get('/documents/filter', [Api\LibraryController::class, 'getFilter']);
Route::get('/documents', [Api\LibraryController::class, 'index']);

Route::get('/spatial-infos/feature/{x}/{y}', [Api\SpatialInfoController::class, 'getFeature']);
Route::get('/spatial-infos/info/{id}', [Api\SpatialInfoController::class, 'getInfo']);
Route::get('/spatial-infos', [Api\SpatialInfoController::class, 'index']);

Route::get('/suitabilities/feature/{x}/{y}', [Api\SuitabilityController::class, 'getFeature']);
Route::get('/suitabilities/info', [Api\SuitabilityController::class, 'getInfo']);
Route::get('/suitabilities', [Api\SuitabilityController::class, 'index']);

Route::get('/progresses/feature/{x}/{y}', [Api\ProgressController::class, 'getFeature']);
Route::get('/progresses/info/{id}', [Api\ProgressController::class, 'getInfo']);
Route::get('/progresses', [Api\ProgressController::class, 'index']);