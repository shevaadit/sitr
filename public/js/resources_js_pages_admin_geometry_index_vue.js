"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_pages_admin_geometry_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mixins_master__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../mixins/master */ "./resources/js/mixins/master.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mixins: [_mixins_master__WEBPACK_IMPORTED_MODULE_0__.default],
  data: function data() {
    return {
      currentPage: 1,
      totalRows: 1,
      loadingData: true,
      tableItems: [],
      tableFields: [// A virtual column that doesn't exist in items
      {
        key: 'index',
        label: 'No',
        width: '40px'
      }, // A column that needs custom formatting
      {
        key: 'id',
        label: 'ID',
        width: '60px'
      }, // A regular column
      {
        key: 'layer_id',
        label: 'Layer Id'
      }, {
        key: 'name',
        label: 'Nama'
      }, {
        key: 'layer_group',
        label: 'Layer Group'
      }, {
        key: 'action',
        label: 'Aksi',
        width: '200px',
        "class": 'text-center'
      }],
      moreParams: {}
    };
  },
  created: function created() {
    this.loadData();
  },
  methods: {
    renderIcon: function renderIcon(classes, options) {
      return "<span class=\"".concat(classes.join(' '), "\"></span>");
    },
    getParams: function getParams() {
      return {
        params: {
          page: this.currentPage
        }
      };
    },
    handlePageChange: function handlePageChange(val) {
      this.currentPage = val;
      this.loadData();
    },
    loadData: function loadData() {
      var _this = this;

      // fetch data dari api menggunakan axios
      this.loadingData = true;
      axios.get("/api/admin/geometries", this.getParams()).then(function (response) {
        _this.loadingData = false;
        _this.tableItems = [];
        _this.tableItems = response.data.data;
        _this.currentPage = response.data.current_page;
        _this.totalRows = response.data.total;
      });
    },
    deleteData: function deleteData(id) {
      var _this2 = this;

      // delete data
      this.loadingData = true;
      axios["delete"]("/api/admin/geometries/".concat(id)).then(function (response) {
        _this2.loadData();
      })["catch"](function (response) {
        _this2.showFailedAlert();
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/mixins/master.js":
/*!***************************************!*\
  !*** ./resources/js/mixins/master.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  methods: {
    parseObjectToFormData: function parseObjectToFormData(object) {
      var allowUnderscoreKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return Object.keys(object).reduce(function (formData, key) {
        var isUnderscoreKey = key.startsWith('_');

        if (allowUnderscoreKey && isUnderscoreKey) {
          formData.append(key, object[key]);
        }

        if (!isUnderscoreKey) {
          formData.append(key, object[key]);
        }

        return formData;
      }, new FormData());
    },
    assignPropertyToObject: function assignPropertyToObject(assigner, object) {
      var ignoreUnderscoreKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

      if (assigner instanceof Object) {
        Object.keys(assigner).filter(function (key) {
          return key in object;
        }).forEach(function (key) {
          object[key] = assigner[key];
        });
      }
    },
    addUploadedMultiFile: function addUploadedMultiFile(files, multiFile) {
      if (multiFile.length) {
        files.length = 0;
        multiFile.forEach(function (file) {
          files.push({
            source: file,
            options: {
              type: 'local'
            }
          });
        });
      }
    },
    addUploadedFile: function addUploadedFile(files, file) {
      if (file) {
        files.length = 0;
        files.push({
          source: file,
          options: {
            type: 'local'
          }
        });
      }
    },
    getUploadableFile: function getUploadableFile(files) {
      if (Array.isArray(files) && files.length) {
        var file = files[0];

        if (file.serverId === null) {
          return file.file;
        }
      }

      return null;
    },
    showLoadingAlert: function showLoadingAlert() {
      var _this = this;

      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Loading ...';
      this.$swal({
        icon: 'info',
        iconHtml: '<i class="fas fa-spinner fa-spin"></i>',
        title: title,
        // timerProgressBar: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function didOpen() {
          _this.$swal.showLoading();
        }
      });
    },
    showSuccessAlert: function showSuccessAlert() {
      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.$swal({
        icon: 'success',
        title: title,
        showConfirmButton: false,
        timer: 1500
      });
    },
    showFailedAlert: function showFailedAlert() {
      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.$swal({
        icon: 'error',
        title: title,
        showConfirmButton: false,
        timer: 1500
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.pagination {\n  margin-top: 0;\n}\n/* .btn.btn-border {\n  border: 1px solid;\n  margin-right: 2px;\n} */\n.vuetable-pagination-info {\n  margin-top: 8px !important;\n}\nspan.sort-icon {\n  float: right;\n  color: #ff9100;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/pages/admin/geometry/index.vue":
/*!*****************************************************!*\
  !*** ./resources/js/pages/admin/geometry/index.vue ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=1ccadbc4& */ "./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/admin/geometry/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=style&index=0&lang=css&");


/***/ }),

/***/ "./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4&":
/*!************************************************************************************!*\
  !*** ./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4& ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccadbc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=1ccadbc4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/geometry/index.vue?vue&type=template&id=1ccadbc4& ***!
  \***************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "b-card",
        {
          attrs: { "no-body": "" },
          scopedSlots: _vm._u([
            {
              key: "header",
              fn: function() {
                return [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-6 col-md-10" }, [
                      _c("h3", [
                        _vm.$route.meta && _vm.$route.meta.icon
                          ? _c("i", { class: _vm.$route.meta.icon + " mr-1" })
                          : _vm._e(),
                        _vm._v(_vm._s(_vm.$route.meta.title))
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-6 col-md-2" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "btn btn-primary btn-sm float-right",
                            attrs: { to: { name: "admin_geometry_create" } }
                          },
                          [
                            _c("i", { staticClass: "fa fa-plus" }),
                            _vm._v(" Tambah")
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _vm._v(" "),
          _c(
            "b-card-body",
            { staticClass: "p-0 p-md-3" },
            [
              _c("b-table", {
                attrs: {
                  bordered: "",
                  striped: "",
                  hover: "",
                  fields: _vm.tableFields,
                  items: _vm.tableItems,
                  busy: _vm.loadingData,
                  responsive: "sm"
                },
                scopedSlots: _vm._u([
                  {
                    key: "table-busy",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticClass: "text-center text-danger my-2" },
                          [
                            _c("b-spinner", { staticClass: "align-middle" }),
                            _vm._v(" "),
                            _c("strong", [_vm._v("  Loading...")])
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  },
                  {
                    key: "table-colgroup",
                    fn: function(scope) {
                      return _vm._l(scope.fields, function(field) {
                        return _c("col", {
                          key: field.key,
                          style: {
                            width:
                              field != "index" && field.width
                                ? field.width
                                : "auto"
                          }
                        })
                      })
                    }
                  },
                  {
                    key: "cell(index)",
                    fn: function(data) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(
                              data.index + (_vm.currentPage - 1) * 10 + 1
                            ) +
                            "\n        "
                        )
                      ]
                    }
                  },
                  {
                    key: "cell(name)",
                    fn: function(data) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(data.item.categorable.name) +
                            "\n        "
                        )
                      ]
                    }
                  },
                  {
                    key: "cell(layer_group)",
                    fn: function(data) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(
                              data.item.layer_group
                                ? data.item.layer_group.name
                                : ""
                            ) +
                            "\n        "
                        )
                      ]
                    }
                  },
                  {
                    key: "cell(action)",
                    fn: function(data) {
                      return [
                        _c(
                          "router-link",
                          {
                            staticClass: "btn btn-info btn-sm",
                            attrs: {
                              tag: "button",
                              to: {
                                name: "admin_geometry_show",
                                params: { id: data.item.id }
                              }
                            }
                          },
                          [
                            _c("span", { staticClass: "fa fa-eye" }),
                            _vm._v(" View")
                          ]
                        ),
                        _vm._v("  \n          "),
                        _c(
                          "router-link",
                          {
                            staticClass: "btn btn-warning btn-sm",
                            attrs: {
                              tag: "button",
                              to: {
                                name: "admin_geometry_edit",
                                params: { id: data.item.id }
                              }
                            }
                          },
                          [
                            _c("span", { staticClass: "fa fa-edit" }),
                            _vm._v(" Edit")
                          ]
                        ),
                        _vm._v("  \n          "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-danger btn-sm",
                            on: {
                              click: function($event) {
                                return _vm.deleteData(data.item.id)
                              }
                            }
                          },
                          [
                            _c("span", { staticClass: "fa fa-trash" }),
                            _vm._v(" Delete")
                          ]
                        ),
                        _vm._v("  \n        ")
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("b-pagination", {
                attrs: { "total-rows": _vm.totalRows, "per-page": 10 },
                on: { change: _vm.handlePageChange },
                model: {
                  value: _vm.currentPage,
                  callback: function($$v) {
                    _vm.currentPage = $$v
                  },
                  expression: "currentPage"
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);