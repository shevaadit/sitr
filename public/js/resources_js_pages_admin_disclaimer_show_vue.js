"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_pages_admin_disclaimer_show_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mixins_master__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../mixins/master */ "./resources/js/mixins/master.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mixins: [_mixins_master__WEBPACK_IMPORTED_MODULE_0__.default],
  data: function data() {
    return {
      formData: {
        content: ''
      }
    };
  },
  methods: {
    loadData: function loadData() {
      var _this = this;

      // fetch data dari api menggunakan axios
      this.loadingData = true;
      this.showLoadingAlert('Loading ...');
      axios.get("/api/admin/disclaimer").then(function (response) {
        _this.formData = response.data;

        _this.showSuccessAlert();
      });
    },
    resetForm: function resetForm() {
      this.loadData();
    },
    onSubmit: function onSubmit() {
      var _this2 = this;

      // this.$refs.baseform.$v.formData.$touch()
      // if (this.$refs.baseform.$v.formData.$anyError) {
      //   return;
      // }
      this.showLoadingAlert('Saving ...');
      axios.request({
        method: 'put',
        url: "/api/admin/disclaimer",
        params: {
          value: this.formData
        }
      }).then(function (response) {
        _this2.showSuccessAlert(response.data.message);
      })["catch"](function (error) {
        _this2.showFailedAlert(error.response.data.message);
      });
    }
  },
  mounted: function mounted() {
    this.loadData();
  }
});

/***/ }),

/***/ "./resources/js/mixins/master.js":
/*!***************************************!*\
  !*** ./resources/js/mixins/master.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  methods: {
    parseObjectToFormData: function parseObjectToFormData(object) {
      var allowUnderscoreKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return Object.keys(object).reduce(function (formData, key) {
        var isUnderscoreKey = key.startsWith('_');

        if (allowUnderscoreKey && isUnderscoreKey) {
          formData.append(key, object[key]);
        }

        if (!isUnderscoreKey) {
          formData.append(key, object[key]);
        }

        return formData;
      }, new FormData());
    },
    assignPropertyToObject: function assignPropertyToObject(assigner, object) {
      var ignoreUnderscoreKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

      if (assigner instanceof Object) {
        Object.keys(assigner).filter(function (key) {
          return key in object;
        }).forEach(function (key) {
          object[key] = assigner[key];
        });
      }
    },
    addUploadedMultiFile: function addUploadedMultiFile(files, multiFile) {
      if (multiFile.length) {
        files.length = 0;
        multiFile.forEach(function (file) {
          files.push({
            source: file,
            options: {
              type: 'local'
            }
          });
        });
      }
    },
    addUploadedFile: function addUploadedFile(files, file) {
      if (file) {
        files.length = 0;
        files.push({
          source: file,
          options: {
            type: 'local'
          }
        });
      }
    },
    getUploadableFile: function getUploadableFile(files) {
      if (Array.isArray(files) && files.length) {
        var file = files[0];

        if (file.serverId === null) {
          return file.file;
        }
      }

      return null;
    },
    showLoadingAlert: function showLoadingAlert() {
      var _this = this;

      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Loading ...';
      this.$swal({
        icon: 'info',
        iconHtml: '<i class="fas fa-spinner fa-spin"></i>',
        title: title,
        // timerProgressBar: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function didOpen() {
          _this.$swal.showLoading();
        }
      });
    },
    showSuccessAlert: function showSuccessAlert() {
      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.$swal({
        icon: 'success',
        title: title,
        showConfirmButton: false,
        timer: 1500
      });
    },
    showFailedAlert: function showFailedAlert() {
      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.$swal({
        icon: 'error',
        title: title,
        showConfirmButton: false,
        timer: 1500
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/pages/admin/disclaimer/show.vue":
/*!******************************************************!*\
  !*** ./resources/js/pages/admin/disclaimer/show.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show.vue?vue&type=template&id=7bf7ea18& */ "./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18&");
/* harmony import */ var _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show.vue?vue&type=script&lang=js& */ "./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__.render,
  _show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/admin/disclaimer/show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18&":
/*!*************************************************************************************!*\
  !*** ./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18& ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_7bf7ea18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=template&id=7bf7ea18& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/pages/admin/disclaimer/show.vue?vue&type=template&id=7bf7ea18& ***!
  \****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "b-card",
        {
          scopedSlots: _vm._u([
            {
              key: "header",
              fn: function() {
                return [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-6 col-md-9" }, [
                      _c("h3", [
                        _vm.$route.meta && _vm.$route.meta.icon
                          ? _c("i", { class: _vm.$route.meta.icon + " mr-1" })
                          : _vm._e(),
                        _vm._v(_vm._s(_vm.$route.meta.title))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-12 col-md-3" }, [
                      _c(
                        "div",
                        { staticClass: " float-right" },
                        [
                          _c(
                            "b-button",
                            {
                              attrs: {
                                size: "sm",
                                type: "submit",
                                variant: "success"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.onSubmit()
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fa fa-save mx-1" }),
                              _vm._v("Simpan")
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mx-2",
                              attrs: { size: "sm", variant: "info" },
                              on: {
                                click: function($event) {
                                  return _vm.resetForm()
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fa fa-sync mx-1" }),
                              _vm._v("Segarkan")
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ])
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "b-form-group",
                { attrs: { label: "Content", "label-for": "example-input-3" } },
                [
                  _c("b-form-input", {
                    staticClass: "d-none",
                    attrs: { id: "example-input-2", name: "example-input-2" },
                    model: {
                      value: _vm.formData.content,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "content", $$v)
                      },
                      expression: "formData.content"
                    }
                  }),
                  _vm._v(" "),
                  _c("quill-editor", {
                    model: {
                      value: _vm.formData.content,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "content", $$v)
                      },
                      expression: "formData.content"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "b-form-invalid-feedback",
                    { attrs: { id: "input-2-live-feedback" } },
                    [_vm._v("This is a required field.")]
                  )
                ],
                1
              )
            ],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);