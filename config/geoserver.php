<?php

return [

    'username' => env('GEOSERVER_USERNAME'),

    'password' => env('GEOSERVER_PASSWORD'),

    'host' => env('GEOSERVER_HOST', 'http://localhost:8080'),

    'rest' => env('GEOSERVER_REST', 'http://localhost:8080'),

    'path' => env('GEOSERVER_PATH', 'geoserver'),

    'workspace' => env('GEOSERVER_WORKSPACE', 'castella'),

    'datastore' => env('GEOSERVER_DATASTORE', 'castella_db'),

];
