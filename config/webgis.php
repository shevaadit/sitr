<?php

return [

    'srid' => env('WEBGIS_SRID', '4326'),
    'srs' => env('WEBGIS_SRS', 'EPSG:4326')
];
