const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

 const path = require('path');
 mix.alias({
     '@': path.join(__dirname, 'resources/js'),
     '_c': path.join(__dirname, 'resources/js/components'),
     '_p': path.join(__dirname, 'resources/js/pages')
 })
 mix.babelConfig({
    // presets: ['babel-preset-env'],
    plugins: ['@babel/plugin-syntax-dynamic-import'],
  });
mix.js('resources/js/admin.js', 'public/js').vue()
    .js('resources/js/webgis.js', 'public/js').vue()
    .js('resources/js/suitability.js', 'public/js').vue()
    .js('resources/js/progress.js', 'public/js').vue()
    .js('resources/js/gee.js', 'public/js').vue()
    .js('resources/js/library.js', 'public/js').vue()
    .js('resources/js/indicator.js', 'public/js').vue()
    .sass('resources/css/base.scss', 'public/css', [
        //
    ]);
    
mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);
