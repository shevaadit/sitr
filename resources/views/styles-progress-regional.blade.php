{!! '<'.'?xml version="1.0"?>' !!}
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:se="http://www.opengis.net/se" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <se:Name>{{ $name }}</se:Name>
    <UserStyle>
      <se:Name>{{ $name }}</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>default_polygon</se:Name>
          <se:Description>
            <se:Title>Default Polygon</se:Title>
          </se:Description>
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">1</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        @foreach ($statuses as $item)
          <se:Rule>
            <se:Name>{{ $item->id }}</se:Name>
            <se:Description>
              <se:Title>{{ $item->name }}</se:Title>
            </se:Description>
            <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>last_progress_status_id</ogc:PropertyName>
                <ogc:Literal>{{ $item->score }}</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:Filter>
            <se:PolygonSymbolizer>
              <se:Fill>
                <se:SvgParameter name="fill">{{ $item->color }}</se:SvgParameter>
              </se:Fill>
              <se:Stroke>
                <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                <se:SvgParameter name="stroke-width">1</se:SvgParameter>
              </se:Stroke>
            </se:PolygonSymbolizer>
          </se:Rule>
        @endforeach
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
