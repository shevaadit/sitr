@extends('layouts.frontpage')
@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            fontFamily: {
              sans: ['Inter', 'sans-serif'],
            },
          }
        }
      }
    </script> --}}
@endsection
@section('nav')
    <div class=" bg-gray-900">
    @section('logo')
        <img src="/images/logo.png" class="ml-3" alt="" style="width: 100px">
    @endsection
    @include('front-nav')
</div>
@endsection
@section('content')
<section class="overflow-hidden text-gray-700 ">
    <div class="container px-5 py-2 mx-auto lg:pt-12 lg:px-24">
        {{-- @foreach ($multiGalleries as $gallery)
            <div class=" rounded-lg bg-gray-100 p-4 mb-6">
                <div class="text-center mb-2">
                    <h2>{{ $gallery->title }}</h2>
                </div>
                <div class="flex flex-wrap -m-1 md:-m-2 justify-center">
                    @foreach ($gallery->multi_file_url as $key => $url)
                        <div data-micromodal-trigger="modal-{{ $gallery->id . '-' . $key }}"
                            class="flex flex-wrap w-full lg:w-1/3">
                            <div class="w-full p-1 md:p-2">
                                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg"
                                    src="{{ $url }}" />
                            </div>
                        </div>
                        <div id="modal-{{ $gallery->id . '-' . $key }}" class="modal micromodal-slide" id="modal-1"
                            aria-hidden="true">
                            <div class="modal__overlay" tabindex="-1" data-micromodal-close>
                                <div class="modal__container" role="dialog" aria-modal="true"
                                    aria-labelledby="modal-1-title">
                                    <div class="w-full p-1 md:p-2">
                                        <img alt="gallery"
                                            class="block object-cover object-center w-full h-full rounded-lg"
                                            src="{{ $url }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach --}}
        <div class=" rounded-lg bg-gray-100 p-4">
            <div class="flex flex-wrap -m-1 md:-m-2">
                @foreach ($galleries as $gallery)
                    <div class="flex flex-wrap w-auto lg:w-1/3 justify-center"
                        data-micromodal-trigger="modal-{{ $gallery->id }}">
                        <div class="w-full p-1 md:p-2">
                            @if ($gallery->is_link)
                                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg"
                                    src="{{ $gallery->url }}">
                            @else
                                <img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg"
                                    src="{{ $gallery->file_url }}">
                            @endif
                        </div>
                        <div class="px-4">
                            <h3>{{ $gallery->title }}</h3>
                        </div>
                    </div>

                    <div id="modal-{{ $gallery->id }}" class="modal micromodal-slide" id="modal-1"
                        aria-hidden="true">
                        <div class="modal__overlay" tabindex="-1" data-micromodal-close>
                            <div class="modal__container w-auto lg:w-9/12" role="dialog" aria-modal="true"
                                aria-labelledby="modal-1-title">
                                <div class="w-full p-1 md:p-2">
                                    @if ($gallery->multi_file_url)
                                        <div class="flex flex-wrap -m-1 md:-m-2 justify-center">
                                            @foreach ($gallery->multi_file_url as $key => $url)
                                                <div data-micromodal-trigger="modal-{{ $gallery->id . '-' . $key }}"
                                                    class="flex flex-wrap w-full lg:w-1/2">
                                                    <div class="w-full p-1 md:p-2">
                                                        <img alt="gallery"
                                                            class="block object-cover object-center w-full h-full rounded-lg"
                                                            src="{{ $url }}" />
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        @if ($gallery->is_link)
                                            <img alt="gallery"
                                                class="block object-cover object-center w-full h-full rounded-lg"
                                                src="{{ $gallery->url }}">
                                        @else
                                            <img alt="gallery"
                                                class="block object-cover object-center w-full h-full rounded-lg"
                                                src="{{ $gallery->file_url }}">
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js"></script>
<script>
    /**
     * Dropdown menu
     */
    document.addEventListener('DOMContentLoaded', function() {
        const menus = document.querySelectorAll('.navbar-burger');
        const dropdowns = document.querySelectorAll('.navbar-menu');

        if (menus.length && dropdowns.length) {
            for (var i = 0; i < menus.length; i++) {
                menus[i].addEventListener('click', function() {
                    for (var j = 0; j < dropdowns.length; j++) {
                        dropdowns[j].classList.toggle('hidden');
                    }
                });
            }
        }
    });
    MicroModal.init();
</script>
@endsection
