{!! '<'.'?xml version="1.0"?>' !!}
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:se="http://www.opengis.net/se" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <se:Name>{{ $config->getStyleName() }}</se:Name>
    <UserStyle>
      <se:Name>{{ $config->getStyleName() }}</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>default_polygon</se:Name>
          <se:Description>
            <se:Title>Default Polygon</se:Title>
          </se:Description>
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">1</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        @foreach ($ranges as $key => $item)
          <se:Rule>
            <se:Name>{{ $key }}</se:Name>
            <se:Description>
              <se:Title>{{ $key }}</se:Title>
            </se:Description>
            <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
              <ogc:PropertyIsBetween>
                <ogc:PropertyName>{{ $config->key }}</ogc:PropertyName>
                <ogc:LowerBoundary>
                  <ogc:Literal>{{ $item['lower'] }}</ogc:Literal>
                </ogc:LowerBoundary>
                <ogc:UpperBoundary>
                  <ogc:Literal>{{ $item['upper'] }}</ogc:Literal>
                </ogc:UpperBoundary>
              </ogc:PropertyIsBetween>
            </ogc:Filter>
            <se:PolygonSymbolizer>
              <se:Fill>
                <se:SvgParameter name="fill">{{ $config->color }}</se:SvgParameter>
                <se:SvgParameter name="fill-opacity">{{ $item['opacity'] }}</se:SvgParameter>
              </se:Fill>
              {{-- <se:Stroke>
                <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                <se:SvgParameter name="stroke-width">0</se:SvgParameter>
              </se:Stroke> --}}
            </se:PolygonSymbolizer>
          </se:Rule>
        @endforeach
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
