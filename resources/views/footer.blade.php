<footer class="p-4 bg-gray-100">
    <div class="flex flex-col lg:flex-row items-center">
        <div class="w-full lg:w-auto lg:mr-auto text-center lg:text-left">
            &copy <a href="#">
                Jatimprov
            </a>
        </div>
    </div>
</footer>
