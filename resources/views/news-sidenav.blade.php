
<div class="card sidenav search">
    <div class="body search">
        <form action="{{ route('news') }}" class="form-horizontal post-search form-wizzard">
            <input type="hidden" name="tags" value="{{ $_GET['tags'] ?? '' }}">
            <div class="input-group input-group-lg" >
                <input type="search" name="search" class="form-control" placeholder="Search ..."/>
                <button class="input-group-text"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
</div>
<div class="card sidenav tags">
    <div class="header p-3">
        <h4 style="text-transform:uppercase; letter-spacing:.2rem;">Tags</h4>
    </div>
    <div class="body widget p-3 pt-0">
        <ul class="list-unstyled categories-clouds m-b-0">
            @foreach ($tags as $tag)
                <li ><a href="{{ route('news', ['tags' => $tag->slug_value ]) }}">{{ $tag->value }} </a></li>
            @endforeach
        </ul>
    </div>
</div>
<div class="card sidenav latest-post">
    <div class="header p-3">
        <h4 style="text-transform:uppercase; letter-spacing:.2rem;">Latest Posts</h4>
    </div>
    <div class="body widget p-3 pt-0">
        <div class="row">
            <div class="col-lg-12">
                @foreach ($latests as $item)
                <div class="single_post post-item">
                    <a href="{{ route('news.detail', $item->slug) }}">
                        <div class="img-post">
                            <img src="{{ $item->image }}" class="w-100 img-fluid" alt="{{ $item->title }}"  onerror="this.onerror=null;this.src='/images/logo.png'">                                        
                        </div>                                            
                        <div class="title-box p-3">
                            <p class="post-title">{{ $item->title }}</p>
                        </div>
                    </a>
                </div>
                @endforeach
                {{-- <div class="single_post">
                    <p class="m-b-0">new rules, more cars, more races</p>
                    <span>jun 8, 2018</span>
                    <div class="img-post">
                        <img src="https://via.placeholder.com/280x280/FFB6C1/000000" alt="Awesome Image">                                            
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>