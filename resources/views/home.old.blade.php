@extends('layouts.frontpage')
@section('content')
    <div id="menu" class="z-10 w-full h-full py-10 overflow-x-hidden bg-white md:py-20">
        <div class="w-full md:w-[1200px] mx-auto">
            <p
                class="block px-4 mx-auto mb-4 text-lg font-black text-center lg:text-lg lg:hidden animate__animated animate__zoomIn">
                Pelayanan Informasi Tata Ruang Provinsi Jawa Timur</p>
            <div class="grid grid-cols-3 md:grid-cols-6 px-1.5">
                <div class="p-3">
                    <a href="/webgis">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/1-informasi.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">Informasi RTR
                            </p>
                        </div>
                    </a>
                </div>
                <div class="p-3">
                    <a href="/progress">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/2-progress.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">Progress RTR
                            </p>
                        </div>
                    </a>
                </div>
                <div class="p-3">
                    <a href="/library">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/3-e-library.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">E-Library
                            </p>
                        </div>
                    </a>
                </div>
                <div class="p-3">
                    <a href="berita">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/4-beritaru.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">Beritaru
                            </p>
                        </div>
                    </a>
                </div>
                <div class="p-3">
                    <a href="/kesesuaian-tata-ruang">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/5-penggunaan-lahan.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">Penggunaan Lahan
                            </p>
                        </div>
                    </a>
                </div>
                <div class="p-3">
                    <a href="/gee">
                        <div style="box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%);" class="pb-4 text-center rounded-xl"
                            data-animere="zoomIn">
                            <div class="px-2 pt-2 pb-1 md:px-8 md:pt-8 md:pb-4">
                                <img src="/assets/images/6-kesesuaian.png" alt="">
                            </div>
                            <p class="text-[8px] md:text-base font-bold">Kesesuaian RTR
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="about"
        class="z-10 flex flex-col justify-center w-screen h-full px-4 overflow-x-hidden lg:h-screen bg-toska">
        <div class="w-full md:w-[1200px] mx-auto py-10">
            <div class="grid grid-cols-1 mb-12 md:grid-cols-2">
                <div data-animere="fadeInLeft">
                    <img src="/assets/images/front-img-1.png" alt="">
                </div>
                <div class="flex flex-col justify-center" data-animere="fadeInRight">
                    <h2 class="p-4 font-bold md:text-[40px] text-base text-center md:text-justify">Tentang Jatim
                        Pintar</h2>
                    <p class="px-4 text-justify text-gray-600">Jatim Pintar merupakan media online Sistem Informasi
                        Tata Ruang (SITR) Provinsi Jawa Timur. Jatim Pintar memuat informasi dan database online
                        data geospasial penataan ruang dan peta tematik. Jatim Pintar memuat Informasi tata ruang
                        RTRW Provinsi Jawa Timur dan Data Spasial Tata Ruang 38 Kabupaten/Kota Se Jawa Timur.
                    </p>
                </div>
            </div>
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div class="md:order-last" data-animere="fadeInRight">
                    <img src="/assets/images/front-img-2.png" alt="">
                </div>
                <div class="flex flex-col justify-center" data-animere="fadeInLeft">
                    <h2 class="p-4 font-bold md:text-[40px] text-base text-center md:text-justify">Mobile Friendly
                    </h2>
                    <p class="px-4 text-justify text-gray-600">Jatim Pintar telah dikembangkan versi dekstop dan
                        mobile. Terbaru, Jatim pintar telah tersedia di playstore (android version). Jatim Pintar
                        dikembangkan untuk memberikan kemudahan dalam akses informasi tata ruang di Provinsi Jawa
                        Timur.Jatim Pintar sekarang data diakses dimanapun dan dengan smartphone anda dapat menambah
                        wawasan penataan ruang di Provinsi Jawa Timur.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="beritaru"
        class="z-10 flex flex-col justify-center w-screen h-full py-10 overflow-x-hidden bg-white lg:h-screen">
        <div class="w-full md:w-[1200px] mx-auto">
            <h2 class="w-full mb-2 text-lg font-bold text-center md:mb-6 md:text-4xl" data-animere="zoomIn">Berita
                Tata Ruang</h2>
            <div class="w-full">
                <div class="hidden grid-cols-1 px-4 md:grid md:grid-cols-3 md:px-0 gap-y-4">
                    @foreach ($latests as $post)
                        <div class="h-full" data-animere="fadeIn">
                            <div class="flex flex-col h-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md">
                                <a href="{{ route('news.detail', $post->slug) }}">
                                    <img class="object-cover rounded-t-lg aspect-video" src="{{ $post->image }}"
                                        alt="">
                                </a>
                                <div class="p-8">
                                    <a href="{{ route('news.detail', $post->slug) }}"
                                        class="text-xl font-bold tracking-tight text-gray-900">{{ $post->title }}
                                    </a>
                                    <p class="my-6 text-sm font-normal text-gray-600">{{ $post->summary }}
                                    </p>
                                    <a href="{{ route('news.detail', $post->slug) }}"
                                        class="text-sm font-bold text-center uppercase">Baca
                                        Selanjutnya
                                    </a>
                                </div>
                                <div class="px-8 py-4 mt-auto border-t border-gray-200">
                                    <span
                                        class="text-xs text-gray-400">{{ date('F jS, Y', strtotime($post->created_at)) }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="hidden w-full mt-8 md:flex md:justify-center">
                    <a role="button" href="/berita"
                        class="text-white bg-berita hover:bg-berita/80 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 focus:outline-none">Baca
                        Lainnya
                    </a>
                </div>
            </div>
            <div class="relative px-5 rounded-lg md:hidden" data-animere="fadeIn" data-carousel="static">
                <!-- Carousel wrapper -->
                <div class="relative h-56 overflow-hidden rounded-lg md:h-96">

                    @foreach ($latests as $post)
                        <div class="absolute inset-0 z-10 transition-all duration-700 ease-in-out transform translate-x-full rounded-lg"
                            data-carousel-item="">
                            <a href="{{ route('news.detail', $post->slug) }}">
                                <img src="{{ $post->image }}" class="carousel-img" alt="...">
                                <div class="carousel-img-gradient"></div>
                                <span class="carousel-title">
                                    {{ $post->title }}
                                </span>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-6 left-1/2">
                    <button type="button" class="w-2 h-2 bg-white rounded-full" aria-current="true"
                        aria-label="Slide 1" data-carousel-slide-to="0"></button>
                    <button type="button" class="w-2 h-2 rounded-full bg-white/50 hover:bg-white" aria-current="false"
                        aria-label="Slide 2" data-carousel-slide-to="1"></button>
                    <button type="button" class="w-2 h-2 rounded-full bg-white/50 hover:bg-white" aria-current="false"
                        aria-label="Slide 3" data-carousel-slide-to="2"></button>
                </div>
                <!-- Slider controls -->
                <button type="button"
                    class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-8 cursor-pointer md:px-4 group focus:outline-none"
                    data-carousel-prev="">
                    <span
                        class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 group-hover:bg-white/50 group-focus:ring-4 group-focus:ring-white group-focus:outline-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6" fill="none"
                            stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7">
                            </path>
                        </svg>
                        <span class="sr-only">Previous</span>
                    </span>
                </button>
                <button type="button"
                    class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-8 cursor-pointer md:px-4 group focus:outline-none"
                    data-carousel-next="">
                    <span
                        class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 group-hover:bg-white/50 group-focus:ring-4 group-focus:ring-white group-focus:outline-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6" fill="none"
                            stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7">
                            </path>
                        </svg>
                        <span class="sr-only">Next</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
@endsection
