@extends('layouts.frontpage')
@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            fontFamily: {
              sans: ['Inter', 'sans-serif'],
            },
          }
        }
      }
    </script> --}}
@endsection
@section('nav')
    <div class=" bg-gray-900">
    @section('logo')
        <img src="/images/logo.png" class="ml-3" alt="" style="width: 100px">
    @endsection
    @include('front-nav')
</div>
@endsection
@section('content')
<div class="container mx-auto px-4">
    <section class="py-12 px-4 text-center">
        <div class="w-full max-w-2xl mx-auto">
            <h1 class="text-5xl leading-tight font-heading"> Kontak Kami</h1>
        </div>
    </section>
    <hr>
    <section class="py-12 px-4">
        <div>
            {!! $content !!}
        </div>
    </section>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js"></script>
@endsection
