<nav class="z-10">
    <div class="w-full lg:w-[1200px] flex flex-wrap justify-between items-center mx-auto py-4 px-8">
        <a href="/"
            class="
              pr-3
              py-2
              rounded-md
              font-medium
              inline-flex
              items-center
              text-lg
            "
            aria-current="page">
            @yield('logo')
        </a>
        <button data-collapse-toggle="navbar-default" type="button"
            class="inline-flex items-center p-2 ml-3 text-sm text-white rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-0 focus:bg-transparent"
            aria-controls="navbar-default" aria-expanded="false" id="menu-toggle">
            <span class="sr-only">Open main menu</span>
            <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clip-rule="evenodd"></path>
            </svg>
        </button>
        <div class="hidden w-full lg:block lg:w-auto lg:relative fixed top-[72px] left-0 lg:top-0" style="z-index: 1"
            id="navbar-default">
            <ul
                class="flex flex-col p-2 border border-gray-100 lg:py-4 lg:px-4 bg-gray-50 lg:flex-row lg:space-x-11 lg:text-sm lg:font-medium lg:border-0 lg:bg-transparent">
                <li class="font-medium">
                    <a href="/"
                        class="block px-4 py-2 text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        Home
                    </a>
                </li>
                <li class="font-medium">
                    <a href="/#about" role="button" data-target="about"
                        class="block px-4 py-2 text-gray-700 nav-link lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        Tentang
                    </a>
                </li>
                <li class="font-medium">
                    <a href="/#beritaru" role="button" data-target="beritaru"
                        class="block px-4 py-2 text-gray-700 nav-link lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        Beritaru
                    </a>
                </li>
                <li class="font-medium">
                    <a href="/contact" role="button" data-target="home"
                        class="block px-4 py-2 text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        Kontak
                    </a>
                </li>
                <li class="font-medium">
                    <a href="/faq"
                        class="block px-4 py-2 text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        FAQ
                    </a>
                </li>

                @auth
                <li class="font-medium">
                    <a href="/login"
                        class="block px-4 py-2 text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        <i class="fas fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>
                @else
                <li class="font-medium">
                    <a href="/login"
                        class="block px-4 py-2 text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0 lg:hover:text-gray-300 lg:p-0">
                        Login
                    </a>
                </li>
                @endauth
                <li class="font-medium">
                    <div id="dropdown-wrapper" class="inline-block">
                        <button type="button" onclick="toggleMenu()"
                            class="inline-flex justify-center w-full px-4 font-medium text-gray-700 lg:text-white hover:bg-gray-100 lg:hover:bg-transparent lg:border-0"
                            id="menu-button" aria-expanded="true" aria-haspopup="true">
                            Galeri
                            <!-- Heroicon name: solid/chevron-down -->
                            <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                        <div id="menu" class="hidden flex flex-col bg-white drop-shadow-md absolute">
                            <a href="/gallery-image" class="text-gray-700 hover:bg-gray-100 block px-8 py-2 text-sm"
                                role="menuitem" tabindex="-1" id="menu-item-0">Image</a>
                            <a href="/gallery-video" class="text-gray-700 hover:bg-gray-100 block px-8 py-2 text-sm"
                                role="menuitem" tabindex="-1" id="menu-item-1">Video</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
