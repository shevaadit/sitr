<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" type="text/css" rel="stylesheet"/>
    <link href="{{ mix('css/base.css') }}" type="text/css" rel="stylesheet" />
    <script src="{{ mix('js/admin.js') }}" type="text/javascript" defer></script>
    <link rel="shortcut icon" href="/favicon.png">
    <title>Admin</title>
</head>

<body style='background-color: white'>
    <div id="app">
    </div>
</body>
</html>
