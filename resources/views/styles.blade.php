{!! '<'.'?xml version="1.0"?>' !!}
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:se="http://www.opengis.net/se" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <se:Name>style_layer_group_{{ $group->id }}</se:Name>
    <UserStyle>
      <se:Name>{{ $group->name }}</se:Name>
      <se:FeatureTypeStyle>
        {{-- <se:Rule>
          <se:Name>default_polygon</se:Name>
          <se:Description>
            <se:Title>Default Polygon</se:Title>
          </se:Description>
          <se:PolygonSymbolizer>
            <se:Fill>
              <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
            </se:Fill>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">1</se:SvgParameter>
            </se:Stroke>
          </se:PolygonSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>default_line</se:Name>
          <se:Description>
            <se:Title>Default Line</se:Title>
          </se:Description>
          <se:LineSymbolizer>
            <se:Stroke>
              <se:SvgParameter name="stroke">#000000</se:SvgParameter>
              <se:SvgParameter name="stroke-width">2</se:SvgParameter>
            </se:Stroke>
          </se:LineSymbolizer>
        </se:Rule> --}}
        @foreach ($group->legends->sortBy('rule_id') as $item)
            {{-- {!! sprintf($item->rule->value, $item->id, $item->name, $item->key, $item->min, $item->color) !!} --}}
            @if ($item->rule->is_range)
            @else
              <se:Rule>
                <se:Name>{{ $item->id }}</se:Name>
                <se:Description>
                  <se:Title>{{ $item->name }}</se:Title>
                </se:Description>
                @if ($item->rule->id !== 0)
                  <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                    <ogc:PropertyIsEqualTo>
                      <ogc:PropertyName>{{ $item->key }}</ogc:PropertyName>
                      <ogc:Literal>{{ $item->min }}</ogc:Literal>
                    </ogc:PropertyIsEqualTo>
                  </ogc:Filter>
                @endif
                @if($item->shape_id === 'line')
                  <se:PolygonSymbolizer>
                    <se:Fill>
                      <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                      <se:SvgParameter name="fill-opacity">0</se:SvgParameter>
                    </se:Fill>
                  </se:PolygonSymbolizer>
                @endif
                <se:{{ $item->shape['tagname'] }}>
                  @if ($item->shape_id === 'point')
                    <se:Graphic>
                      <se:Mark>
                        <se:WellKnownName>circle</se:WellKnownName>
                        <se:Fill>
                          <se:SvgParameter name="fill">{{ $item->color }}</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                          <se:SvgParameter name="stroke">{{ $item->stroke }}</se:SvgParameter>
                          <se:SvgParameter name="stroke-width">{{ $item->stroke_width }}</se:SvgParameter>
                          <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                      </se:Mark>
                      <Size>12</Size>
                    </se:Graphic>
                  @elseif($item->shape_id === 'line')
                    {{-- <se:Fill>
                      <se:SvgParameter name="fill">{{ $item->color }}</se:SvgParameter>
                      <se:SvgParameter name="stroke">{{ $item->stroke }}</se:SvgParameter>
                      <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                    </se:Fill> --}}

                    <se:Stroke>
                      <se:SvgParameter name="stroke">{{ $item->stroke }}</se:SvgParameter>
                      <se:SvgParameter name="stroke-width">{{ $item->stroke_width }}</se:SvgParameter>
                    </se:Stroke>
                  @else
                    <se:Fill>
                      <se:SvgParameter name="fill">{{ $item->color }}</se:SvgParameter>
                    </se:Fill>
                    <se:Stroke>
                      <se:SvgParameter name="stroke">{{ $item->stroke }}</se:SvgParameter>
                      <se:SvgParameter name="stroke-width">{{ $item->stroke_width }}</se:SvgParameter>
                      <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                    </se:Stroke>
                  @endif
                </se:{{ $item->shape['tagname'] }}>
              </se:Rule>
            @endif
        @endforeach
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
