
<nav class="navbar navbar-expand-lg @yield('navbar-class', 'navbar-dark') fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand pb-1 pt-0" href="/">
            {{-- <img src="https://jatim-pintar.com/images/logo-home-text.png" alt=""  height="40" class="mr-2 d-inline-block align-text-top"> --}}
            {{-- <img src="/images/logo_only.png" alt="" height="40" width="40" class="d-inline-block align-text-top"> --}}
            <img src="/images/logo.png" style="fill: var(--primary)" alt="" height="40" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-toggle="collapse" data-target="#navbarNav" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto ml-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">
                        <i class="fa fa-home"></i>
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/berita">
                        <i class="fa fa-blog"></i>
                        Berita
                    </a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="/admin">
                        <i class="fas fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="/login">
                        <i class="fas fa-sign-in-alt"></i>
                        Login
                    </a>
                </li>
                @endauth
            </ul>
        </div>
    </div> 
</nav>