<div
    class="relative z-10 flex flex-col justify-between w-full overflow-hidden border-0 lg:h-screen aspect-video lg:aspect-auto">
    @include('front-nav')
    <div class="absolute flex flex-col justify-center w-screen h-full">
        <img src="/assets/images/logo.png" class="w-1/2 mx-auto lg:w-1/4 animate__animated animate__zoomIn" alt="">
        <p
            class="font-black text-[10px] lg:text-lg mt-4 hidden lg:block text-white mx-auto animate__animated animate__zoomIn">
            Pelayanan Informasi Tata Ruang Provinsi Jawa Timur</p>
        <div class="justify-center hidden w-full mt-8 lg:flex animate__animated animate__zoomIn">
            {{-- IG --}}
            <a href="{{ $contact->instagram ?? '#' }}" class="p-2.5 mr-4 border-2 border-white rounded-full">
                <svg class="w-[25px] h-[25px] text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                    fill="currentColor">
                    <!--! Font Awesome Pro 6.1.2 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                    <path
                        d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z" />
                </svg>
            </a>
            {{-- Email --}}
            {{-- <a href="/contact" class="p-2.5 mr-4 border-2 border-white rounded-full"> --}}
            <a href="mailto:{{ $contact->email ?? '#' }}" class="p-2.5 mr-4 border-2 border-white rounded-full">
                <svg class="w-[25px] h-[25px] text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                    fill="currentColor">
                    <!--! Font Awesome Pro 6.1.2 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                    <path
                        d="M464 64C490.5 64 512 85.49 512 112C512 127.1 504.9 141.3 492.8 150.4L275.2 313.6C263.8 322.1 248.2 322.1 236.8 313.6L19.2 150.4C7.113 141.3 0 127.1 0 112C0 85.49 21.49 64 48 64H464zM217.6 339.2C240.4 356.3 271.6 356.3 294.4 339.2L512 176V384C512 419.3 483.3 448 448 448H64C28.65 448 0 419.3 0 384V176L217.6 339.2z" />
                </svg>
            </a>
            {{-- YouTube --}}
            <a href="{{ $contact->youtube ?? '#' }}" class="p-2.5 border-2 border-white rounded-full">
                <svg class="w-[25px] h-[25px] text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                    fill="currentColor">
                    <!--! Font Awesome Pro 6.1.2 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                    <path
                        d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z" />
                </svg>
            </a>
            {{--  --}}
            <a href="{{ $contact->facebook ?? '#' }}"
                class="ml-4 p-2.5 border-2 border-white rounded-full">
                {{-- <i class="fab fa-lg fa-fw fa-whatsapp pb-0" style="vertical-align:middle;color:white;"></i> --}}
                <i class="fab fa-lg fa-fw fa-facebook pb-0" style="vertical-align:middle;color:white;"></i>
            </a>
            {{--  --}}
            <a href="https://wa.me/{{ $contact->whatsapp ?? '#' }}"
                class="ml-4 p-2.5 border-2 border-white rounded-full">
                <i class="fab fa-lg fa-fw fa-whatsapp pb-0" style="vertical-align:middle;color:white;"></i>
            </a>
        </div>
    </div>
    <div class="justify-center hidden py-4 lg:flex lg:py-2">
        <a id="btn-arrow" role="button">
            <svg class="w-6 text-white lg:w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"
                fill="currentColor">
                <!--! Font Awesome Pro 6.1.2 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                <path
                    d="M374.6 310.6l-160 160C208.4 476.9 200.2 480 192 480s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 370.8V64c0-17.69 14.33-31.1 31.1-31.1S224 46.31 224 64v306.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0S387.1 298.1 374.6 310.6z" />
            </svg>
        </a>
    </div>
</div>
