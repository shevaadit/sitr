@extends('layouts.frontpage')
@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            fontFamily: {
              sans: ['Inter', 'sans-serif'],
            },
          }
        }
      }
    </script> --}}
@endsection
@section('nav')
    <div class=" bg-gray-900">
    @section('logo')
        <img src="/images/logo.png" class="ml-3" alt="" style="width: 100px">
    @endsection
    @include('front-nav')
</div>
@endsection
@section('content')
<section class="overflow-hidden text-gray-700 ">
    <div class="container px-5 py-2 mx-auto lg:pt-12 lg:px-32">
        <div class="flex flex-wrap -m-1 md:-m-2">
            @foreach ($galleries as $gallery)
                <div class="flex flex-wrap w-1/3">
                    <div class="w-full p-1 md:p-2">
                        <div class="embed-responsive embed-responsive-16by9 relative w-full overflow-hidden"
                            style="padding-top: 56.25%">
                            @if ($gallery->is_link)
                                <iframe
                                    class="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                                    src="{{ $gallery->url }}" allowfullscreen=""
                                    data-gtm-yt-inspected-2340190_699="true" id="240632615"></iframe>
                            @else
                                <iframe
                                    class="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                                    src="{{ $gallery->file_url }}" allowfullscreen=""
                                    data-gtm-yt-inspected-2340190_699="true" id="240632615"></iframe>
                            @endif
                        </div>
                        <div  data-micromodal-trigger="modal-{{ $gallery->id }}">
                            <h3>{{ $gallery->title }}</h3>
                        </div>
                    </div>
                </div>

                <div id="modal-{{ $gallery->id }}" class="modal micromodal-slide" id="modal-1"
                    aria-hidden="true">
                    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
                        <div class="modal__container" role="dialog" aria-modal="true"
                            aria-labelledby="modal-1-title">
                            <div class="embed-responsive embed-responsive-16by9 w-96 relative overflow-hidden"
                                style="padding-top: 56.25%">
                                @if ($gallery->is_link)
                                    <iframe
                                        class="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                                        src="{{ $gallery->url }}" allowfullscreen=""
                                        data-gtm-yt-inspected-2340190_699="true" id="240632615"></iframe>
                                @else
                                    <iframe
                                        class="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                                        src="{{ $gallery->file_url }}" allowfullscreen=""
                                        data-gtm-yt-inspected-2340190_699="true" id="240632615"></iframe>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js"></script>
<script>
    /**
     * Dropdown menu
     */
    document.addEventListener('DOMContentLoaded', function() {
        const menus = document.querySelectorAll('.navbar-burger');
        const dropdowns = document.querySelectorAll('.navbar-menu');

        if (menus.length && dropdowns.length) {
            for (var i = 0; i < menus.length; i++) {
                menus[i].addEventListener('click', function() {
                    for (var j = 0; j < dropdowns.length; j++) {
                        dropdowns[j].classList.toggle('hidden');
                    }
                });
            }
        }
    });
    MicroModal.init();
</script>
@endsection
