@extends('layouts.frontpage')
@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/index.min.css" />

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <script src="https://cdn.tailwindcss.com"></script>
    <script>
      tailwind.config = {
        theme: {
          extend: {
            fontFamily: {
              sans: ['Inter', 'sans-serif'],
            },
          }
        }
      }
    </script> --}}
@endsection
@section('nav')
    <div class=" bg-gray-900">
    @section('logo')
        <img src="/images/logo.png" class="ml-3" alt="" style="width: 100px">
    @endsection
    @include('front-nav')
</div>
@endsection
@section('content')
<div class="container mx-auto px-4">
    <section class="py-12 px-4 text-center">
        <div class="w-full max-w-2xl mx-auto">
            <h1 class="text-5xl leading-tight font-heading">FAQ</h1>
        </div>
    </section>
    <hr>
    <section class="py-12 px-4">
        <div class="max-w-3xl mx-auto">
            <div class="accordion" id="faq">
                @foreach ($faqs as $faq)
                    @php
                        $faqId = "faq-{$faq->id}";
                    @endphp
                    <div class="accordion-item bg-white border border-gray-200">
                        <h2 class="accordion-header mb-0" id="h-{{ $faqId }}">
                            <button
                                class="
                                    accordion-button
                                    relative
                                    flex
                                    items-center
                                    w-full
                                    py-4
                                    px-5
                                    text-base text-gray-800 text-left
                                    bg-white
                                    border-0
                                    rounded-none
                                    transition
                                    focus:outline-none
                                "
                                type="button" data-bs-toggle="collapse" data-bs-target="#{{ $faqId }}"
                                aria-expanded="true" aria-controls="{{ $faqId }}">
                                {{ $faq->question }}
                            </button>
                        </h2>
                        <div id="{{ $faqId }}" class="accordion-collapse collapse show"
                            aria-labelledby="h-{{ $faqId }}" data-bs-parent="#faq">
                            <div class="accordion-body py-4 px-5">
                                {!! $faq->answer !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js"></script>
<script>
    /**
     * Dropdown menu
     */
    document.addEventListener('DOMContentLoaded', function() {
        const menus = document.querySelectorAll('.navbar-burger');
        const dropdowns = document.querySelectorAll('.navbar-menu');

        if (menus.length && dropdowns.length) {
            for (var i = 0; i < menus.length; i++) {
                menus[i].addEventListener('click', function() {
                    for (var j = 0; j < dropdowns.length; j++) {
                        dropdowns[j].classList.toggle('hidden');
                    }
                });
            }
        }
    });
</script>
@endsection
