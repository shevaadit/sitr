export default {
  methods: {
    parseObjectToFormData(object, allowUnderscoreKey = false) {
      return Object.keys(object).reduce((formData, key) => {
        let isUnderscoreKey = key.startsWith('_')
        if (allowUnderscoreKey && isUnderscoreKey) {
          formData.append(key, object[key])
        }
        if (!isUnderscoreKey) {
          formData.append(key, object[key])
        }
        return formData
      }, new FormData())
    },
    assignPropertyToObject(assigner, object, ignoreUnderscoreKey = true) {
      if (assigner instanceof Object) {
        Object.keys(assigner).filter(key => key in object).forEach(key  => {
          object[key] = assigner[key]
        })
      }
    },
    addUploadedMultiFile(files, multiFile) {

      if (multiFile.length) {
        files.length = 0
        multiFile.forEach(file => {
          files.push({
            source: file,
            options: {
              type: 'local',
            },
          });
        })
      }
    },
    addUploadedFile(files, file) {
      if (file) {
        files.length = 0
        files.push({
          source: file,
          options: {
            type: 'local',
          },
        });
      }
    },
    getUploadableFile(files) {
      if (Array.isArray(files) && files.length) {
        let file = files[0]
        if (file.serverId === null) {
          return file.file
        }
      }
      return null
    },
    showLoadingAlert(title = 'Loading ...') {
      this.$swal({
        icon: 'info',
        iconHtml: '<i class="fas fa-spinner fa-spin"></i>',
        title: title,
        // timerProgressBar: true,
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: () => {
          this.$swal.showLoading()
        }
      })
    },
    showSuccessAlert(title = null) {
      this.$swal({
        icon: 'success',
        title: title,
        showConfirmButton: false,
        timer: 1500
      })
    },
    showFailedAlert(title = null) {
      this.$swal({
        icon: 'error',
        title: title,
        showConfirmButton: false,
        timer: 1500
      })
    }
  }
}