
import { validationMixin } from "vuelidate";

export default {
  mixins: [validationMixin],
  computed: {
    filepondServer() {
      return {
        load: (source, load, _1, _2, abort, _4) => {
          let request = new Request(source);
          fetch(request).then(function (response) {
            response.blob().then(function (blob) {
              load(blob)
            });
          })

          // Should expose an abort method so the request can be cancelled
          return {
            abort: () => {
              // User tapped cancel, abort our ongoing actions here
              // Let FilePond know the request has been cancelled
              abort()
            },
          };
        },
      }
    },
  },
  methods: {
    v$(property, name = 'formData') {
      const { $dirty, $error } = this.$v[name].hasOwnProperty(property) ? this.$v[name][property] : {}
      return $dirty ? !$error : null
    },
    handleUpdateFile(files, name = 'files') { // new handler for single file
      if (files.length) {
        let file = files[0]
        if (file.status === 2) {
          this[name][0] = file
        }
      }
    },
    handleUpdateFiles(files, name = 'files') {
      if (files.length) {
        let file = files[0]
        this[name][0] = file
        return
      }
    },
    invalidate(name = 'formData') {
      this.$v[name].$touch();
      return this.$v[name].$anyError ? true : false
    }
  }
}
