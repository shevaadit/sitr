/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');
 
 import Vue from 'vue'
 
 import VueRouter from 'vue-router';
 import VueAxios from 'vue-axios';
 import Axios from 'axios';
 import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
 // import {
 //     ValidationObserver,
 //     ValidationProvider,
 //     extend,
 //     localize
 //   } from 'vee-validate'
 // import en from 'vee-validate/dist/locale/en.json'
 // import * as rules from 'vee-validate/dist/rules'
 
 import Multiselect from 'vue-multiselect'
 
 import VueSweetalert2 from 'vue-sweetalert2';
  
 // If you don't need the styles, do not connect
 import 'sweetalert2/dist/sweetalert2.min.css';
 
 // Import Vue FilePond
 import vueFilePond from 'vue-filepond';
 
 // Import FilePond styles
 import 'filepond/dist/filepond.min.css';
 
 // Import FilePond plugins
 // Please note that you need to install these plugins separately
 
 // Import image preview plugin styles
 import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
 
 // Import image preview and file type validation plugins
 import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
 import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
 import FilePondPluginFileMetadata from 'filepond-plugin-file-metadata';
 
 // setOptions({
 //   server: {
 //     load: (source, load, error, progress, abort, headers) => {
 //       let request = new Request(source);
 //       fetch(request).then(function(response) {
 //         response.blob().then(function(blob) {
 //           load(blob)
 //         });
 //       })         
 //     },
 //     process: {
 //         headers: {
 //             'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
 //         },
 //         onload: (response) => {
 //           this.formData.image = JSON.parse(response).path
 //         }
 //     }
 //   },
 // })
 // Create component
 const FilePond = vueFilePond(FilePondPluginFileValidateType, FilePondPluginImagePreview, FilePondPluginFileMetadata);
 
 Vue.component('file-pond', FilePond)
 
 Vue.use(VueSweetalert2);
 
 // Object.keys(rules).forEach(rule => {
 //     extend(rule, rules[rule]);
 // });
   
 // localize("en", en);
 
 // Install VeeValidate components globally
 // Vue.component("ValidationObserver", ValidationObserver);
 // Vue.component("ValidationProvider", ValidationProvider);
   
 Vue.use(VueRouter,VueAxios,Axios);
 Vue.use(BootstrapVue)
 // Optionally install the BootstrapVue icon components plugin
 Vue.use(IconsPlugin)
 
 // Multiselect
 Vue.component('multiselect', Multiselect)
 /**
  * The following block of code may be used to automatically register your
  * Vue components. It will recursively scan this directory for the Vue
  * components and automatically register them with their "basename".
  *
  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
  */
 
 // const files = require.context('./', true, /\.vue$/i)
 // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 
 // Vue.component('example-component', require('./components/ExampleComponent.vue').default);
 
 import VueQuillEditor from 'vue-quill-editor'
 var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],
  ['link'],

  // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  // [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  // [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  // [{ 'direction': 'rtl' }],                         // text direction

  // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  // [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  // [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
]
 Vue.use(VueQuillEditor, {
   modules:{
       toolbar: toolbarOptions
   }
 })
 
 
 import { LMap, LTileLayer, LWMSTileLayer, LGeoJson, LMarker, LControlLayers, LLayerGroup, LControl, LControlZoom, LPopup } from 'vue2-leaflet';
 import 'leaflet/dist/leaflet.css';
 
 Vue.component('l-map', LMap);
 Vue.component('l-geo-json', LGeoJson);
 Vue.component('l-tile-layer', LTileLayer);
 Vue.component('l-popup', LPopup);
 Vue.component('l-marker', LMarker);
 Vue.component('l-control-layers', LControlLayers);
 Vue.component('l-layer-group', LLayerGroup);
 Vue.component('l-control', LControl);
 Vue.component('l-control-zoom', LControlZoom);
 Vue.component('l-wms-tile-layer', LWMSTileLayer);
 
 
 
 import VueClipboard from 'vue-clipboard2'
 
 Vue.use(VueClipboard)
 

import VueLayers from 'vuelayers'
import 'vuelayers/lib/style.css' // needs css-loader

Vue.use(VueLayers)

import Treeselect from '@riophae/vue-treeselect'
// import the styles
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

Vue.component('treeselect', Treeselect)

import pdf from 'pdfvuer'

Vue.component('vue-pdf', pdf)