import { HorizontalBar, mixins } from 'vue-chartjs'
import zoom from 'chartjs-plugin-zoom';

export default {
  extends: HorizontalBar,
  props: {
    options: Object
  },
  data() {
    return {
      defaultOptions: {
        plugins: {
          zoom: {
            // Container for pan options
            pan: {
              // Boolean to enable panning
              enabled: true,

              // Panning directions. Remove the appropriate direction to disable 
              // Eg. 'y' would only allow panning in the y direction
              mode: 'y'
            },

            // Container for zoom options
            zoom: {
              // Boolean to enable zooming
              enabled: true,

              // Zooming directions. Remove the appropriate direction to disable 
              // Eg. 'y' would only allow zooming in the y direction
              mode: 'y',
            }
          }
        }
      }
    }
  },
  watch: {
    chartData () {
      this.renderChart(this.chartData, { ...this.defaultOptions, ...this.options } )
    }
  },
  mixins: [mixins.reactiveProp],
  mounted() {
    this.addPlugin(zoom)
    this.renderChart(this.chartData, { ...this.defaultOptions, ...this.options } )
  }
}