/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./base');

// window.Vue = require('vue');

import VueRouter from 'vue-router';
import Vue from 'vue'
// import VueAxios from 'vue-axios';
// import Axios from 'axios';
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import {
//     ValidationObserver,
//     ValidationProvider,
//     extend,
//     localize
//   } from 'vee-validate'
// import en from 'vee-validate/dist/locale/en.json'
// import * as rules from 'vee-validate/dist/rules'

// import Multiselect from 'vue-multiselect'

// Object.keys(rules).forEach(rule => {
//     extend(rule, rules[rule]);
// });

// localize("en", en);

// // Install VeeValidate pages globally
// Vue.component("ValidationObserver", ValidationObserver);
// Vue.component("ValidationProvider", ValidationProvider);

// Vue.use(VueRouter,VueAxios,Axios);
// Vue.use(BootstrapVue)
// // Optionally install the BootstrapVue icon pages plugin
// Vue.use(IconsPlugin)

// // Multiselect
// Vue.component('multiselect', Multiselect)
import Main from './pages/main.vue'
import ParentView from './pages/parent-view.vue'

const routes = [
    {
        name: 'admin_home',
        redirect: '/admin/dashboard',
        path: '/admin',
        meta: {
            access: ['superadmin', 'admin'],
            hide: true,
        }
    },
    {
        name: 'admin_dashboard',
        path: '/admin/dashboard',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-home',
            title: 'Dashboard'
        },
        component: () => import('./pages/admin/dashboard.vue')
    },
    {
        name: 'admin_profile',
        path: '/admin/profile',
        meta: {
            // access: ['superadmin'],
            icon: 'fas fa-user',
            title: 'Profil',
        },
        component: () => import('./pages/admin/profile')
    },

    {
        name: 'admin_regulation',
        path: '/admin/regulation',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-gavel',
            title: 'Peraturan'
        },
        component: () => import('./pages/admin/regulation/index')
    },
    {
        name: 'admin_regulation_create',
        path: '/admin/regulation/create',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-gavel',
            parent: 'admin_regulation',
            title: 'Peraturan',
            hide: true
        },
        component: () => import('./pages/admin/regulation/create')
    },
    {
        name: 'admin_regulation_show',
        path: '/admin/regulation/:id',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-gavel',
            parent: 'admin_regulation',
            title: 'Peraturan',
            hide: true
        },
        component: () => import('./pages/admin/regulation/show')
    },
    {
        name: 'admin_regulation_edit',
        path: '/admin/regulation/:id/edit',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-gavel',
            parent: 'admin_regulation',
            title: 'Peraturan',
            hide: true
        },
        component: () => import('./pages/admin/regulation/edit')
    },

    {
        name: 'admin_post',
        path: '/admin/post',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-blog',
            title: 'Berita'
        },
        component: () => import('./pages/admin/post/index.vue')
    },
    {
        name: 'admin_post_create',
        path: '/admin/post/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-blog',
            parent: 'admin_post',
            title: 'Berita',
            hide: true
        },
        component: () => import('./pages/admin/post/create')
    },
    {
        name: 'admin_post_show',
        path: '/admin/post/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-blog',
            parent: 'admin_post',
            title: 'Berita',
            hide: true
        },
        component: () => import('./pages/admin/post/show')
    },
    {
        name: 'admin_post_edit',
        path: '/admin/post/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-blog',
            parent: 'admin_post',
            title: 'Berita',
            hide: true
        },
        component: () => import('./pages/admin/post/edit')
    },

    {
        name: 'admin_layer_category',
        path: '/admin/layer-category',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-location-arrow',
            title: 'Layer Kategori'
        },
        component: () => import('./pages/admin/layer-category/index')
    },
    {
        name: 'admin_layer_category_create',
        path: '/admin/layer-category/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-location-arrow',
            parent: 'admin_layer_category',
            title: 'Layer Kategori',
            hide: true
        },
        component: () => import('./pages/admin/layer-category/create')
    },
    {
        name: 'admin_layer_category_show',
        path: '/admin/layer-category/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-location-arrow',
            parent: 'admin_layer_category',
            title: 'Layer Kategori',
            hide: true
        },
        component: () => import('./pages/admin/layer-category/show')
    },
    {
        name: 'admin_layer_category_edit',
        path: '/admin/layer-category/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-location-arrow',
            parent: 'admin_layer_category',
            title: 'Layer Kategori',
            hide: true
        },
        component: () => import('./pages/admin/layer-category/edit')
    },

    {
        name: 'admin_layer_group',
        path: '/admin/layer-group',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cubes',
            title: 'Layer Grup'
        },
        component: () => import('./pages/admin/layer-group/index')
    },
    {
        name: 'admin_layer_group_create',
        path: '/admin/layer-group/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cubes',
            parent: 'admin_layer_group',
            title: 'Layer Grup',
            hide: true
        },
        component: () => import('./pages/admin/layer-group/create')
    },
    {
        name: 'admin_layer_group_show',
        path: '/admin/layer-group/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cubes',
            parent: 'admin_layer_group',
            title: 'Layer Grup',
            hide: true
        },
        component: () => import('./pages/admin/layer-group/show')
    },
    {
        name: 'admin_layer_group_edit',
        path: '/admin/layer-group/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cubes',
            parent: 'admin_layer_group',
            title: 'Layer Grup',
            hide: true
        },
        component: () => import('./pages/admin/layer-group/edit')
    },

    {
        name: 'admin_layer',
        path: '/admin/layer',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-layer-group',
            title: 'Layer'
        },
        component: () => import('./pages/admin/layer/index')
    },
    {
        name: 'admin_layer_create',
        path: '/admin/layer/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-layer-group',
            parent: 'admin_layer',
            title: 'Layer',
            hide: true
        },
        component: () => import('./pages/admin/layer/create')
    },
    {
        name: 'admin_layer_show',
        path: '/admin/layer/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-layer-group',
            parent: 'admin_layer',
            title: 'Layer',
            hide: true
        },
        component: () => import('./pages/admin/layer/show')
    },
    {
        name: 'admin_layer_edit',
        path: '/admin/layer/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-layer-group',
            parent: 'admin_layer',
            title: 'Layer',
            hide: true
        },
        component: () => import('./pages/admin/layer/edit')
    },

    // {
    //     name: 'progress',
    //     path: '/admin/progress',
    //     redirect: '/admin/progress-list',
    //     meta: {
    //         // access: ['superadmin'],
    //         icon: 'fas fa-check-double',
    //         title: 'Progres',
    //     },
    //     component: ParentView,
    //     children: [
    {
        name: 'admin_progress_regional',
        path: '/admin/progress-regional',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-th-large',
            title: 'Entri Progres RTRW'
        },
        component: () => import('./pages/admin/progress-regional/index')
    },
    {
        name: 'admin_progress_regional_create',
        path: '/admin/progress-regional/create',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-th-large',
            parent: 'admin_progress_regional',
            title: 'Entri Progres RTRW',
            hide: true
        },
        component: () => import('./pages/admin/progress-regional/create')
    },
    {
        name: 'admin_progress_regional_show',
        path: '/admin/progress-regional/:id',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-th-large',
            parent: 'admin_progress_regional',
            title: 'Entri Progres RTRW',
            hide: true
        },
        component: () => import('./pages/admin/progress-regional/show')
    },
    {
        name: 'admin_progress_regional_edit',
        path: '/admin/progress-regional/:id/edit',
        meta: {
            access: ['superadmin', 'admin'],
            icon: 'fas fa-th-large',
            parent: 'admin_progress_regional',
            title: 'Entri Progres RTRW',
            hide: true
        },
        component: () => import('./pages/admin/progress-regional/edit')
    },

    {
        name: 'admin_approval_progress_regional',
        path: '/admin/approval-progress-regional',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-check-square',
            title: 'Persetujuan Progres RTRW'
        },
        component: () => import('./pages/admin/approval-progress-regional/index')
    },

    {
        name: 'admin_progress_status_regional',
        path: '/admin/progress-status-regional',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-square',
            title: 'List Status Progres RTRW'
        },
        component: () => import('./pages/admin/progress-status/regional/index')
    },
    // {
    //     name: 'admin_progress_status_create',
    //     path: '/admin/progress-status/create',
    //     meta: {
    //         access: ['superadmin'],
    // icon: 'fas fa-tasks',
    //         parent: 'admin_progress_status',
    //         title: 'Status Progres',
    //         hide: true
    //     },
    //     component: () => import('./pages/admin/progress-status/create')
    // },
    {
        name: 'admin_progress_status_regional_show',
        path: '/admin/progress-status-regional/:id',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-square',
            parent: 'admin_progress_status_regional',
            title: 'Status Progres',
            hide: true
        },
        component: () => import('./pages/admin/progress-status/show')
    },
    {
        name: 'admin_progress_status_regional_edit',
        path: '/admin/progress-status-regional/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-square',
            parent: 'admin_progress_status_regional',
            title: 'Status Progres',
            hide: true
        },
        component: () => import('./pages/admin/progress-status/edit')
    },

    {
        name: 'admin_progress_detailed',
        path: '/admin/progress-detailed',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-th',
            title: 'Entri Progres RDTR'
        },
        component: () => import('./pages/admin/progress-detailed/index')
    },
    {
        name: 'admin_progress_detailed_create',
        path: '/admin/progress-detailed/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-th',
            parent: 'admin_progress_detailed',
            title: 'Entri Progres RDTR',
            hide: true
        },
        component: () => import('./pages/admin/progress-detailed/create')
    },
    {
        name: 'admin_progress_detailed_show',
        path: '/admin/progress-detailed/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-th',
            parent: 'admin_progress_detailed',
            title: 'Entri Progres RDTR',
            hide: true
        },
        component: () => import('./pages/admin/progress-detailed/show')
    },
    {
        name: 'admin_progress_detailed_edit',
        path: '/admin/progress-detailed/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-th',
            parent: 'admin_progress_detailed',
            title: 'Entri Progres RDTR',
            hide: true
        },
        component: () => import('./pages/admin/progress-detailed/edit')
    },

    {
        name: 'admin_approval_progress_detailed',
        path: '/admin/approval-progress-detailed',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-check-circle',
            title: 'Persetujuan Progres RDTR'
        },
        component: () => import('./pages/admin/approval-progress-detailed/index')
    },

    {
        name: 'admin_progress_status_detailed',
        path: '/admin/progress-status-detailed',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-circle',
            title: 'List Status Progres RDTR'
        },
        component: () => import('./pages/admin/progress-status/detailed/index')
    },
    {
        name: 'admin_progress_status_detailed_show',
        path: '/admin/progress-status-detailed/:id',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-circle',
            parent: 'admin_progress_status_detailed',
            title: 'Status Progres',
            hide: true
        },
        component: () => import('./pages/admin/progress-status/show')
    },
    {
        name: 'admin_progress_status_detailed_edit',
        path: '/admin/progress-status-detailed/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'far fa-check-circle',
            parent: 'admin_progress_status_detailed',
            title: 'Status Progres',
            hide: true
        },
        component: () => import('./pages/admin/progress-status/edit')
    },
    //     ]
    // },

    {
        name: 'admin_indicator',
        path: '/admin/indicator',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-chart-bar',
            title: 'Indikator'
        },
        component: () => import('./pages/admin/indicator/index')
    },
    {
        name: 'admin_indicator_create',
        path: '/admin/indicator/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-chart-bar',
            parent: 'admin_indicator',
            title: 'Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator/create')
    },
    {
        name: 'admin_indicator_show',
        path: '/admin/indicator/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-chart-bar',
            parent: 'admin_indicator',
            title: 'Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator/show')
    },
    {
        name: 'admin_indicator_edit',
        path: '/admin/indicator/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-chart-bar',
            parent: 'admin_indicator',
            title: 'Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator/edit')
    },

    {
        name: 'admin_indicator_document',
        path: '/admin/indicator-document',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-file-alt',
            title: 'Indikator Dokumen'
        },
        component: () => import('./pages/admin/indicator-document/index')
    },
    {
        name: 'admin_indicator_document_create',
        path: '/admin/indicator-document/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-file-alt',
            parent: 'admin_indicator_document',
            title: 'Indikator Dokumen',
            hide: true
        },
        component: () => import('./pages/admin/indicator-document/create')
    },
    {
        name: 'admin_indicator_document_show',
        path: '/admin/indicator-document/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-file-alt',
            parent: 'admin_indicator_document',
            title: 'Indikator Dokumen',
            hide: true
        },
        component: () => import('./pages/admin/indicator-document/show')
    },
    {
        name: 'admin_indicator_document_edit',
        path: '/admin/indicator-document/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-file-alt',
            parent: 'admin_indicator_document',
            title: 'Indikator Dokumen',
            hide: true
        },
        component: () => import('./pages/admin/indicator-document/edit')
    },

    {
        name: 'admin_indicator_config',
        path: '/admin/indicator-config',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-pencil-ruler',
            title: 'Kerangka Indikator'
        },
        component: () => import('./pages/admin/indicator-config/index')
    },
    {
        name: 'admin_indicator_config_create',
        path: '/admin/indicator-config/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-pencil-ruler',
            parent: 'admin_indicator_config',
            title: 'Kerangka Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator-config/create')
    },
    {
        name: 'admin_indicator_config_show',
        path: '/admin/indicator-config/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-pencil-ruler',
            parent: 'admin_indicator_config',
            title: 'Kerangka Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator-config/show')
    },
    {
        name: 'admin_indicator_config_edit',
        path: '/admin/indicator-config/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-pencil-ruler',
            parent: 'admin_indicator_config',
            title: 'Kerangka Indikator',
            hide: true
        },
        component: () => import('./pages/admin/indicator-config/edit')
    },

    {
        name: 'admin_geometry',
        path: '/admin/geometry',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cube',
            title: 'Geometri'
        },
        component: () => import('./pages/admin/geometry/index')
    },
    {
        name: 'admin_geometry_create',
        path: '/admin/geometry/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cube',
            parent: 'admin_geometry',
            title: 'Geometri',
            hide: true
        },
        component: () => import('./pages/admin/geometry/create')
    },
    {
        name: 'admin_geometry_show',
        path: '/admin/geometry/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cube',
            parent: 'admin_geometry',
            title: 'Geometri',
            hide: true
        },
        component: () => import('./pages/admin/geometry/show')
    },
    {
        name: 'admin_geometry_edit',
        path: '/admin/geometry/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cube',
            parent: 'admin_geometry',
            title: 'Geometri',
            hide: true
        },
        component: () => import('./pages/admin/geometry/edit')
    },

    {
        name: 'admin_library',
        path: '/admin/library',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-book',
            title: 'Library'
        },
        component: () => import('./pages/admin/library/index')
    },
    {
        name: 'admin_library_create',
        path: '/admin/library/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-book',
            parent: 'admin_library',
            title: 'Library',
            hide: true
        },
        component: () => import('./pages/admin/library/create')
    },
    {
        name: 'admin_library_show',
        path: '/admin/library/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-book',
            parent: 'admin_library',
            title: 'Library',
            hide: true
        },
        component: () => import('./pages/admin/library/show')
    },
    {
        name: 'admin_library_edit',
        path: '/admin/library/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-book',
            parent: 'admin_library',
            title: 'Library',
            hide: true
        },
        component: () => import('./pages/admin/library/edit')
    },

    {
        name: 'admin_gee_rule',
        path: '/admin/gee',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-globe-asia',
            title: 'GEE'
        },
        component: () => import('./pages/admin/earthengine/index')
    },

    {
        name: 'admin_regency',
        path: '/admin/regency',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-building',
            title: 'Kabupaten/Kota'
        },
        component: () => import('./pages/admin/regency/index')
    },


    {
        name: 'admin_user',
        path: '/admin/user',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-user',
            title: 'Pengguna'
        },
        component: () => import('./pages/admin/user/index')
    },
    {
        name: 'admin_user_create',
        path: '/admin/user/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-user',
            parent: 'admin_user',
            title: 'Pengguna',
            hide: true
        },
        component: () => import('./pages/admin/user/create')
    },
    {
        name: 'admin_user_show',
        path: '/admin/user/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-user',
            parent: 'admin_user',
            title: 'Pengguna',
            hide: true
        },
        component: () => import('./pages/admin/user/show')
    },
    {
        name: 'admin_user_edit',
        path: '/admin/user/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-user',
            parent: 'admin_user',
            title: 'Pengguna',
            hide: true
        },
        component: () => import('./pages/admin/user/edit')
    },

    {
        name: 'admin_partner',
        path: '/admin/partner',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-handshake',
            title: 'Link Kab/Kota'
        },
        component: () => import('./pages/admin/partner/index.vue')
    },
    {
        name: 'admin_partner_create',
        path: '/admin/partner/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-handshake',
            parent: 'admin_partner',
            title: 'Link Kab/Kota',
            hide: true
        },
        component: () => import('./pages/admin/partner/create')
    },
    {
        name: 'admin_partner_show',
        path: '/admin/partner/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-handshake',
            parent: 'admin_partner',
            title: 'Link Kab/Kota',
            hide: true
        },
        component: () => import('./pages/admin/partner/show')
    },
    {
        name: 'admin_gallery',
        path: '/admin/gallery',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-photo-video',
            title: 'Gallery'
        },
        component: () => import('_p/admin/gallery/index.vue')
    },
    {
        name: 'admin_gallery_create',
        path: '/admin/gallery/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-photo-video',
            parent: 'admin_gallery',
            title: 'Gallery',
            hide: true
        },
        component: () => import('_p/admin/gallery/create')
    },
    {
        name: 'admin_gallery_show',
        path: '/admin/gallery/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-photo-video',
            parent: 'admin_gallery',
            title: 'Gallery',
            hide: true
        },
        component: () => import('_p/admin/gallery/show')
    },
    {
        name: 'admin_gallery_edit',
        path: '/admin/gallery/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-photo-video',
            parent: 'admin_gallery',
            title: 'Gallery',
            hide: true
        },
        component: () => import('_p/admin/gallery/edit')
    },


    {
        name: 'admin_slide',
        path: '/admin/slide',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-image',
            title: 'Slide'
        },
        component: () => import('_p/admin/slide/index.vue')
    },
    {
        name: 'admin_slide_create',
        path: '/admin/slide/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-image',
            parent: 'admin_slide',
            title: 'Slide',
            hide: true
        },
        component: () => import('_p/admin/slide/create')
    },
    {
        name: 'admin_slide_show',
        path: '/admin/slide/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-image',
            parent: 'admin_slide',
            title: 'Slide',
            hide: true
        },
        component: () => import('_p/admin/slide/show')
    },
    {
        name: 'admin_slide_edit',
        path: '/admin/slide/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-image',
            parent: 'admin_slide',
            title: 'Slide',
            hide: true
        },
        component: () => import('_p/admin/slide/edit')
    },

    {
        name: 'admin_partner_edit',
        path: '/admin/partner/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-handshake',
            parent: 'admin_partner',
            title: 'Link Kab/Kota',
            hide: true
        },
        component: () => import('./pages/admin/partner/edit')
    },

    {
        name: 'admin_faq',
        path: '/admin/faq',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-question-circle',
            title: 'F.A.Q'
        },
        component: () => import('./pages/admin/faq/index')
    },
    {
        name: 'admin_faq_create',
        path: '/admin/faq/create',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-question-circle',
            parent: 'admin_faq',
            title: 'F.A.Q',
            hide: true
        },
        component: () => import('./pages/admin/faq/create')
    },
    {
        name: 'admin_faq_show',
        path: '/admin/faq/:id',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-question-circle',
            parent: 'admin_faq',
            title: 'F.A.Q',
            hide: true
        },
        component: () => import('./pages/admin/faq/show')
    },
    {
        name: 'admin_faq_edit',
        path: '/admin/faq/:id/edit',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-question-circle',
            parent: 'admin_faq',
            title: 'F.A.Q',
            hide: true
        },
        component: () => import('./pages/admin/faq/edit')
    },

    {
        name: 'admin_disclaimer_edit',
        path: '/admin/disclaimer',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cog',
            title: 'Disclaimer Kesesuaian'
        },
        component: () => import('./pages/admin/disclaimer/show')
    },
    {
        name: 'admin_disclaimer_webgis_edit',
        path: '/admin/disclaimer-webgis',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cog',
            title: 'Disclaimer Webgis'
        },
        component: () => import('./pages/admin/disclaimer-webgis/show')
    },
    {
        name: 'admin_setting',
        path: '/admin/setting',
        meta: {
            access: ['superadmin'],
            icon: 'fas fa-cog',
            title: 'Setting'
        },
        component: () => import('./pages/admin/setting/index.vue')
    },
    { path: "*", component: () => import('./pages/404.vue') }
]

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

new Vue(Vue.util.extend({
    router,
    created() {
        axios.interceptors.response.use(function (response) {
            // console.log(response);
            // Any status code within the range of 2xx cause this function to trigger
            // Do something with response data
            return response;
        }, function (error) {
            // console.log(error);
            if (error.response.status) {
                switch (error.response.status) {
                    case 401:
                        window.location.reload()
                        break;
                }
            }
            return Promise.reject(error);
        });
    }
}, Main)).$mount("#app");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding pages to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });
