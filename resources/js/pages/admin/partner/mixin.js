
import masterMixin from '../../../mixins/master'

export default {
  data () {
    return {
      fileImage: [],
      types: [],
      formData: {
        is_published: 0,
        name: null,
        url: null,
        _image: null,
        image: null
      }
    }
  },
  mixins: [ masterMixin ],
  methods: {
    setOptions (data) {
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      let file = this.getUploadableFile(this.fileImage)
      if (file) {
        formData.append('file', file)
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}