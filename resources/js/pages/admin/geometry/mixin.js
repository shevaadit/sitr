export default {
  data () {
    return {
      categories: [],
      formData: {
        name: null,
        category_id: null,
      }
    }
  },
  methods: {  
    loadGeojson () {
      axios.get(`/api/admin/geometries/${this.id}/geojson`).then(response => {
        this.$refs.baseform.setGeoJson(response.data)
      });
    },
    setOptions (data) {
      this.categories = data.categories
    },
    getParams () {
      return {
        name: this.formData.name,
        category_id: this.formData.category_id,
      }
    },
  }
}