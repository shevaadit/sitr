export default {
  data () {
    return {
      id: null,
      errors: {},
      showError: false,
      groups: [],
      regulations: [],
      regionTypes: [],
      files: [],
      formData: {
        file: null,
        srid: null,
        is_published: 0,
        name: null,
        region_id: null,
        region_type: null,
        regulation_id: null,
        layer_group_id: null
      }
    }
  },
  methods: {    
    loadGeojson () {
      axios.get(`/api/admin/layers/${this.id}/geojson`).then(response => {
        this.$refs.baseform.setGeoJson(response.data)
      });
    },
    setOptions (data) {
      this.groups = data.groups
      this.regulations = data.regulations
      this.regionTypes = data.regionTypes
      this.$refs.baseform.loadSrid(this.formData.srid)
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      formData.append('layer_group_id', this.formData.layer_group_id)
      formData.append('regulation_id', this.formData.regulation_id)
      formData.append('region_type', this.formData.region_type)
      formData.append('region_id', this.formData.region_id)
      formData.append('srid', this.formData.srid)
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}