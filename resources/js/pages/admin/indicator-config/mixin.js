export default {
  data () {
    return {
      formData: {
        name: null,
        parent_id: null,
        category_id: null,
      }
    }
  },
  methods: {
    setOptions (data) {
    },
    getParams () {
      return {
        name: this.formData.name,
        category_id: this.formData.category_id,
        parent_id: this.formData.parent_id,
      }
    },
  }
}