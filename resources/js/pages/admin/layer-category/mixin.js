export default {
  data () {
    return {
      formData: {
        name: null,
        key: null,
      }
    }
  },
  methods: {
    getParams () {
      return {
        name: this.formData.name,
        key: this.formData.key,
      }
    },
  }
}