
import masterMixin from '@/mixins/master'

export default {
  data () {
    return {
      id: null,
      endpoint: '/api/admin/slides',
      files: [],
      loadingData: false,
      formData: {
        name: null,
        url: null,
        image: null,
        is_published: 0,
      }
    }
  },
  mixins: [ masterMixin ],
  methods: {
    setOptions (data) {
      // this.statuses = data.statuses
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (editing) {
        formData.append('_method', 'put')
      }
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}