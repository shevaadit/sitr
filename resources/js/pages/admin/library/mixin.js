
import BaseForm from './form'
import masterMixin from '../../../mixins/master'

export default {
  components: {
    BaseForm
  },
  mixins: [ masterMixin ],
  data () {
    return {
      id: null,
      errors: {},
      showError: false,
      types: [],
      tags: [],
      files: [],
      formData: {
        file: null,
        issuance: null,
        taglist: [],
        name: null,
        note: null,
        type_id: null,
      },
      defaultFormData: {
        file: null,
        issuance: 0,
        taglist: [],
        name: null,
        note: null,
        type_id: null
      }
    }
  },
  methods: {
    setOptions (data) {
      this.tags = data.tags
      this.types = data.types
    },
    resetForm () {
      this.assignPropertyToObject(this.defaultFormData, this.formData);

      this.$nextTick (() => {
        this.$refs.baseform.$v.$reset()
      });
    },
    getFormData (editing = false) {
      let formData = this.parseObjectToFormData(this.formData)
      formData.delete('taglist')
      this.formData.taglist.forEach(item => {
        formData.append('taglist[]', item.value)
      })
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}