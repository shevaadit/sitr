
import masterMixin from '../../../mixins/master'

export default {
  data () {
    return {
      fileImage: [],
      types: [],
      formData: {
        is_published: 0,
        name: null,
        content: null,
        image: null,
        taglist: []
      }
    }
  },
  mixins: [ masterMixin ],
  methods: {
    setOptions (data) {
      this.types = data.tags
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      formData.delete('taglist')
      this.formData.taglist.forEach(item => {
        formData.append('taglist[]', item.value)
      })
      
      if (this.fileImage.length) {
        formData.append('file', this.fileImage[0].file)
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}