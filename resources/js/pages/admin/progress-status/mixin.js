export default {
  data () {
    return {
      formData: {
        name: null,
        note: null,
        score: 0,
        color: null,
      }
    }
  },
  methods: {
    getParams () {
      return {
        name: this.formData.name,
        color: this.formData.color,
        score: this.formData.score,
        note: this.formData.note
      }
    },
  }
}