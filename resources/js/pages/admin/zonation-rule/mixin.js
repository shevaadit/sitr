export default {
  data () {
    return {
      regulations: [],
      formData: {
        regulation_id: null,
        regulation_number: null,
        chapter: null,
        verse: null,
        point: null,
        content: null,
        description: null
      }
    }
  },
  methods: {
    getParams () {
      return this.formData
    },
  }
}