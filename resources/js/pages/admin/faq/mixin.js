
import masterMixin from '@/mixins/master'

export default {
  data () {
    return {
      id: null,
      endpoint: '/api/admin/faqs',
      loadingData: false,
      formData: {
        sequence: null,
        issuance: null,
        question: null,
        answer: null
      }
    }
  },
  mixins: [ masterMixin ],
  methods: {
    setOptions (data) {
      // this.statuses = data.statuses
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}