export default {
  data () {
    return {
      id: null,
      fileImage: [],
      types: [],
      regionTypes: [],
      formData: {
        name: null,
        content: null,
        image: null,
        _input_password: null,
        _confirm_password: null,
        region_id: null,
        region_type: null
      }
    }
  },
  methods: {
    setOptions (data) {
      this.regionTypes = data.regionTypes
    },
    getParams () {
      return {
        email: this.formData.email,
        name: this.formData.name,
        password: this.formData._input_password,
        region_type: this.formData.region_type,
        region_id: this.formData.region_id,
        image: this.formData.image
      }
    }
  }
}