export default {
  data () {
    return {
      categories: [],
      parents: [],
      formData: {
        name: null,
        parent_id: null,
        category_id: null,
      }
    }
  },
  methods: {
    setOptions (data) {
      this.categories = data.categories
      this.parents = data.parents
    },
    getParams () {
      return {
        name: this.formData.name,
        category_id: this.formData.category_id,
        parent_id: this.formData.parent_id,
        is_published: this.formData.is_published
      }
    },
  }
}