
import masterMixin from '../../../mixins/master'

export default {
  data () {
    return {
      id: null,
      files: [],
      formData: {
        name: null,
        is_published: 0,
        file: null,
      }
    }
  },
  mixins: [masterMixin],
  methods: {
    loadData () {},
    resetForm () {
      this.formData = {
        name: null,
        is_published: 0,
        file: null
      };
      this.loadData()
    },
    // setOptions (data) {
    //   this.configs = data.configs
    // },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return {
        name: this.formData.name,
        key: this.formData.key,
      }
    },
  }
}