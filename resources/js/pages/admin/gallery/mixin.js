
import masterMixin from '@/mixins/master'

export default {
  data() {
    return {
      id: null,
      endpoint: '/api/admin/galleries',
      files: [],
      loadingData: false,
      formData: {
        title: null,
        url: null,
        image: null,
        file_type: 0,
        media_type: 0,
        is_published: 0,
        _hasFile: false,
      }
    }
  },
  mixins: [masterMixin],
  methods: {
    setOptions(data) {
      // this.statuses = data.statuses
    },
    getFormData(editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (editing) {
        formData.append('_method', 'put')
      }
      if (this.formData.media_type === 0) {
        if (this.files.length) {
          this.files.forEach(file => {
            if (file.file) {
              formData.append('files[]', file.file)
            }
          });
        }
      }
      return formData
    },
    getParams() {
      return this.formData
    }
  }
}