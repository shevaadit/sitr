
import masterMixin from '../../../mixins/master'

export default {
  data () {
    return {
      id: null,
      types: [],
      groups: [],
      regulations: [],
      regionTypes: [],
      files: [],
      vector: {
        features: []
      },
      formData: {
        file: null,
        srid: null,
        name: null,
        type_id: null,
        regency_id: null,
        region_id: null,
        region_type: null,
        regulation_id: null,
        layer_group_id: null
      }
    }
  },
  mixins: [ masterMixin ],
  methods: {    
    loadGeojson () {
      axios.get(`/api/admin/progress-detaileds/${this.id}/geojson`).then(response => {
        this.$refs.baseform.setGeoJson(response.data)
      });
    },
    setOptions (data) {
      // this.groups = data.groups
      this.regulations = data.regulations
      this.regionTypes = data.regionTypes
      this.types = data.types
      if (data.geojson) {
        this.vector = data.geojson
      }
      // this.$refs.baseform.(response.data)
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      if (this.vector.features.length) {
        formData.append('geometry', JSON.stringify(this.vector.features[0].geometry))
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return this.formData
    }
  }
}