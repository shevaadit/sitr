
import masterMixin from '../../../mixins/master'

export default {
  data () {
    return {
      id: null,
      files: [],
      tableFields: [],
      tableItems: [],
      configs: [],
      formData: {
        name: null,
        is_published: 0,
        config_id: null,
        file: null,
      }
    }
  },
  mixins: [masterMixin],
  methods: {
    loadData () {},
    resetForm () {
      this.formData = {
        name: null,
        is_published: 0,
        config_id: null,
        file: null
      };
      this.loadData()
    },
    setOptions (data) {
      this.configs = data.configs
    },
    loadDetails () {
      axios.get(`/api/admin/indicators/${this.id}/fetch`).then(response => {
        this.tableFields = response.data.fields
        this.tableItems = response.data.items
      })
    },
    getFormData (editing = false) {
      const formData = this.parseObjectToFormData(this.formData)
      if (this.files.length) {
        formData.append('file', this.files[0].file)
      }
      if (editing) {
        formData.append('_method', 'put')
      }
      return formData
    },
    getParams () {
      return {
        name: this.formData.name,
        key: this.formData.key,
      }
    },
  }
}