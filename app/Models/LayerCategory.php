<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayerCategory extends MasterModel
{
    use HasFactory;
    protected $fillable = [
        'key',
        'name',
        'created_at',
        'update_at',
    ];
}
