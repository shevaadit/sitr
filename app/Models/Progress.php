<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class Progress extends MasterModel
{
    use HasFactory;

    protected $fillable = [
        'region_type',
        'region_id',
        'regulation_id',
        'last_progress_status_id',
        'submission_person_name',
        'submission_person_position',
        'submission_date',
        'note',
        'type_id',
        'geometry',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public static function boot() {
        parent::boot();
		parent::authorizing();
    }

    public function region()
    {
        return $this->morphTo(__FUNCTION__, 'region_type', 'region_id');
    }

    public function regency()
    {
        return $this->belongsTo(RegencyFormatted::class, 'region_id');
    }

    public function district()
    {
        return $this->belongsTo(DistrictFormatted::class, 'region_id');
    }

    public function regulation()
    {
        return $this->belongsTo(Regulation::class, 'regulation_id');
    }

    public function lastStatus()
    {
        return $this->belongsTo(ProgressStatus::class, 'last_progress_status_id');
    }

    public function submissions()
    {
        return $this->hasMany(ProgressSubmission::class, 'progress_id');
    }
    public function getTypeRegionAttribute()
    {
        return (Object) self::REGION_TYPES[$this->region_type];
    }
    public function getTypeAttribute()
    {
        return (Object) self::TYPES[$this->type_id];
    }

    public function scopeRegional($query)
    {
        return $query->where('progress.type_id', 'regional');
    }

    public function scopeDetailed($query)
    {
        return $query->where('progress.type_id', 'detailed');
    }

    const REGION_TYPES = [
        Regency::class => [
            'id' => Regency::class,
            'name' => 'Kabupaten/Kota'
        ],
        District::class => [
            'id' => District::class,
            'name' => 'Kecamatan'
        ]
    ];

    const TYPE_REGIONAL = 'regional';
    const TYPE_DETAILED = 'detailed';

    const TYPES = [
        'regional' => [
            'id' => 'regional',
            'name' => 'RTRW (Rencana Tata Ruang Wilayah)',
            'region_type' => Regency::class
        ],
        'detailed' => [
            'id' => 'detailed',
            'name' => 'RDTR (Rencana Detail Tata Ruang)',
            'region_type' => District::class
        ]
    ];
}
