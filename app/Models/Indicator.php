<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class Indicator extends MasterModel
{
    use HasFactory;
    use HasAuthFill;
    protected $fillable = [
        'name',
        'file',
        'is_published',
        'config_id',
        'created_by',
        'updated_by'
    ];
    protected $appends = [
        'wms'
    ];
    public function config()
    {
        return $this->belongsTo(IndicatorConfig::class, 'config_id');
    }
    public function getLayerName() {
        return "layer_indicator_{$this->id}";
    }
    public function getWmsAttribute() {
        return config('geoserver.host').'/'.config('geoserver.path').'/'.config('geoserver.workspace').'/wms';
    }
}
