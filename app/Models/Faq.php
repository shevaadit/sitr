<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;

class Faq extends MasterModel
{
    use HasFactory;
    protected $fillable = [
        'sequence',
        'answer',
        'question',
        'issuance',
        'created_by',
        'updated_by',
    ];
    public static function boot() {
        parent::boot();
		parent::authorizing();
    }
}
