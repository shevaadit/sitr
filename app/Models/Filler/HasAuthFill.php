<?php

namespace App\Models\Filler;

trait HasAuthFill {   
    public static function boot() {
        parent::boot();
        if (auth()->check()) {
            self::creating(function ($query) {
                $query->created_by = auth()->user()->id;
            });
            self::updating(function ($query) {
                $query->updated_by = auth()->user()->id;
            });
        }
    }
    // public static function creating($callback)
    // {
    //     $callback->created_by = auth()->user()->id;
    //     parent::creating($callback);
    // }
    // public function setCreatedByAttribute()
    // {
    //     $this->attributes['created_by'] = auth()->user()->id ?? null;
    // }
    // public function setUpdatedByAttribute()
    // {
    //     $this->attributes['updated_by'] = auth()->user()->id ?? null;
    // }
}
