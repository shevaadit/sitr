<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class ZonationRuleActivity extends MasterModel
{
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'zonation_rule_id',
        'rule',
        'name',
    ];
}
