<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, AuthenticationLogable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'email',
        'region_id',
        'region_type',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTypeAttribute()
    {
        return User::REGION_TYPES[$this->region_type];
    }
    public function region()
    {
        return $this->morphTo(__FUNCTION__, 'region_type', 'region_id');
    }
    public function getFillable()
    {
        return $this->fillable;
    }
    public static function boot() {
        parent::boot();
        
		self::creating(function ($query) {
            if (property_exists($query, 'created_by')) 
			    $query->created_by = auth()->user()->id ?? null;
		});
		self::updating(function ($query) {
            if (property_exists($query, 'updated_by')) 
			    $query->updated_by = auth()->user()->id ?? null;
		});
    }

    const REGION_TYPES = [
        Province::class => [
            'id' => Province::class,
            'name' => 'Provinsi'
        ],
        Regency::class => [
            'id' => Regency::class,
            'name' => 'Kabupaten/Kota'
        ]
    ];
}
