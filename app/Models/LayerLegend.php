<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayerLegend extends Model
{
    use HasFactory;
    protected $fillable = [
        'min',
        'max',
        'key',
        'name',
        'rule_id',
        'shape_id',
        'color',
        'stroke',
        'stroke_width',
        'layer_group_id',
        'created_at',
        'update_at',
    ];

    
    protected $appends = [
        'shape'
    ];
    public function getShapeAttribute()
    {
        return self::SHAPES[$this->shape_id];
    }
    public function rule()
    {
        return $this->belongsTo(LayerLegendRule::class, 'rule_id');
    }
    const SHAPES = [
        'polygon' => [
            'id' => 'polygon',
            'tagname' => 'PolygonSymbolizer',
            'name' => 'Polygon',
        ],
        'line' => [
            'id' => 'line',
            'tagname' => 'LineSymbolizer',
            'name' => 'Line'
        ],
        'point' => [
            'id' => 'point',
            'tagname' => 'PointSymbolizer',
            'name' => 'Point'
        ]
    ];
}
