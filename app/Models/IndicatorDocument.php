<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class IndicatorDocument extends Model
{
    use HasFactory;
    use HasAuthFill;
    protected $fillable = [
        'name',
        'file',
        'mime',
        'is_published',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
