<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

/**
 * Province Model.
 */
class ProvinceFormatted extends Province {
    protected $table = 'province_formatteds';
}
