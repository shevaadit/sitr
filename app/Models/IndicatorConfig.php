<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class IndicatorConfig extends MasterModel
{
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'indicator_config_id',
        'name',
    ];
}
