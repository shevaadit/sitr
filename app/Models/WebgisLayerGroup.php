<?php

namespace App\Models;

class WebgisLayerGroup extends LayerGroup
{
    protected $table = 'layer_groups';

    protected $appends = [
        'children',
        'wms'
    ];
    public function legends()
    {
        return $this->hasMany(LayerLegend::class, 'layer_group_id');
    }
    public function child()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->where('is_published', 1)
            ->selectWebgisFields();
            // ->with('child');
    }

    public function getChildrenAttribute()
    {
        $children = $this->child();
        return $children->count() ? $children->get() : false;
    }

    public function scopeSelectWebgisFields($query) {
        return $query->select("*")
            ->selectRaw('true as is_showing')
            ->selectRaw('1 as opacity')
            ->selectRaw('has_child as isDisabled')
            ->selectRaw('id as uuid')
            ->selectRaw('name as label')
            ->withCount('legends');
    }
}