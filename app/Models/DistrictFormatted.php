<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

/**
 * Province Model.
 */
class DistrictFormatted extends Province {
    protected $table = 'district_formatteds';
}
