<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class IndicatorConfigDetail extends MasterModel
{
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'indicator_config_id',
        'name',
        'key',
        'color',
        'is_label',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];
    public function getStyleName() {
        return "style_indicator_{$this->id}_{$this->key}";
    }
    public function values()
    {
        return $this->hasMany(IndicatorValue::class, 'config_detail_id');
    }
}
