<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayerAttribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'key',
        'name',
        'data_type',
        'layer_group_id',
        'created_at',
        'update_at',
    ];

    public function group()
    {
        return $this->belongsTo(LayerGroup::class, 'layer_group_id');
    }
    const DATA_TYPES = [
        'number' => [
            'id' => 'number',
            'name' => 'Number'
        ],
        'string' => [
            'id' => 'string',
            'name' => 'String'
        ]
    ];
}
