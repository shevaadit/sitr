<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class ProgressSubmission extends MasterModel
{
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'id',
        'progress_id',
        'progress_status_id',
        'prev_progress_status_id',
        'submission_person_name',
        'submission_person_position',
        'note',
        'status',
        'message',
        'checked_date',
        'checked_by',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    public function progress()
    {
        return $this->belongsTo(Progress::class, 'progress_id');
    }
    public function prevProgressStatus()
    {
        return $this->belongsTo(ProgressStatus::class, 'prev_progress_status_id');
    }
    public function progressStatus()
    {
        return $this->belongsTo(ProgressStatus::class, 'progress_status_id');
    }
    public function documents()
    {
        return $this->hasMany(ProgressSubmissionDocument::class, 'progress_submission_id');
    }
    public function scopeApproved($query)
    {
        return $query->whereIn('status', [1]);
    }
}
