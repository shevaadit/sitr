<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZonationRule extends Model
{
    use HasFactory;

    protected $fillable = [
        'layer_group_id',
        'value',
        'key',
        'name',
    ];
    protected $attributes = [
        // 'regulation_number' => '',
        // 'content' => ''
    ];
    public function group()
    {
        return $this->belongsTo(LayerGroup::class, 'layer_group_id');
    }
    public function activities()
    {
        return $this->hasMany(ZonationRuleActivity::class, 'zonation_rule_id');
    }
}
