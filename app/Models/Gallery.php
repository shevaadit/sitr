<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Gallery extends MasterModel implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    public const MEDIA_COLLECTION = 'gallery';

    public const MEDIA_TYPE_FILE = 0;
    public const MEDIA_TYPE_LINK = 1;

    public const FILE_TYPE_IMAGE = 0;
    public const FILE_TYPE_VIDEO = 1;

    protected $fillable = [
        'title',
        'url',
        'file_type',
        'media_type',
        'is_published',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public static function boot()
    {
        parent::boot();
        // parent::logging();
        parent::authorizing();
    }

    protected $appends = [
        'multi_file_url',
        'file_url',
        'is_link',
    ];
    public function getFileUrlAttribute(): string
    {
        return $this->getFirstMediaUrl(static::MEDIA_COLLECTION);
    }
    public function getMultiFileUrlAttribute(): ?array
    {
        $mediaItems = $this->getMedia(static::MEDIA_COLLECTION);
        if (sizeof($mediaItems) > 1) {
            $urls = [];
            foreach ($mediaItems as $file) {
                $urls[] = $file->getFullUrl();
            }
            return $urls;
        } else {
            return null;
        }
    }
    public function getIsLinkAttribute()
    {
        return $this->media_type == static::MEDIA_TYPE_LINK;
    }
}
