<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regulation extends MasterModel
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'created_at',
        'update_at',
    ];
}
