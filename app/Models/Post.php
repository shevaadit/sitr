<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Tags\HasTags;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use App\Models\Filler\HasAuthFill;

class Post extends Model
{
    //
    use HasTags;
    use HasSlug;
    use HasAuthFill;

    public const TAGS_TYPE = 'post';

    protected $fillable = [
        'title',
        'slug',
        'content',
        'image',
        'view_count',
        'is_published',
        'created_by',
        'updated_by',
        // 'deleted_by',
        'created_at',
        'updated_at',
        // 'deleted_at',
    ];
    protected $attributes = [
        'title' => null,
        'content' => null,
        'image' => null
    ];
    protected $appends = [
        'summary',
        'image_url',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getImageUrlAttribute(): string {
        return config('app.url') . $this->image;
    }
    public function getSummaryAttribute()
    {
        return substr(strip_tags($this->content), 0, 50) . '...';
    }
    public function taglist(?string $locale = null): MorphToMany
    {
        $locale = !is_null($locale) ? $locale : app()->getLocale();

        return $this
            ->morphToMany(self::getTagClassName(), 'taggable')
            ->select('tags.id', 'tags.slug')
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(name, '$locale') as value")
            // ->selectRaw("JSON_UNQUOTE(JSON_EXTRACT(slug, '$.\"{$locale}\"')) as slug")
            ->ordered();
    }
    public static function getTags(?string $locale = null)
    {
        $locale = !is_null($locale) ? $locale : app()->getLocale();

        return \Spatie\Tags\Tag::selectRaw("JSON_EXTRACT_PATH_TEXT(name, '$locale') as value")
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(slug, '$locale') as slug")
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(slug, '$locale') as slug_value")
            ->where("type", self::TAGS_TYPE)
            ->get();
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
}
