<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Slide extends MasterModel implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    public const MEDIA_COLLECTION = 'slide';

    protected $fillable = [
        'name',
        'url',
        'is_published',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public static function boot()
    {
        parent::boot();
        // parent::logging();
        parent::authorizing();
    }

    protected $appends = [
      'image',
    ];

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl(self::MEDIA_COLLECTION);
    }
}
