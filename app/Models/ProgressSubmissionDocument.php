<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgressSubmissionDocument extends MasterModel
{
    use HasFactory;

    protected $fillable = [
        'name',
        'file',
        'progress_submission_id',
        'created_at',
        'update_at',
    ];
}
