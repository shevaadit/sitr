<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemProperties extends Model
{
    use HasFactory;

    public const APPS_KEY = 'apps';
    public const CONTACT_KEY = 'contact_admin';
    public const LIVE_CHAT_KEY = 'live_chat';
    public const CONTACT_PAGE_KEY = 'contact_page';

    public static function boot() {
        parent::boot();
        if (auth()->check()) {
            self::creating(function ($query) {
                $query->created_by = auth()->user()->id;
            });
            self::updating(function ($query) {
                $query->updated_by = auth()->user()->id;
            });
        }
    }
    protected $fillable = [
        'key',
        'value'
    ];
}
