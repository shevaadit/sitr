<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class MasterModel extends Model
{
    public static $withoutAppends = false;

    public function getFillable()
    {
        return $this->fillable;
    }
    public function scopeWithoutAppends($query)
    {
        self::$withoutAppends = true;

        return $query;
    }

    protected function getArrayableAppends()
    {
        if (self::$withoutAppends) {
            return [];
        }

        return parent::getArrayableAppends();
    }

    public function getTempTableBySelections(array $selections, string $valueKey = 'name'): string
    {
        $rowQueries = [];
        foreach ($selections as $key => $value) {
            $val = $value[$valueKey];
            $rowQueries[] = "SELECT $key AS id, '$val' AS name";
        }

        $query = implode(PHP_EOL . ' UNION ' . PHP_EOL, $rowQueries);

        return "($query)";
    }

    public function getValueBySelections($key, array $selections): ?array
    {
        return isset($key) && array_key_exists($key, $selections) ? $selections[$key] : null;
    }

    public static function logging()
    {
        self::creating(function ($query) {
            if (empty($query->attributes['created_over'])) {
                $request = request();
                $query->created_over = "{$request->ip()}  {$request->header('user-agent')}";
            }
        });
        self::updating(function ($query) {
            if (empty($query->attributes['updated_over'])) {
                $request = request();
                $query->updated_over = "{$request->ip()}  {$request->header('user-agent')}";
            }
        });
    }

    public static function authorizing()
    {
        if (auth()->check()) {
            self::creating(function ($query) {
                $query->created_by = auth()->user()->id;
            });
            self::updating(function ($query) {
                $query->updated_by = auth()->user()->id;
            });
        }
    }

    // /**
    //  * Prepare a date for array / JSON serialization.
    //  *
    //  * @param  \DateTimeInterface  $date
    //  * @return string
    //  */
    // protected function serializeDate(DateTimeInterface $date)
    // {
    //     return $date->format('Y-m-d H:i:s');
    // }
}
