<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;
use Spatie\Tags\HasTags;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Document extends MasterModel
{
    use HasTags;
    use HasFactory;
    use HasAuthFill;
    
    public const TAGS_TYPE = 'document';

    protected $fillable = [
        'id',
        'file',
        'name',
        'note',
        'mime',
        'type_id',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];
    protected $appends = [
        'file_url',
        'image_url',
    ];
    public function getFileUrlAttribute(): string {
        return config('app.url') . $this->file;
    }
    public function getImageUrlAttribute(): ?string {
        if (strpos($this->mime, 'image/') === false) return null;
        return config('app.url') . $this->file;
    }
    public function taglist(?string $locale = null): MorphToMany
    {
        $locale = ! is_null($locale) ? $locale : app()->getLocale();

        return $this
            ->morphToMany(self::getTagClassName(), 'taggable')
            ->select('tags.id', 'tags.slug')
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(name, '$locale') as value")
            // ->selectRaw("JSON_UNQUOTE(JSON_EXTRACT(slug, '$.\"{$locale}\"')) as slug")
            ->ordered();
    }
    public static function getTags(?string $locale = null) {
        $locale = ! is_null($locale) ? $locale : app()->getLocale();

        return \Spatie\Tags\Tag::select('id')
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(name, '$locale') as value")
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(slug, '$locale') as slug")
            ->selectRaw("JSON_EXTRACT_PATH_TEXT(slug, '$locale') as slug_value")
            ->where("type", self::TAGS_TYPE)
            ->get();
    }

    public function getTypeAttribute()
    {
        return self::TYPES[$this->type_id];
    }
    public function getImageAttribute()
    {
        if (strpos($this->mime, 'image/') === false) return null;
        return $this->file;
    }

    const TYPES = [
        1 => [
            'id' => 1,
            'name' => 'Public'
        ],
        2 => [
            'id' => 2,
            'name' => 'Internal'
        ]
    ];
}
