<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class IndicatorValue extends MasterModel
{
    use HasFactory;
    use HasAuthFill;
    protected $fillable = [
        'value',
        'row',
        'indicator_id',
        'config_detail_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    public function config()
    {
        return $this->belongsTo(IndicatorConfigDetail::class, 'config_detail_id');
    }
}
