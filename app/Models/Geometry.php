<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Geometry extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function categorable()
    {
        return $this->morphTo(__FUNCTION__, 'morphable_type', 'morphable_id');
    }
    public function layer()
    {
        return $this->belongsTo(Layer::class);
    }
    public function layerGroup()
    {
        return $this->hasOneThrough(
            LayerGroup::class,
            Layer::class,
            'id', //
            'id', // 
            'layer_id', //
            'layer_group_id', //
        );
    }
}
