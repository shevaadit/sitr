<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layer extends MasterModel
{
    use HasFactory;
    protected $fillable = [
        'is_published',
        'srid',
        'file',
        'region_type',
        'region_id',
        'regulation_id',
        'layer_group_id',
        'created_at',
        'update_at',
    ];

    public function getTypeAttribute()
    {
        return User::REGION_TYPES[$this->region_type];
    }
    public function region()
    {
        return $this->morphTo(__FUNCTION__, 'region_type', 'region_id');
    }
    public function group()
    {
        return $this->belongsTo(LayerGroup::class, 'layer_group_id');
    }

    public function province()
    {
        return $this->belongsTo(ProvinceFormatted::class, 'region_id')
            ->where('region_type', Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(RegencyFormatted::class, 'region_id')
            ->where('region_type', Regency::class);
    }

    public function regulation()
    {
        return $this->belongsTo(Regulation::class, 'regulation_id');
    }
}
