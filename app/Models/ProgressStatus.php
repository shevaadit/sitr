<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class ProgressStatus extends MasterModel
{
    const AUTO_APPROVE_SCORES = [
        0,
        1,
        2,
    ];
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'name',
        'note',
        'score',
        // 'type_id',
        'color',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    public function scopeProgressable($query)
    {
        return $query->where('id', '>', 0);
    }
    public function scopeRegional($query)
    {
        return $query->where('type_id', 'regional');
    }
    public function scopeDetailed($query)
    {
        return $query->where('type_id', 'detailed');
    }
}
