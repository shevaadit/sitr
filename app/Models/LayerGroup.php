<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayerGroup extends MasterModel
{
    use HasFactory;
    
    protected $fillable = [
        'has_child',
        'is_published',
        'level',
        'parent_id',
        'category_id',
        'name',
        'created_at',
        'update_at',
    ];

    protected $appends = [
        'wms'
    ];
    public function layers()
    {
        return $this->hasMany(Layer::class);
    }
    public function legends()
    {
        return $this->hasMany(LayerLegend::class);
    }
    public function category()
    {
        return $this->belongsTo(LayerCategory::class, 'category_id');
    }
    public function getWmsAttribute() {
        return config('geoserver.host').'/'.config('geoserver.path').'/'.config('geoserver.workspace').'/wms';
    }
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
    public function child()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
    public function getStyleName() {
        return "style_layer_group_{$this->id}";
    }
    public function getLayerName() {
        return "layer_group_{$this->id}";
    }
}
