<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;

class Partner extends MasterModel
{
    use HasFactory;
    protected $fillable = [
        'title',
        'url',
        'image',
        'is_published',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
    protected $appends = [
        'image_url',
    ];
    public static function boot()
    {
        parent::boot();
        parent::authorizing();
    }
    public function getImageUrlAttribute(): ?string {
        if ($this->image) {
            return config('app.url') . Storage::url($this->image);
        }
        return null;
    }
}
