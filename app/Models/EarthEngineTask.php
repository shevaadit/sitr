<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Filler\HasAuthFill;

class EarthEngineTask extends Model
{
    use HasFactory;
    use HasAuthFill;

    protected $fillable = [
        'code',
        'category',
        'filename',
        'type',
        'preview_url',
        'status',
        'created_by',
        'updated_by',
    ];
}
