<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;

class RegulationDocument extends MasterModel
{
    use HasFactory;

    protected $fillable = [
        'name',
        'file',
        'regulation_id',
        'created_at',
        'update_at',
    ];
    protected $appends = [
        'file_url'
    ];
    public function getFileUrlAttribute(): ?string {
        if ($this->file) {
            return asset($this->file);
        }
        return null;
    }
}
