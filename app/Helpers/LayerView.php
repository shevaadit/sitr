<?php

namespace App\Helpers;

use App\Models\Layer;
use App\Models\LayerAttribute;

class LayerView
{
    protected Layer $layer;

    public function __construct(Layer $layer = null) {
        $this->layer = $layer;
    }
    private function getAttributes() {
        $layerAttributes = LayerAttribute::where('layer_group_id', $this->layer->layer_group_id)
            ->get();
        $attributes = [];
        foreach ($layerAttributes as $value) {
            $attributes[] = "g.attributes->>'{$value->key}' as {$value->key}";
        }
        return implode(',', $attributes);
    }
    public function upsertView () {
        \DB::beginTransaction();
        try {
            $viewName = 'layer_'.$this->layer->id;
            \DB::connection()->getPdo()->exec("CREATE OR REPLACE VIEW {$viewName} AS
                select g.*, l.layer_group_id, {$this->getAttributes()}
                from geometries g
                join layers l on l.id = g.layer_id
                where l.id = {$this->layer->id}");
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }
}