<?php

namespace App\Helpers;

use App\Models\LayerGroup;
use App\Models\LayerAttribute;
use App\Models\LayerLegend;

class LayerGroupView
{
    protected LayerGroup $layerGroup;

    public function __construct(LayerGroup $layerGroup) {
        $this->layerGroup = $layerGroup;
    }
    private function getAttributes() {
        $layerGroupAttributes = LayerAttribute::where('layer_group_id', $this->layerGroup->id)
            ->get();
        $layerGroupLegends = LayerLegend::where('layer_group_id', $this->layerGroup->id)
            // ->groupBy('key')
            ->get();
        $attributes = [];
        $attributeKeys = [];
        foreach ($layerGroupAttributes as $value) {
            $key = strtoupper($value->key);
            $attributeKeys[] = $key;
            if ($value->data_type === 'number') {
                $attributes[] = "TO_NUMBER(g.attributes->>'{$key}', 'FM9G999D99S' ) as \"{$key}\"";
            } else {
                $attributes[] = "g.attributes->>'{$key}' as \"{$key}\"";
            }
        }
        foreach ($layerGroupLegends as $value) {
            if (in_array(strtoupper($value->key), $attributeKeys)) continue;
            $key = strtoupper($value->key);
            $attributeKeys[] = $key;
            $attributes[] = "g.attributes->>'{$key}' as \"{$key}\"";
        }
        $attributes = sizeof($attributes) ? ', '.implode(',', $attributes) : '';
        return $attributes;
    }
    public function upsertViewQuery () {
        \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS {$this->layerGroup->getLayerName()}; CREATE VIEW {$this->layerGroup->getLayerName()} AS select g.*, l.layer_group_id {$this->getAttributes()} from geometries g join layers l on l.id = g.layer_id where l.layer_group_id = {$this->layerGroup->id} AND l.is_published = 1");

    }
    public function upsertView () {
        \DB::beginTransaction();
        try {
            $this->upsertViewQuery();
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }
}