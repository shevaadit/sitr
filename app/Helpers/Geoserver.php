<?php

namespace App\Helpers;

use App\Models\Layer;
use App\Models\LayerGroup;
use App\Models\ProgressStatus;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Storage;

class Geoserver
{
    private $path;
    private $workspace;
    private $datastore;
    private $username;
    private $password;
    private $native_crs;
    private $client;
    private $requestConfig;
    private $url;
    private $srid;

    /**
     * Create new Geoserver Object
     *
     * @param  string  $response_type
     * @return void
     */
    public function __construct()
    {
        $this->path = config('geoserver.rest').'/'.config('geoserver.path');

        $this->workspace = $this->path.'/rest/workspaces/'.config('geoserver.workspace');
        $this->datastore = $this->workspace.'/datastores/'.config('geoserver.datastore');
        $this->url = $this->datastore;

        $this->username = config('geoserver.username');
        $this->password = config('geoserver.password');
        $this->native_crs = config('webgis.srtext');
        $this->srid = config('webgis.srid');

        $this->client = new Client();

        $this->requestConfig = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'auth' => [$this->username, $this->password],
        ];

    }

    public function publishLayer(LayerGroup $group)
    {

        $layerBody['featureType'] = [
            'name' => $group->getLayerName(),
            'nativeName' => $group->getLayerName(),
            'title' => $group->name,
            'keywordatastore' => [
                'string' => ['features', $group->getLayerName()]
            ],
            'nativeCRS' => $this->native_crs,
            'srs' => 'EPSG:'.$this->srid,
            'projectionPolicy' => 'FORCE_DECLARED',
            'maxFeatures' => 0,
            'numDecimals' => 0,
            'overridingServiceSRS' => false,
            'skipNumberMatched' => false,
            'circularArcPresent' => false
        ];
        $requestUrl = $this->url . '/featuretypes';

        $this->requestConfig['body'] = json_encode($layerBody);

        $this->client->request('POST', $requestUrl, $this->requestConfig);
    }
    public function updateLayer(LayerGroup $group)
    {
        $layerBody['featureType'] = [
            'name' => $group->getLayerName(),
            'nativeName' => $group->getLayerName(),
            'title' => $group->name,
            'keywordatastore' => [
                'string' => ['features', $group->getLayerName()]
            ],
            'nativeCRS' => $this->native_crs,
            'srs' => 'EPSG:'.$this->srid,
            'projectionPolicy' => 'FORCE_DECLARED',
            'maxFeatures' => 0,
            'numDecimals' => 0,
            'overridingServiceSRS' => false,
            'skipNumberMatched' => false,
            'circularArcPresent' => false
        ];
        $requestUrl = $this->url . '/featuretypes/' . $group->getLayerName() . '?recalculate=nativebbox,latlonbbox';

        $this->requestConfig['body'] = json_encode($layerBody);

        $this->client->request('PUT', $requestUrl, $this->requestConfig);
    }

    public function insertStyle(LayerGroup $group)
    {
        $name = 'style_layer_group_' . $group->id;

        $body['style'] = ['name' => $name, 'filename' => $name.'.sld'];

        $requestUrl = $this->workspace.'/styles';

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/json'
        ];
        $this->requestConfig['body'] = json_encode($body);

        return $this->client->request('POST', $requestUrl, $this->requestConfig);
    }
    public function updateStyle(LayerGroup $group)
    {
        // $content_type = ['1.0.0' => 'application/vnd.ogc.sld+xml', '1.1.0' => 'application/vnd.ogc.se+xml'];

        $name = 'style_layer_group_' . $group->id;

        $requestUrl = $this->workspace.'/styles/'.$name;

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/vnd.ogc.se+xml'
        ];
        $this->requestConfig['body'] = view('styles', compact('group'))->render();

        $this->client->request('PUT', $requestUrl, $this->requestConfig);
    }
    public function applyStyle($layer,  $layerStyle)
    {
        $body['layer'] = ['defaultStyle' => ['name' => config('geoserver.workspace').':'.$layerStyle]];

        $requestUrl = $this->workspace.'/layers/'.$layer;

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/json'
        ];
        $this->requestConfig['body'] = json_encode($body);

        $this->client->request('PUT', $requestUrl, $this->requestConfig);
    }

    public function insertStyleProgress($type)
    {
        $name = 'style_progress_' . $type;

        $body['style'] = ['name' => $name, 'filename' => $name.'.sld'];

        $requestUrl = $this->workspace.'/styles';

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/json'
        ];
        $this->requestConfig['body'] = json_encode($body);

        return $this->client->request('POST', $requestUrl, $this->requestConfig);
    }

    public function publishLayerProgress($type)
    {

        $name = 'layer_progress_' . $type;

        $layerBody['featureType'] = [
            'name' => $name,
            'nativeName' => $name,
            'title' => $name,
            'keywordatastore' => [
                'string' => ['features', $name]
            ],
            'nativeCRS' => $this->native_crs,
            'srs' => 'EPSG:'.$this->srid,
            'projectionPolicy' => 'FORCE_DECLARED',
            'maxFeatures' => 0,
            'numDecimals' => 0,
            'overridingServiceSRS' => false,
            'skipNumberMatched' => false,
            'circularArcPresent' => false
        ];
        $requestUrl = $this->url . '/featuretypes';

        $this->requestConfig['body'] = json_encode($layerBody);

        $this->client->request('POST', $requestUrl, $this->requestConfig);
    }
    public function updateStyleProgress($type)
    {
        // $content_type = ['1.0.0' => 'application/vnd.ogc.sld+xml', '1.1.0' => 'application/vnd.ogc.se+xml'];

        $name = 'style_progress_' . $type;

        $requestUrl = $this->workspace.'/styles/'.$name;

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/vnd.ogc.se+xml'
        ];
        $statuses = ProgressStatus::progressable()->get();
        $this->requestConfig['body'] = view('styles-progress-'.$type, compact('statuses', 'type', 'name'))->render();

        $this->client->request('PUT', $requestUrl, $this->requestConfig);
    }
    public function publishLayerIndicator($indicator)
    {

        $layerBody['featureType'] = [
            'name' => $indicator->getLayerName(),
            'nativeName' => $indicator->getLayerName(),
            'title' => $indicator->name,
            'keywordatastore' => [
                'string' => ['features', $indicator->getLayerName()]
            ],
            'nativeCRS' => $this->native_crs,
            'srs' => 'EPSG:'.$this->srid,
            'projectionPolicy' => 'FORCE_DECLARED',
            'maxFeatures' => 0,
            'numDecimals' => 0,
            'overridingServiceSRS' => false,
            'skipNumberMatched' => false,
            'circularArcPresent' => false
        ];
        $requestUrl = $this->url . '/featuretypes';

        $this->requestConfig['body'] = json_encode($layerBody);

        $this->client->request('POST', $requestUrl, $this->requestConfig);
    }
    public function insertStyleIndicator($config)
    {
        $name = $config->getStyleName();

        $body['style'] = ['name' => $name, 'filename' => $name.'.sld'];

        $requestUrl = $this->workspace.'/styles';

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/json'
        ];
        $this->requestConfig['body'] = json_encode($body);

        return $this->client->request('POST', $requestUrl, $this->requestConfig);
    }
    public function updateStyleIndicator($config, $data)
    {
        // $content_type = ['1.0.0' => 'application/vnd.ogc.sld+xml', '1.1.0' => 'application/vnd.ogc.se+xml'];

        $name = $config->getStyleName();

        $requestUrl = $this->workspace.'/styles/'.$name;

        $this->requestConfig['headers'] = [
            'Content-Type' => 'application/vnd.ogc.se+xml'
        ];
        $this->requestConfig['body'] = view('styles-indicator', $data)->render();

        $this->client->request('PUT', $requestUrl, $this->requestConfig);
    }
}
