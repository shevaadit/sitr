<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Geoserver;

class MakeProgressViewRegional extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:progress-view-regional';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CREATE PROGRESS VIEW REGIONAL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        try {
            $helper = new Geoserver();
            \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS layer_progress_regional; CREATE VIEW layer_progress_regional AS select * from progress where type_id = 'regional'");
            $helper->insertStyleProgress('regional');
            $helper->publishLayerProgress('regional');
            $helper->applyStyle('layer_progress_regional', 'style_progress_regional');
            return 0;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
