<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Progress;
use App\Models\ProgressSubmission;
use App\Models\ProgressStatus;
use App\Models\ProgressSubmissionDocument;


class GenerateProgessSubmissionDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:progress-submission-doc {--type=regional} {--filepath=/progress-submission-docs/default.pdf}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = $this->option('type');
        $filepath = $this->option('filepath');
        $submissions = ProgressSubmission::whereHas('progress', function($query) use($type) {
                $query->where('type_id', $type);
            })
            ->whereDoesntHave('documents')
            ->get();
        \DB::beginTransaction();
        try {
            foreach ($submissions as $key => $submission) {
                ProgressSubmissionDocument::create([
                    'progress_submission_id' => $submission->id,
                    'name' => $submission->progressStatus->name,
                    'file' => $filepath,
                ]);
            }
            \DB::commit();
            return 1;
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }
}
