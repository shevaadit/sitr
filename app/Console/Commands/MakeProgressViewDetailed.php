<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Geoserver;

class MakeProgressViewDetailed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:progress-view-detailed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CREATE PROGRESS VIEW DETAILED';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        try {
            $helper = new Geoserver();
            \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS layer_progress_detailed; CREATE VIEW layer_progress_detailed AS select * from progress where type_id = 'detailed'");
            $helper->insertStyleProgress('detailed');
            $helper->publishLayerProgress('detailed');
            $helper->applyStyle('layer_progress_detailed', 'style_progress_detailed');
            return 0;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
