<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Progress;
use App\Models\ProgressSubmission;
use App\Models\ProgressStatus;

class SubmitProgressRegional extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'submit:progress-regional {--id=?} {--level=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $id = $this->option('id');
            $level = $this->option('level');
            $progress = Progress::findOrFail($id);
            $statuses = ProgressStatus::regional()
                ->where('score', '<=', $level)
                ->where('score', '>', 0)
                ->orderBy('score', 'asc')
                ->get();
            
            $prevStatusId = $progress->last_progress_status_id; 
            foreach ($statuses as $key => $status) {
                $hasSubmission = ProgressSubmission::where('progress_id', $progress->id)
                    ->where('progress_status_id', $status->id)
                    ->exists();
                
                if ($hasSubmission) {
                    $prevStatusId = $status->id;
                    continue;
                } 

                ProgressSubmission::create([
                    'progress_id' => $progress->id,
                    'progress_status_id' => $status->id,
                    'prev_progress_status_id' => $prevStatusId,
                    'submission_person_name' => '_',
                    'submission_person_position' => '_',
                    'note' => '_',
                    'status' => 1,
                    'created_by' => 1,
                ]);
                $prevStatusId = $status->id;
            }
            $progress->update([
                'last_progress_status_id' => $prevStatusId
            ]);
            \DB::commit();
            return 1;
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }
}
