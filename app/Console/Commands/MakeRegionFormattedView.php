<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeRegionFormattedView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:region-view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS province_formatteds; CREATE VIEW province_formatteds AS select id::int8, name from provinces;");
            \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS regency_formatteds; CREATE VIEW regency_formatteds AS select id::int8, name, province_id::int8 from regencies;");
            \DB::connection()->getPdo()->exec("DROP VIEW IF EXISTS district_formatteds; CREATE VIEW district_formatteds AS select id::int8, name, regency_id::int8 from districts;");
            return 0;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
