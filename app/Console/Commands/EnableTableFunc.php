<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EnableTableFunc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tablefunc:enable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable PostgreSQL Table Function';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            \DB::connection()->getPdo()->exec("CREATE EXTENSION IF NOT EXISTS tablefunc;");
            $this->info('Enabling tablefunc was successful!');
            return 0;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
