<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Madnest\Madzipper\Madzipper;

class SameFilename implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $full_path = $value->getRealPath();

        $zipper = new Madzipper;
        $zipper->make($full_path);
        $layerFile = $zipper->listFiles('/\.(dbf|prj|shp|shx)$/i');

        $arr_filename = [];

        foreach ($layerFile as $filepath) {
            $pathinfo = pathinfo($filepath);
            $arr_filename[] = $pathinfo['dirname'].DIRECTORY_SEPARATOR.$pathinfo['filename'];
        }

        return (count(array_unique($arr_filename)) == 1);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ada file di dalam zip yang memiliki nama atau dalam folder yang berbeda.';
    }
}
