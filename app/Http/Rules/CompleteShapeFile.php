<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Madnest\Madzipper\Madzipper;

class CompleteShapeFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $full_path = $value->getRealPath();

        $zipper = new Madzipper;
        $zipper->make($full_path);
        $layerFile = $zipper->listFiles('/\.(dbf|prj|shp|shx)$/i');

        return (count($layerFile) == 4);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Isi berkas zip tidak lengkap (.prj, .dbf, .shp, .shx).';
    }
}
