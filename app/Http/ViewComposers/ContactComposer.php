<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\SystemProperties;

class ContactComposer
{
    protected $contact;

    public function __construct()
    {
        $contact = SystemProperties::select('value')
            ->where('key', SystemProperties::CONTACT_KEY)
            ->value('value');
        $this->contact = (object) json_decode($contact, true);
    }

    /**
    * Bind data to the view.
    *
    * @param  View  $view
    * @return void
    */
    public function compose(View $view)
    {
        $view->with('contact', $this->contact);
    }
}