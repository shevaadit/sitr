<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\SystemProperties;

class LiveChatComposer
{
    protected $tawkto = '';

    public function __construct()
    {
        $val = SystemProperties::select("value->tawkto as val")
            ->where('key', SystemProperties::LIVE_CHAT_KEY)
            ->value('val');
        $this->tawkto = $val;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('tawkto', $this->tawkto);
    }
}
