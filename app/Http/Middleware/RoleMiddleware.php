<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$allowedLevels)
    {
        if (is_array($allowedLevels) && auth()->check() && in_array(auth()->user()->level, $allowedLevels)) {
            return $next($request);
        }

        if ($request->expectsJson()) {
            return response()->json([
                'error' => 'permission denied.',
            ], 403);
        }
        return route('login');
    }
}
