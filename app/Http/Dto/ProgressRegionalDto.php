<?php

namespace App\Http\Dto;

use App\Models\Progress;
use App\Models\Regency;
use Exception;

class ProgressRegionalDto extends Progress
{
    protected $table = 'progress';
    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        if (!auth()->check()) throw new Exception();

        // TODO: move to scope if posible
        $user = auth()->user();
        if ($user->level === 'admin') {
            return $this->where('region_type', Regency::class)
                ->where('region_id', $user->region_id)
                ->findOrFail($value);
        }

        return $this->where('region_type', Regency::class)
            ->findOrFail($value);
    }
}
