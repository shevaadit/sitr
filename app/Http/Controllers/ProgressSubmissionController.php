<?php

namespace App\Http\Controllers;

use App\Models\ProgressSubmission;
use App\Models\ProgressStatus;
use App\Models\Progress;
use Illuminate\Http\Request;
use App\Helpers\ProgressView;
use App\Helpers\Geoserver;

class ProgressSubmissionController extends MasterController
{
    public function __construct()
    {
        parent::__construct(ProgressSubmission::class);
    }

    public function add($id, Request $request)
    {
        $header = Progress::findOrFail($id);
        $statuses = ProgressStatus::where('type_id', $header->type_id)->get();
        $lastStatus = $header->lastStatus;
        return compact('statuses', 'lastStatus');
    }
    private function getHeader($id)
    {
        $query = Progress::query();
        $user = auth()->user();
        // TODO: check in mode detailed
        if ($user->level === 'admin') {
            $query->where('region_id', $user->region_id);
        }
        return $query->findOrFail($id);
    }
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $header = $this->getHeader($id);
            $status = ProgressStatus::findOrFail($this->request->progress_status_id);

            $this->runDBTransaction(function () use ($header, $status) {
                $fields = $this->getFillableFields();
                $fields['progress_id'] = $header->id;
                if (in_array($status->score, ProgressStatus::AUTO_APPROVE_SCORES)) {
                    $fields['status'] = 1;
                    $fields['checked_date'] = now();
                    $fields['checked_by'] = auth()->user()->id;
                }
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $header->last_progress_status_id = $status->id;
                $header->saveOrFail();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->with('progressStatus:id,name')
            // ->with('prevStatus:id,name')
            ->with('prevProgressStatus:id,name')
            ->with('documents')
            ->selectRaw('false as _edited')
            ->where('progress_id', $id)
            ->get();
        return compact('listData');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgressSubmission  $progressSubmission
     * @return \Illuminate\Http\Response
     */
    public function show(ProgressSubmission $progressSubmission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgressSubmission  $progressSubmission
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgressSubmission $progressSubmission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgressSubmission  $progressSubmission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgressSubmission $progressSubmission)
    {
        //
        $this->model = $progressSubmission;
        $this->request = $request;
        try {
            $this->runDBTransaction(function () {
                $fields = $this->getFillableFields();
                $this->model->fill($fields);
                $this->model->saveOrFail();
                // $helper = new ProgressView($this->model->group);
                // $helper->upsertViewQuery();
            });
            $helper = new Geoserver();
            $helper->updateLayer($this->model->group);
            return $this->responseSuccess();
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->responseError($e->getResponse());
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgressSubmission  $progressSubmission
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgressSubmission $progressSubmission)
    {
        //
        $this->model = $progressSubmission;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
