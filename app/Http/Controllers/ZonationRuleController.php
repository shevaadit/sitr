<?php

namespace App\Http\Controllers;

use App\Models\ZonationRule;
use App\Models\Regulation;
use App\Models\LayerGroup;

use Illuminate\Http\Request;

class ZonationRuleController extends MasterController
{

    public function __construct () {
        parent::__construct(ZonationRule::class);
    }

    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $header = LayerGroup::findOrFail($id);
            $this->runDBTransaction(function() use($header) {
                $fields = $this->getFillableFields();
                $fields['layer_group_id'] = $header->id;
                $this->model->fill($fields);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess();
         } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->withCount('activities')
            ->with('activities:id,zonation_rule_id,rule,name')
            ->where('layer_group_id', $id)
            ->get();
        // $dataTypes = array_values($this->modelClass::DATA_TYPES);
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData');
    }
    
    public function compilate(ZonationRule $formData = null) {
        $regulations = Regulation::select('id', 'name')->get();
        return compact('formData', 'regulations');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $data = $this->modelClass::paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ZonationRule  $zonationRule
     * @return \Illuminate\Http\Response
     */
    public function show(ZonationRule $zonationRule)
    {
        //
        return ['formData' => $zonationRule];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ZonationRule  $zonationRule
     * @return \Illuminate\Http\Response
     */
    public function edit(ZonationRule $zonationRule)
    {
        //
        return $this->compilate($zonationRule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ZonationRule  $zonationRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ZonationRule $zonationRule)
    {
        //
        $this->model = $zonationRule;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ZonationRule  $zonationRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ZonationRule $zonationRule)
    {
        //
        $this->model = $zonationRule;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
