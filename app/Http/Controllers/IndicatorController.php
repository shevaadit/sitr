<?php

namespace App\Http\Controllers;

use App\Models\Indicator;
use App\Models\IndicatorValue;
use App\Models\IndicatorConfig;
use App\Models\IndicatorConfigDetail;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Helpers\Geoserver;

class IndicatorController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $indexable = [
        'id' => 'id',
        'name' => 'name',
        'is_published' => 'is_published',
    ];
    private $row = 1;
    private $helper;

    public function __construct () {
        parent::__construct(Indicator::class);
        $this->helper = new Geoserver();
    }
    
    public function compilate(Indicator $formData = null) {
        $configs = IndicatorConfig::select('id', 'name')->get();
        return compact('configs', 'formData');
    }
    
    public function index()
    {
        //
        $query = $this->modelClass::select(array_values($this->getSelectable())); 
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);
        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function getLineValue(&$line, $key) {
        if (array_key_exists($key, $line)) {
            $value = $line[$key];
            if (!empty($value)) {
                return $value;
            }
        }
        return 0;
    }
    private function getFetchValues(&$keys) {
        return " $$ VALUES ('" . implode("'::text), ('", $keys) . "'::text) $$ ";
    }
    private function getFetchColumns(&$keys) {
        return ' ("ROW" int, "INDICATOR_ID" int, "'. implode('" varchar, "', $keys) . '" varchar) ';
    }
    private function getFetchQuery($id, &$keys) {

        $query = "SELECT iv.row, iv.indicator_id, icd.key, iv.value AS val
            FROM indicator_values iv 
            JOIN indicator_config_details icd 
            ON iv.config_detail_id = icd.id
            WHERE iv.indicator_id = $id
            ORDER BY 1";
        $values = $this->getFetchValues($keys);
        $columns = $this->getFetchColumns($keys);

        return "SELECT * FROM crosstab('$query', $values) AS val $columns";
    }
    private function getKeys() {
        $configs = IndicatorConfigDetail::where('indicator_config_id', $this->model->config_id)
            ->get();
        $keys = $configs->pluck('key')->toArray();
        return $keys;
    }
    public function fetch($id) {
        $this->model = $this->modelClass::where('id', $id)
            ->firstOrFail();
        $configs = IndicatorConfigDetail::where('indicator_config_id', $this->model->config_id)
            ->get();
        $keys = $this->getKeys();

        $fields = $configs->map(function($item) {
            return [
                'key' => $item->key,
                'sortable' => true,
                'label' => $item->name
            ];
        });
        // $query = "SELECT iv.row, iv.indicator_id, icd.key, iv.value AS val
        //     FROM indicator_values iv 
        //     JOIN indicator_config_details icd 
        //     ON iv.config_detail_id = icd.id
        //     WHERE iv.indicator_id = $id
        //     ORDER BY 1";
        // $values = $this->getFetchValues($keys);
        // $columns = $this->getFetchColumns($keys);

        $items = \DB::select($this->getFetchQuery($id, $keys));
        
        return compact('items', 'fields');
    }

    private function getViewQuery($id, &$keys) {

        $query = "SELECT iv.row, iv.indicator_id, icd.key, iv.value AS val
            FROM indicator_values iv 
            JOIN indicator_config_details icd 
            ON iv.config_detail_id = icd.id
            WHERE iv.indicator_id = $id
            ORDER BY 1";
        $values = $this->getFetchValues($keys);
        $columns = $this->getFetchColumns($keys);
        $label = 'KAB_KOTA';

        return "SELECT val.*, r.id, rg.geometry  FROM crosstab('$query', $values) AS val $columns
            LEFT JOIN regencies r on LOWER(val.\"{$label}\") = LOWER(r.name)
            LEFT JOIN regency_geometries rg on r.id = rg.regency_id";
    }
    public function upsertView () {
        \DB::beginTransaction();
        try {
            $viewName = $this->model->getLayerName();
            $keys = $this->getKeys();
            $query = $this->getViewQuery($this->model->id, $keys);

            \DB::connection()->getPdo()->exec("CREATE OR REPLACE VIEW {$viewName} AS {$query}");
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }
    public function store(Request $request) // [IMPORTANT] Need Refactor
    {
        //
        $this->model = new $this->modelClass();
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                $fields['config_id'] = $this->request->config_id;
                $fields['file'] = 1;
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->model->id = \DB::getPdo()->lastInsertId();
                $configs = IndicatorConfigDetail::where('indicator_config_id', $this->request->config_id)
                    ->get();
                (new FastExcel)->import($this->request->file, function ($line) use($configs) {
                    foreach ($configs as $config) {
                        IndicatorValue::create([
                            'indicator_id' => $this->model->id,
                            'row' => $this->row,
                            'config_detail_id' => $config->id, 
                            'value' => $this->getLineValue($line, $config->key), 
                        ]);
                    }
                    $this->row++;
                });
            });
            $this->upsertView();
            $this->helper->publishLayerIndicator($this->model);
            $id = $this->model->id;
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function show(Indicator $indicator)
    {
        //
        return $indicator->load('config:id,name');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function edit(Indicator $indicator)
    {
        //
        return $this->compilate($indicator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Indicator $indicator)
    {
        //
        $this->model = $indicator;
        
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Indicator $indicator)
    {
        $this->model = $indicator;
        try {
            $this->runDBTransaction(function() {
                IndicatorValue::where('indicator_id', $this->model->id)
                    ->delete();
                $this->model->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
