<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function runDBTransaction(callable $callback, bool $getInsertId = false) {
        \DB::beginTransaction();
        try {
            $callback();
            \DB::commit();
            if ($getInsertId) return \DB::getPdo()->lastInsertId();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
        }
    }

    protected function responseSuccess($message = "Success.", $data = null) {
        $response = [
            "success" => true,
            "message" => $message
        ];
        if (isset($data)) {
            $response = array_merge($data, $response);
        }
        return response()->json($response);
    }
    protected function responseError($error = "Error.") {
        if ($error instanceof \Throwable) {
            $message = $error->getMessage();
        } else {
            $message = $error;
        }
        $response = [
            'error' => true,
            'message' => $message,
        ];
        if (isset($data)) {
            $response = array_merge((array) $data, $response);
        }
        if (!$this->isProduction() && $error instanceof \Throwable) {
            $response['stacktrace'] = $error->getTrace();
        }

        return response()->json($response, 500);
    }
    protected function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }
    
        if (!is_dir($dir)) {
            return unlink($dir);
        }
    
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
    
            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
    
        }
    
        return rmdir($dir);
    }
    protected function isProduction(): bool
    {
        return config('app.env') === 'production';
    }
}
