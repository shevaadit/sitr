<?php

namespace App\Http\Controllers;

use App\Models\IndicatorConfig;
use App\Models\IndicatorConfigDetail;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class IndicatorConfigController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $indexable = [
        'id' => 'id',
        'name' => 'name',
    ];
    public function __construct () {
        parent::__construct(IndicatorConfig::class);
    }

    
    public function compilate(IndicatorConfig $formData = null) {
        return compact('formData');
    }
    
    public function index()
    {
        //
        $query = $this->modelClass::select(array_values($this->getSelectable())); 
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);
        $data = $query->paginate(10);
        return $data;
    }
    public function excel($id) {
        $configs = IndicatorConfigDetail::where('indicator_config_id', $id)
            ->orderBy('is_label', 'desc')
            ->get();
        $header = [];
        foreach ($configs as $value) {
            $header[$value->key] = '';
        }
        $header = collect([$header]);
        return (new FastExcel($header))->download('kerangka-' . date("Y-m-d_H:i:s") . '.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IndicatorConfig  $indicatorConfig
     * @return \Illuminate\Http\Response
     */
    public function show(IndicatorConfig $indicatorConfig)
    {
        //
        return $indicatorConfig;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IndicatorConfig  $indicatorConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(IndicatorConfig $indicatorConfig)
    {
        //
        return $this->compilate($indicatorConfig);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IndicatorConfig  $indicatorConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndicatorConfig $indicatorConfig)
    {
        //
        $this->model = $indicatorConfig;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IndicatorConfig  $indicatorConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndicatorConfig $indicatorConfig)
    {
        //
        $this->model = $indicatorConfig;
        try {
            $this->runDBTransaction(function() {
                IndicatorConfigDetail::where('indicator_config_id', $this->model->id)
                    ->delete();
                $this->model->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
