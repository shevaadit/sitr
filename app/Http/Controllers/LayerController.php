<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Layer;
use App\Models\Regency;
use App\Models\Province;
use App\Models\RegencyGeometry;
use App\Models\Geometry;
use App\Models\Regulation;
use App\Models\LayerGroup;
use Illuminate\Http\Request;

use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;
use Shapefile\ShapefileWriter;

use App\Http\Rules\CompleteDatafile;
use App\Http\Rules\SameFilename;
use Madnest\Madzipper\Madzipper;
use App\Http\Rules\CompleteShapeFile;

class LayerController extends MasterController
{

    protected $indexable = [
        'id' => 'layers.id',
        'is_published' => 'layers.is_published',
        'region_id' => 'layers.region_id',
        'region_type' => 'layers.region_type',
        'layer_group_id' => 'layer_group_id',
        'regulation_id' => 'regulation_id',
        'layer_group_name' =>'layer_group.name',
        'regulation_name' => 'regulation.name'
    ];
    public function __construct () {
        parent::__construct(Layer::class);
    }
    public function compilate(Layer $formData = null) {

        $groups = LayerGroup::select('id', 'name')->get();
        $regulations = Regulation::select('id', 'name')->get();
        $regionTypes = array_values(User::REGION_TYPES);

        return compact('formData', 'groups', 'regulations', 'regionTypes');
    }
    public function getSrid(Request $request) {
        $data = \DB::table('spatial_ref_sys')
            ->where('srid', 'ilike', '%'.$request->search.'%')
            ->select(\DB::raw('srid as id'), \DB::raw('auth_name as name'))
            ->take(10)
            ->get();
        return $data;
    }
    public function getRegions(Request $request) {
        if ($request->id && array_key_exists($request->id, User::REGION_TYPES)) {
            return $request->id::select('id', 'name')->get();
        }
        return [];
    }
    public function getShapefile($id) {
        try {
            $this->model = $this->modelClass::findOrfail($id);
            return response()->file(storage_path($this->model->file));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function geojson($id) {
        $data = \DB::select("SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geometry)::jsonb,
                'properties', to_jsonb(row) - 'attributes' - 'geometry'
            ) AS feature
            FROM (
                SELECT geometries.*, regencies.name as regency_name 
                FROM geometries 
                LEFT JOIN regencies 
                    ON regencies.id::int8 = geometries.morphable_id::int8
                WHERE geometries.layer_id = ?) row) features;"
        , [$id]);

        return $data[0]->jsonb_build_object;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function indexingJoin(&$query) {
        return $query->join('layer_groups as layer_group', 'layers.layer_group_id', '=', 'layer_group.id')
            ->join('regulations as regulation', 'layers.regulation_id', '=', 'regulation.id');
    }
    
    public function index()
    {
        $sortMaps = [
            'group' => 'layer_group_id',
            'regulation' => 'regulation_id',
            'region' => 'region_id'
        ];
        // return get_class($this->indexable[$this->request->searchKey]);
        $selectable = $this->getSelectable($this->indexable);
        $query = $this->modelClass::select(array_values($selectable))
            ->with('region:id,name')
            ->when($this->request->searchKey === 'region', function($query) {
                if (empty($this->request->searchVal)) return;
                $query->whereHas('regency', function($query) {
                    $query->where('name', 'ilike', '%'.$this->request->searchVal.'%');
                })->orWhereHas('province', function($query) {
                    $query->where('name', 'ilike', '%'.$this->request->searchVal.'%');
                });
            })
            ->has('group'); 
        $this->searchByIndexable($query);
        $this->indexingJoin($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, Layer $layer) {
        $file           = $request->file("file");
        $filename       = $file->hashName();
        $filepath       = DIRECTORY_SEPARATOR.'layer'.DIRECTORY_SEPARATOR;
        // if (!file_exists($extractpath)) {
            //     mkdir($extractpath, 0755, true);
            // }
        $isUploaded = $file->move(storage_path($filepath), $filename);
            // return $;
        // return $file;
        if ($isUploaded) {
            $zipper = new Madzipper;
                
            $zipper->make($isUploaded->getPathname());
            $extractpath = $isUploaded->getPathname().'.extract'.DIRECTORY_SEPARATOR;
            $fixpath = $extractpath.'fix';
    
            $layerFile = $zipper->listFiles('/\.(dbf|prj|shp|shx)$/i');
            $shapeFile = $zipper->listFiles('/\.(shp)$/i');
            $shape_filename = [];
            // $arr_basename = [];
    
            foreach ($shapeFile as $path) {
                $shape_filename[] = pathinfo($path, PATHINFO_FILENAME);
            //     $arr_basename[] = pathinfo($path, PATHINFO_BASENAME);
            }
    
            $zip_folder = dirname($layerFile[0]);
    
            if ($zip_folder != '.') {
                $zipper->folder($zip_folder);
            }
            // return $zip_folder;
    
            $zipper->extractMatchingRegex($extractpath, '/\.(dbf|prj|shp|shx)$/i');
            $zipper->close();

            // Open Shapefile
            $Shapefile = new ShapefileReader($extractpath.$shape_filename[0].'.shp', [
                Shapefile::OPTION_POLYGON_ORIENTATION_READING_AUTOSENSE => true,
            ]);

            // Read all the records
            $json = [];
            $wkt = [];
            $data = [];
            $properties = [];
            $boundingBoxes = [];
            try {
                foreach ($Shapefile as $i => $geometry) {
                // Skip the record if marked as "deleted"
                    if ($geometry->isDeleted()) {
                        continue;
                    }
                    $attributes = $geometry->getDataArray();
                    // throw new \Exception();
                    // throw new \Exception(json_encode($attributes));
                    // $name = $attributes[$layer->group->category->key];
                    // $name = str_replace('Kab. ', '', $name);
                    // $name = strpos($name, 'Kota') !== false ? $name : 'Kabupaten ' . $name;
                    // $name = strtoupper($name);


                    // $regency = Regency::where('name', $name)
                    //     ->first();
                    // $regency = Regency::where('id', )
                    //     ->first();
                    // if (!$regency) continue;
                    // $geometry->getArray();
                    
                    if ($geometry->isZ()) {
                        $g = $geometry->getWKT();
                        $g = explode(', ', $g);
                        $g = implode('|', $g);
                        $g = str_replace(',', '.', $g);
                        $g = str_replace('|', ', ', $g);
                        $json[$layer->region_id][] = $g;
                    } else {
                        $json[$layer->region_id][] = $geometry->getWKT();
                    }
                    $properties[$layer->region_id][] = $attributes;
                    // $boundingBoxes[$regency->id][] = $geometry->getBoundingBox();
                    // $data[$layer->region_id] = array_key_exists($layer->region_id, $data) ? array_merge($attributes, $data[$layer->region_id]) : $attributes;
                }
                foreach ($json as $key => $value) {
                    // $fields['geometry'] = \DB::raw("ST_Collect(ARRAY[ST_GeomFromGeoJSON('".implode("'), ST_GeomFromText('", $value) ."')])");
                    foreach($value as $index => $item) {
                        $geometry = new Geometry();
                        $fields = [];
                        $fields['morphable_id'] = $key;
                        $fields['morphable_type'] = $layer->region_type;
                        $fields['attributes'] = json_encode($properties[$key][$index]);
                        
                        $fields['geometry'] = \DB::raw("ST_Transform(ST_SetSRID(ST_Force2D(ST_GeomFromEWKT('" . $item. "')), ".$request->srid."), 4326)");
                        $fields['layer_id'] = $layer->id;

                        $data[] = $fields;
                        $geometry->fill($fields);
                        $geometry->saveOrFail();
                    }
                }
            } catch (\Throwable $th) {
                $this->deleteDirectory(storage_path($filepath.$filename));
                throw $th;
            } finally {
                $this->deleteDirectory($extractpath);
            }
        }
        return $filepath.$filename;
    }
    public function store(Request $request)
    {
        //
        $request->validate([
            'file' => [
                'bail',
                'required',
                'file',
                'mimes:zip',
                new CompleteShapeFile()
            ]
        ]);

        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->model->id = \DB::getPdo()->lastInsertId();
                $this->model->file = $this->upload($this->request, $this->model);
                $this->model->saveOrFail();

                // $this->model->file 
            });

            return $this->responseSuccess('Success', ["id" => $this->model->id]);
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Regency  $layer
     * @return \Illuminate\Http\Response
     */

    public function show(Layer $layer)
    {
        //
        return $layer->load('group', 'regulation')
            ->setAppends(['type']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Layer  $layer
     * @return \Illuminate\Http\Response
     */

    public function edit(Layer $layer)
    {
        //
        return $this->compilate($layer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Regency  $layer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Layer $layer)
    {
        //
        $this->model = $layer;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Regency  $layer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Layer $layer)
    {
        //
        try {
            $this->runDBTransaction(function() use($layer) {
                Geometry::where('layer_id', $layer->id)
                    ->delete();
                $layer->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
