<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Models\SystemProperties;

class LiveChatController extends SettingController
{
    public const KEY = SystemProperties::LIVE_CHAT_KEY;

    protected array $fillable = [
        'tawkto',
    ];

    public function read(Request $request)
    {
        return $this->reading($request);
    }

    public function alter(Request $request)
    {
        return $this->altering($request);
    }
}
