<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;

class ContactPageController extends SettingController
{
    public const KEY = 'contact_page';

    protected array $fillable = [
        'content',
    ];

    public function read(Request $request)
    {
        return $this->reading($request);
    }

    public function alter(Request $request)
    {
        return $this->altering($request);
    }
}
