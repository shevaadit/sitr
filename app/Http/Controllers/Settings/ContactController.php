<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Models\SystemProperties;
class ContactController extends SettingController
{
    public const KEY = SystemProperties::CONTACT_KEY;

    protected array $fillable = [
        'whatsapp',
        'instagram',
        'facebook',
        'email',
        'youtube',
        'twitter',
    ];

    public function read(Request $request)
    {
        return $this->reading($request);
    }

    public function alter(Request $request)
    {
        return $this->altering($request);
    }
}
