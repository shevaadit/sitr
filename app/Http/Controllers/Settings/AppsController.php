<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Http\Request;
use App\Models\SystemProperties;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Storage;
class AppsController extends SettingController
{
    public const KEY = SystemProperties::APPS_KEY;

    protected array $fillable = [
        'version',
        'url',
    ];

    public function read(Request $request)
    {
        return $this->reading($request);
    }

    protected function getValues(Request $request)
    {
        $values = parent::getValues($request);
        if ($request->hasFile('file')) {
            $dir = 'public/apps';
            $file = $request->file('file');
            // TODO: check why apk file detected ad zip
            // $filename = $file->hashName() . "." . $file->extension(); 
            $filename = $file->hashName() . ".apk"; 
            $path = Storage::putFileAs(
                $dir,
                $file,
                $filename
            );
            if ($path) {
                $values['url'] = Storage::url($path);
            }
        }
        return $values;
    }
    public function alter(Request $request)
    {
        // TODO: truely check is apk file
        $this->validate($request, [
            'file' => 'mimes:apk,zip'
        ]);
        return $this->altering($request);
    }
}
