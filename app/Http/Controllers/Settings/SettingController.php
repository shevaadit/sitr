<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\SystemProperties;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public const KEY = '';

    protected array $fillable = [];

    protected function reading(Request $request)
    {
        try {
            $formData = SystemProperties::select('value')
                ->where('key', static::KEY)
                ->value('value');
            $formData = json_decode($formData);
    
            return compact(
                'formData'
            );
        } catch (\Throwable $th) {
            $this->responseError($th->getMessage());
        }
    }

    protected function getFillable(): array
    {
        return $this->fillable;
    }
    protected function getValues(Request $request) 
    {
        return $request->only($this->getFillable());
    }
    protected function getContent(Request $request)
    {
        $values = $this->getValues($request);

        return json_encode($values);
    }

    protected function altering(Request $request)
    {
        try {
            $model = SystemProperties::firstOrNew([
                'key' => static::KEY,
            ]);
            $model->value = $this->getContent($request);
            $model->saveOrFail();

            return $this->responseSuccess();
        } catch (\Exception $e) {
            return $this->responseError($e);
        }
    }
}
