<?php

namespace App\Http\Controllers;

use App\Models\ZonationRule;
use App\Models\ZonationRuleActivity;
use Illuminate\Http\Request;

class ZonationRuleActivityController extends MasterController
{

    public function __construct () {
        parent::__construct(ZonationRuleActivity::class);
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        try {
            $header = ZonationRule::findOrFail($id);
            $fields = $this->getFillableFields();
            $fields['zonation_rule_id'] = $header->id;

            $this->storeModel($fields);
            // $this->runDBTransaction(function() use($header) {
            //     $fields['progress_submission_id'] = $header->id;

            //     $file           = $this->request->file("file");
            //     $filename       = $file->hashName();
            //     $filepath       = DIRECTORY_SEPARATOR.'progress-submission-document'.DIRECTORY_SEPARATOR;
            //     $isUploaded = $file->move(public_path($filepath), $filename);

            //     if (empty($isUploaded)) throw new Exception("Error Saving File.");
                
            //     $fields['file'] = $filepath.$filename;
            //     $this->model->fill($fields);
            //     $this->model->saveOrFail();
            // });
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->where('progress_submission_id', $id)
            ->get();
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ZonationRuleActivity  $zonationRuleActivity
     * @return \Illuminate\Http\Response
     */
    public function show(ZonationRuleActivity $zonationRuleActivity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ZonationRuleActivity  $zonationRuleActivity
     * @return \Illuminate\Http\Response
     */
    public function edit(ZonationRuleActivity $zonationRuleActivity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ZonationRuleActivity  $zonationRuleActivity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ZonationRuleActivity $zonationRuleActivity)
    {
        //
        $this->model = $zonationRuleActivity;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                if ($this->request->has('file')) {
                    $file           = $this->request->file("file");
                    $filename       = $file->hashName();
                    $filepath       = DIRECTORY_SEPARATOR.'progress-submission-document'.DIRECTORY_SEPARATOR;
                    $isUploaded = $file->move(public_path($filepath), $filename);

                    if (empty($isUploaded)) throw new Exception("Error Saving File.");
                    $fields['file'] = $filepath.$filename;
                }
                $this->model->fill($fields);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ZonationRuleActivity  $zonationRuleActivity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ZonationRuleActivity $zonationRuleActivity)
    {
        //
        $this->model = $zonationRuleActivity;
        try {
            return $this->runDBTransaction(function() {
                $this->model->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
