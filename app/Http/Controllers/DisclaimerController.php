<?php

namespace App\Http\Controllers;

use App\Models\SystemProperties;
use Illuminate\Http\Request;

class DisclaimerController extends MasterController
{
    protected $key = 'DISCLAIMER';
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $model = SystemProperties::where('key', $this->key)
            ->firstOrFail();
        return response()->json(json_decode($model->value));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        try {
            $model = SystemProperties::where('key', $this->key)
                ->update([
                    'value' => $this->request->value
                ]);

            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
