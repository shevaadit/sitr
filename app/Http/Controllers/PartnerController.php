<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use App\Models\PartnerType;
use Illuminate\Http\Request;

class PartnerController extends MasterController
{
    const STORAGE_PUBLIC_PATH = '/app/public';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $indexable = [
        'id' => 'id',
        'title' => 'title',
        'url' => 'url',
        'is_published' => 'is_published',
    ];

    public function __construct()
    {
        parent::__construct(Partner::class);
    }

    public function index()
    {
        //
        $query = $this->modelClass::select($this->getSelectable());

        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate(new Partner());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function upload(Request $request, &$fields, Partner $partner)
    {

        if (!$request->hasFile('file') && $partner->image == null) {
            throw new \Exception('File not found.');
        }
        if ($request->hasFile('file')) {
            $file           = $request->file("file");
            $filename       = $file->hashName();
            $filepath       = '/images/partner';

            $isUploaded     = $file->move(storage_path(self::STORAGE_PUBLIC_PATH.$filepath), $filename);

            if ($isUploaded) {
                $fields['image'] = $filepath . DIRECTORY_SEPARATOR . $filename;
                return;
            }
            throw new \Exception('Upload File Failed.');
        }
    }
    public function store(Request $request)
    {
        //
        $partner = new Partner();

        \DB::beginTransaction();
        try {
            $fields = $request->only($partner->getFillable());
            $this->upload($request, $fields, $partner);
            $partner->fill($fields);
            $partner->saveOrFail();

            \DB::commit();
            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function compilate(Partner $formData = null)
    {

        return compact('formData');
    }
    public function show(Partner $partner)
    {
        //
        return $partner;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \App\Models\Partner
     */
    public function edit(Partner $partner)
    {
        //
        return $this->compilate($partner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        //
        \DB::beginTransaction();
        try {
            $fields = $request->only($partner->getFillable());
            $this->upload($request, $fields, $partner);
            $partner->fill($fields);
            $partner->saveOrFail();

            \DB::commit();

            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        //
        \DB::beginTransaction();
        try {
            $partner->delete();
            \DB::commit();
            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }
}
