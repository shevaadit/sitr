<?php

namespace App\Http\Controllers\v2;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    protected $indexable = [];

    protected $hasMedia = false;
    protected $hasMultipleMedia = false;

    protected ?string $modelClass;

    protected Model $model;

    protected Request $request;

    public function __construct($modelClass = null)
    {
        $this->request = app('request');
        $this->modelClass = $modelClass;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return User|null
     **/
    protected function getUser()
    {
        return auth()->user();
    }
    // protected function getSelectable() {
    //     $array = $this->indexable;
    //     array_walk($array, function(&$val, $key) { $val = \DB::raw("$val as $key"); });
    //     return $array;
    // }

    protected function getSelectable()
    {
        $fields = [];
        foreach ($this->indexable as $key => $val) {
            $fields[] = is_string($key) ? \DB::raw("$val as $key") : $val;
        }

        return $fields;
    }

    protected function getIndexableFields()
    {
        return array_values($this->indexable);
    }

    protected function getIndexableFieldNames()
    {
        $fields = [];
        foreach ($this->indexable as $key => $value) {
            $fields[] = is_string($key) ? $key : $value;
        }

        return $fields;
    }

    protected function getIndexableField($name)
    {
        $fieldNames = $this->getIndexableFieldNames();
        $index = array_search($name, $fieldNames);
        $fields = $this->getIndexableFields();

        return $fields[$index];
    }
    // protected function searchByIndexable(&$query) {
    //     if (empty($this->request->searchVal)) return $query;

    //     $searchable = in_array($this->request->searchKey, array_keys($this->indexable));

    //     return $query->when($searchable, function($query) {
    //         $query->where(function($query) {
    //             $searchKey = $this->indexable[$this->request->searchKey];
    //             $searchVal = "%{$this->request->searchVal}%";
    //             $query->where($searchKey, 'ilike', $searchVal);
    //         });
    //     });
    // }

    protected function filterByIndexable(&$query)
    {
        if (empty($this->request->filters)) {
            return $query;
        }

        $filters = json_decode($this->request->filters);

        if (! is_array($filters)) {
            return $query;
        }

        $query->where(function ($query) use ($filters) {
            foreach ($filters as  $filter) {
                $filter = (array) $filter;
                $filterKeys = array_keys($filter);
                $filterVals = array_values($filter);

                $indexableFields = $this->getIndexableFields();
                foreach ($filterKeys as $index => $key) {
                    $filterable = in_array($key, $indexableFields);
                    if ($filterable) {
                        $query->where($key, $filterVals[$index]);
                    }
                }
            }
        });
    }

    protected function searchByIndexable(&$query)
    {
        if (! isset($this->request->searchVal)) {
            return $query;
        }

        $fieldNames = $this->getIndexableFieldNames();
        $searchable = in_array($this->request->searchKey, $fieldNames);

        if (! $searchable) {
            return $query;
        }

        return $query->where(function ($query) {
            $searchKey = $this->getIndexableField($this->request->searchKey);
            $searchVal = "%{$this->request->searchVal}%";
            $query->where($searchKey, 'ilike', $searchVal);
        });
    }
    // protected function sortByIndexable(&$query) {
    //     $sortBy = $this->request->sortBy;
    //     $sortOrder = $this->request->sortDesc === 'true' ? 'DESC' : 'ASC';

    //     if (in_array($sortBy, array_keys($this->indexable))) {
    //         $sortBy = $this->indexable[$sortBy];
    //         return $query->orderBy($sortBy, $sortOrder);
    //     }
    //     return $query;
    // }

    protected function sortByIndexable(&$query)
    {
        $sortBy = $this->request->sortBy;
        $sortOrder = $this->request->sortDesc === 'true' ? 'DESC' : 'ASC';
        $fieldNames = $this->getIndexableFieldNames();
        $sortable = in_array($sortBy, $fieldNames);
        if ($sortable) {
            return $query->orderBy($sortBy, $sortOrder);
        }

        return $query;
    }

    protected function sortByRequest(&$query, $sortMaps = [])
    {
        $sortBy = $this->request->sortBy;
        $sortOrder = $this->request->sortDesc === 'true' ? 'DESC' : 'ASC';

        if (in_array($sortBy, array_keys($sortMaps))) {
            $sortBy = $sortMaps[$sortBy];
        }

        if (empty($sortBy)) {
            return $query;
        }

        return $query->orderBy($sortBy, $sortOrder);
    }

    protected function getFillable()
    {
        return $this->model->getFillable() ?? [];
    }

    protected function getFillableFields()
    {
        return $this->request->only($this->getFillable());
    }

    protected function fillModel(array $fields = null)
    {
        $fields = isset($fields) ? $fields : $this->getFillableFields();
        $this->model->fill($fields);
    }

    protected function executeQuery($fields)
    {
        $this->fillModel($fields);
        $this->model->saveOrFail();
    }

    private function saveModel(array $fields = null, $getInsertId = false)
    {
        try {
            return $this->runDBTransaction(function () use ($fields) {
                $this->executeQuery($fields);
            }, $getInsertId);
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    protected function deleteModel()
    {
        try {
            return $this->runDBTransaction(function () {
                $this->model->delete();
            }, false);
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    protected function storeModel(array $fields = null)
    {
        return $this->saveModel($fields, true);
    }

    protected function updateModel(array $fields = null)
    {
        $this->saveModel($fields);
    }

    protected function getIndexingQuery()
    {
        return $this->modelClass::select($this->getSelectable());
    }

    protected function indexing()
    {
        $pageSize = $this->request->pageSize ?? 10;

        $query = $this->getIndexingQuery();

        $this->filterByIndexable($query);
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);
        $data = $query->paginate($pageSize);

        return $data;
    }

    protected function getMediaCollection()
    {
        return '';
    }

    protected function getMediaModel()
    {
        return $this->model;
    }

    protected function getMediaKey()
    {
        return 'file';
    }

    protected function getMultipleMediaKey()
    {
        return 'files';
    }

    protected function addMediaIfExists($collection = '', $model = null, $mediaKey = null)
    {
        $model = $model ? $model : $this->getMediaModel();
        $mediaKey = $mediaKey ? $mediaKey : $this->getMediaKey();

        if (! $this->request->hasFile($mediaKey)) {
            return false;
        }

        $file = $this->request->file($mediaKey);
        $filename = $file->hashName();

        $model->clearMediaCollection($collection);

        $model->addMedia($file->getPathname())
            ->usingFileName($filename)
            ->toMediaCollection($collection);

        return true;
    }

    protected function addMultipleMediaIfExists($collection = '', $model = null, $mediaKey = null)
    {
        $model = $model ? $model : $this->getMediaModel();
        $mediaKey = $mediaKey ? $mediaKey : $this->getMultipleMediaKey();

        if (! $this->request->hasFile($mediaKey)) {
            return false;
        }

        $model->clearMediaCollection($collection);

        $files = $this->request->file($mediaKey);

        foreach ($files as $file) {
            $filename = $file->hashName();
    
            $model->addMedia($file->getPathname())
                ->usingFileName($filename)
                ->toMediaCollection($collection);
        }

        return true;
    }

    protected function storing()
    {
        $this->model = new $this->modelClass();
        try {
            $id = $this->storeModel();
            if ($this->hasMedia) {
                $this->model = $this->modelClass::findOrFail($id);
                $this->addMediaIfExists($this->getMediaCollection());
            }

            if ($this->hasMultipleMedia) {
                $this->model = $this->modelClass::findOrFail($id);
                $this->addMultipleMediaIfExists($this->getMediaCollection());
            }

            return $this->responseSuccess('Success', compact('id'));
        } catch (\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function updating($modelObject)
    {
        //
        $this->model = $modelObject;
        try {
            $this->updateModel();

            if ($this->hasMedia) {
                $this->addMediaIfExists($this->getMediaCollection());
            }

            if ($this->hasMultipleMedia) {
                $this->addMultipleMediaIfExists($this->getMediaCollection());
            }

            return $this->responseSuccess();
        } catch (\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroying($modelObject)
    {
        //
        $this->model = $modelObject;
        try {
            $this->deleteModel();

            return $this->responseSuccess();
        } catch (\Exception $e) {
            return $this->responseError($e);
        }
    }
}
