<?php

namespace App\Http\Controllers;

use App\Models\Geometry;
use Illuminate\Http\Request;

class GeometryController extends MasterController
{
    public function __construct () {
        parent::__construct(Geometry::class);
    }
    
    
    public function compilate(Geometry $formData = null) {
        return compact('formData');
    }

    public function geojson($id) {
        $data = \DB::select("SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geometry)::jsonb,
                'properties', to_jsonb(row) - 'attributes' - 'geometry'
            ) AS feature
            FROM (
                SELECT geometries.*, regencies.name as regency_name 
                FROM geometries 
                LEFT JOIN regencies
                    ON regencies.id::int8 = geometries.morphable_id::int8 
                WHERE geometries.id = ?) row) features;"
        , [$id]);

        return $data[0]->jsonb_build_object;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = $this->modelClass::with('categorable')
            ->with('layer')
            ->with('layerGroup')
            ->orderBy('layer_id', 'DESC')
            ->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function show(Geometry $geometry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function edit(Geometry $geometry)
    {
        //
        return $this->compilate($geometry);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Geometry $geometry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Geometry $geometry)
    {
        //
    }
}
