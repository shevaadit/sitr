<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\v2\MasterController;
use App\Models\SystemProperties;

class ContactController extends MasterController
{
    public function __construct()
    {
        parent::__construct(SystemProperties::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $value = SystemProperties::where('key', SystemProperties::CONTACT_KEY)
            ->value('value');
        $data = json_decode($value, true);
        return $data;
    }
}
