<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\v2\MasterController;
use App\Models\Partner;

class PartnerController extends MasterController
{
    protected $indexable = [
        'id',
        'title',
        'url',
        'image',
    ];

    protected $hasMedia = true;

    public function __construct()
    {
        parent::__construct(Partner::class);
    }

    protected function getIndexingQuery()
    {
        $query = $this->modelClass::select($this->getSelectable());
        $query->where('is_published', 1);

        return $query;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->indexing();
    }
}
