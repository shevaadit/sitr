<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\v2\MasterController;
use App\Models\Post;

class PostController extends MasterController
{
    protected $indexable = [
        'id',
        'title',
        'content',
        'image',
        'slug',
        'created_at',
        'created_by',
    ];

    protected $hasMedia = true;

    public function __construct()
    {
        parent::__construct(Post::class);
    }

    protected function getIndexingQuery()
    {
        $query = $this->modelClass::select($this->getSelectable());
        $query->with('taglist');
        $query->where('is_published', 1);

        return $query;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->indexing();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return $post;
    }
}
