<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\v2\MasterController;
use App\Models\Slide;

class SlideController extends MasterController
{
    protected $indexable = [
        'id',
        'name',
        'url',
        'created_at',
        'created_by',
    ];

    protected $hasMedia = true;

    public function __construct()
    {
        parent::__construct(Slide::class);
    }

    protected function getIndexingQuery()
    {
        $query = parent::getIndexingQuery();
        $query->where('is_published', 1);

        return $query;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->indexing();
    }
}
