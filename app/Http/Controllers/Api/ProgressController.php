<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Progress;
use App\Models\Regency;
use App\Models\ProgressSubmission;
use App\Http\Controllers\Controller;

class ProgressController extends Controller
{
    //

    public function getFeature(Request $request, $x, $y)
    {
        $srid = config('webgis.srid');

        $point = "ST_GeomFromText('POINT (" . $x . " " . $y . ")'," . $srid . ")";
        $distance = 'ST_Distance(geometry, ' . $point . ') as distance';
        $selectable[] = 'id';
        // $selectable[] = 'layer_id';
        $selectable[] = \DB::raw('ST_AsGeoJSON(geometry) AS geometry');;
        $selectable[] = \DB::raw($distance);
        $selectable[] = \DB::raw('ST_Area(geometry) as area');
        $groups = $request->groups ?: [];
        $data = Progress::select($selectable)
            ->when($request->view === 'filtered', function ($query) use ($request) {
                $query->whereIn('region_id', $request->regencies);
            })
            ->whereRaw("ST_DWithin(geometry, {$point}, 600)")
            ->orderBy('distance')
            ->orderBy('area')
            ->first();

        $id = ! empty($data) ? $data->id : null;
        $feature = [
            'type' => 'Feature',
            'properties' => [
                'id' => $id,
            ],
            'geometry' => null
        ];
        if (!empty($data)) {
            $feature['geometry'] = json_decode($data->geometry);
            // $response['properties']['group_name'] = $data->layerGroup->name;
        }
        $response = [
            'id' => $id,
            'type' => 'FeatureCollection',
            'features' => [
                $feature
            ],
        ];
        return response()->json($response);
    }

    private function getAttributes($id) {
        $progress = Progress::findOrFail($id);
        $listData = [];
        $listData[] = [
            'name' => $progress->typeRegion->name,
            'value' => $progress->region->name
        ];
        $listData[] = [
            'name' => 'Skor',
            'value' => $progress->lastStatus->name
        ];
        return $listData;
    }
    private function getHistories($id) {
        $histories = ProgressSubmission::approved()
            ->where('progress_id', $id)
            ->with(['documents' => function($query) {
                $query->select('id', 'name', 'progress_submission_id')
                    ->when(auth()->check(), function($query) {
                        $query->selectRaw("file as file_url");
                    })
                    ->when(auth()->check() === false, function($query) {
                        $query->selectRaw("'#' as file_url");
                    });
            }])
            ->with('progressStatus:id,name')
            ->get();
        return $histories;
    }
    public function getInfo($id)
    {
        $attributes = $this->getAttributes($id);
        $histories = $this->getHistories($id);
        $histories = $histories->map(function ($item) {
            return [
                'name' => $item->progressStatus->name ?? '',
                'value' => $item->progressStatus->name ?? '',
                'created_at' => $item->created_at,
            ];
        });
        return compact('attributes', 'histories');
    } 
    public function index()
    {
        $regencies = $this->getRegencies();

        $wms = config('geoserver.host').'/'.config('geoserver.path').'/'.config('geoserver.workspace').'/wms?';
        return compact(
            // 'disclaimer', 
            'wms',
            'regencies',
            // 'groups'
        );
    }
    private function getRegencies()
    {
        return Regency::select("*")
            ->selectRaw('true as is_showing')
            ->selectRaw("null as groups")
            ->get();
    }
}
