<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        try {
            $user = new User();
            $user->fill([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $user->saveOrFail();

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'token' => $token,
                'type' => 'Bearer',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            $this->responseError($th->getMessage());
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if (! Auth::attempt($request->only('email', 'password'))) {
            return $this->responseError('Invalid login credentials');
        }

        $user = User::where('email', $request['email'])
            // ->whereIn('level', ['surveyor', 'customer'])
            ->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'type' => 'Bearer',
        ]);
    }
}
