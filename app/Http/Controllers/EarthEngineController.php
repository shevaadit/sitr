<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegencyGeometry;
use App\Models\ZonationRule;
use App\Models\WebgisLayerGroup as LayerGroup;
use App\Models\Layer;
use App\Models\EarthEngineTask;
use App\Models\Geometry;
use App\Models\Regency;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class EarthEngineController extends Controller
{
    //
    private const LAYER_GROUP_IDS = [5, 22];
    public function __construct()
    {        
        $this->client = new Client();

        $this->requestConfig = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            // 'auth' => [$this->username, $this->password],
        ];

    }
    public function index() {
    }
    public function getUrl(Request $request) {
        $query = [
            'region' => $request->region,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ];
        $requestUrl = config('earthengine.host').'/get-url';

        $this->requestConfig['query'] = $query;
        try {
            $response = $this->client->request('GET', $requestUrl, $this->requestConfig);
            return $response->getBody()->getContents();;
            //code...
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function getLandCoverUrl(Request $request) {
        $query = [
            'region' => $request->region,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ];
        $requestUrl = config('earthengine.host').'/land-cover/get-url';

        $this->requestConfig['query'] = $query;
        try {
            $response = $this->client->request('GET', $requestUrl, $this->requestConfig);
            return $response->getBody()->getContents();;
            //code...
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function downloadLandCover(Request $request) {
        $query = [
            'region' => $request->region,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ];
        $host = config('earthengine.host');
        $requestUrl = "{$host}/land-cover/download";

        $this->requestConfig['query'] = $query;
        try {
            $response = $this->client->request('GET', $requestUrl, $this->requestConfig);
            $contents = $response->getBody()->getContents();
            $result = json_decode($contents);
            $response = null;
            foreach ($result->values as $value) {
                if ($value->task_type == 'classified') {
                    $response = $value;
                }
                $model = new EarthEngineTask(); 
                $fields = [
                    'code' => $value->task_id,
                    'category' => 'PENUTUP LAHAN',
                    'filename' => $value->filename,
                    'type' => $value->task_type,
                    'preview_url' => $value->preview_url ?? null,
                    'status'  => $value->task_status->state
                ];
                $model->fill($fields);
                $model->saveOrFail();
            }
            return $response;
            //code...
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function download(Request $request) {
        $query = [
            'region' => $request->region,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ];
        $host = config('earthengine.host');
        $requestUrl = "{$host}/download";

        $this->requestConfig['query'] = $query;
        try {
            $response = $this->client->request('GET', $requestUrl, $this->requestConfig);
            $result = $response->getBody()->getContents();
            $response = json_decode($result);

            $model = new EarthEngineTask(); 
            $fields = [
                'code' => $response->task_id,
                'category' => 'CITRA GEE',
                'filename' => $response->filename,
                'type' => $response->task_type,
                'preview_url' => $response->preview_url ?? null,
                'status'  => $response->task_status->state
            ];
            $model->fill($fields);
            $model->saveOrFail();

            return $response;
            //code...
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
