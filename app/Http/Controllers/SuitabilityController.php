<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegencyGeometry;
use App\Models\ZonationRule;
use App\Models\ZonationRuleActivity;
use App\Models\WebgisLayerGroup as LayerGroup;
use App\Models\Layer;
use App\Models\LayerAttribute;
use App\Models\Geometry;
use App\Models\Regency;
use App\Models\SystemProperties;

use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;
use Shapefile\ShapefileWriter;

use App\Http\Rules\CompleteDatafile;
use Madnest\Madzipper\Madzipper;
use App\Http\Rules\CompleteShapeFile;

class SuitabilityController extends Controller
{
    //
    private const LAYER_GROUP_IDS = [7, 28];
    private const STRUCTURE_ID = 12;
    // private $layerIds;
    // public function __construct()
    // {
    //     $this->layerIds = Layer::whereIn('layer_group_id', self::LAYER_GROUP_IDS)
    //         ->pluck('id');
      //its just a dummy data object.
    //   dd(!auth()->check());
        // $this->middleware('auth');
    // }
    public function index() {
        // $ids = implode(',', $this->layerIds);
        // $data = \DB::select("SELECT jsonb_build_object(
        //         'type',     'FeatureCollection',
        //         'features', jsonb_agg(feature)
        //     )
        //     FROM (
        //     SELECT jsonb_build_object(
        //         'type',       'Feature',
        //         'id',         id,
        //         'geometry',   ST_AsGeoJSON(geometry)::jsonb,
        //         'properties', to_jsonb(row) - 'geometry' - 'attributes'
        //     ) AS feature
        //     FROM (
        //         SELECT
        //             geometries.*, 
        //             geometries.attributes->>'SCORE_PROG' as score, 
        //             regencies.name as regency_name, 
        //             regencies.id as regency_id
        //         FROM geometries
        //         LEFT JOIN regencies
        //             ON regencies.id::int8 = geometries.morphable_id::int8
        //         WHERE geometries.layer_id = ?) row) features;"
        // , [ "({$ids})" ]);
        $disclaimer = SystemProperties::select("value->content as val")
            ->where('key', 'DISCLAIMER')
            ->value('val');
        return compact('disclaimer');
    }
    public function getRegencies() {
        return Regency::select("*")
            ->selectRaw('true as is_showing')
            ->selectRaw("null as groups")
            ->get();
    }
    public function getGroups() {
        return LayerGroup::selectWebgisFields()
            ->where('level', 1)
            ->whereNotIn('id', [2, 3])
            ->withCount('legends')
            // ->whereIn('id', self::LAYER_GROUP_IDS)
            // ->with('children')
            ->get();
    }
    
    public function getSrid(Request $request) {
        $data = \DB::table('spatial_ref_sys')
            ->where('srid', 'ilike', "%{$request->search}%")
            ->selectRaw('srid as id')
            ->selectRaw('auth_name as name')
            ->take(10)
            ->get();
        return $data;
    }
    private function gettingRulesJoin(&$query) {
        return $query->join('layers as layer', 'geometries.layer_id', '=', 'layer.id')
            ->join('layer_groups as layer_group', 'layer.layer_group_id', '=', 'layer_group.id')
            ->join('zonation_rules as zonation_rule', 'layer_group.id', '=', 'zonation_rule.layer_group_id')
            ->join('zonation_rule_activities as zonation_rule_activity', 'zonation_rule.id', '=', 'zonation_rule_activity.zonation_rule_id');
    }
    public function getRules(Request $request) {

        $rules = ZonationRule::select('key')
            ->whereIn('layer_group_id', self::LAYER_GROUP_IDS)
            ->groupBy('key')
            ->get();

        $ruleKeys = $rules->pluck('key')->toArray();

        $selectable = [
            'zonation_rule_activity.id',
            'zonation_rule_activity.name',
            'zonation_rule_activity.rule'
        ];
        $selectableAttrs = [];
        $groupBys = [];

        foreach($ruleKeys as $index => $key) {
            $alias = $index === array_key_first($ruleKeys) ? 'value' : 'value_'.$index; 
            $selectableAttrs[] = "geometries.attributes->{$key} as {$alias}";
            $groupBys[] = "geometries.attributes->{$key}";
        }
        $query = Geometry::select(array_merge($selectable, $selectableAttrs))
            ->whereIn('layer_group.id', self::LAYER_GROUP_IDS)
            ->whereIn('geometries.id', $request->ids);

        $this->gettingRulesJoin($query);

        $query->where(function($q) use($ruleKeys) {
            foreach($ruleKeys as $index => $key) {
                if ($index === array_key_first($ruleKeys)) {
                    $q->whereRaw("geometries.attributes->>'{$key}' ILIKE zonation_rule.value");
                } else {
                    $q->whereRaw("geometries.attributes->>'{$key}' ILIKE zonation_rule.value");
                }
            }            
        });
        $data = $query->groupBy(array_merge($selectable, $groupBys))
            ->get();
        // if (in_array($ruleKeys, array_keys($attributes))) {
        //     $value_active
        // }
        return compact('data', 'ruleKeys');
    }
    public function getAttributes(Request $request) {
        $ids = is_array($request->ids) ? $request->ids : [];            

        $query = LayerAttribute::select([
                'geom.id',
                'd.key',
                'layer_attributes.name',
                'd.value',
            ])
            ->selectRaw('layer_group.name as layer_group_name')
            ->join('layers as layer', 'layer_attributes.layer_group_id', '=', 'layer.layer_group_id')
            ->join('layer_groups as layer_group', 'layer_attributes.layer_group_id', '=', 'layer_group.id')
            ->join('geometries as geom', 'layer.id', '=', 'geom.layer_id')
            ->join(\DB::raw('json_each_text(geom.attributes) as d'), 'd.key', '=', 'layer_attributes.key')
            ->whereIn('geom.id', $request->ids)
            ->orderByRaw('1, 2');

        $listData = $query->get();

        $grouped = $listData->groupBy('layer_group_name');

        return $grouped->all();
    }
    public function getRawAttributes($id) {
        $listData = \DB::select('select
                g.id,
                d.key as name,
                d.value
            from
                geometries g
            left join json_each_text(g."attributes") d on true
            where
                g.id = ? 
            order by
                1,
                2;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return $listData;
    }
    public function getDocuments($id) {
        $listData = \DB::select('select
                rd.*
            from
                regulation_documents rd
            join layers l on l.regulation_id = rd.regulation_id
            join geometries g on g.layer_id = l.id
            where
                g.id = ?;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return $listData;
    }
    private function getWktByShp($file) {
        $zipper = new Madzipper;

        $zipper->make($file->getPathname());
        $extractpath = $file->getPathname().'.extract'.DIRECTORY_SEPARATOR;

        $layerFile = $zipper->listFiles('/\.(dbf|prj|shp|shx)$/i');
        $shapeFile = $zipper->listFiles('/\.(shp)$/i');
        $shapeFilenames = [];
        // $arr_basename = [];

        foreach ($shapeFile as $path) {
            $shapeFilenames[] = pathinfo($path, PATHINFO_FILENAME);
        //     $arr_basename[] = pathinfo($path, PATHINFO_BASENAME);
        }

        $zipFolder = dirname($layerFile[0]);

        if ($zipFolder != '.') {
            $zipper->folder($zipFolder);
        }

        $zipper->extractMatchingRegex($extractpath, '/\.(dbf|prj|shp|shx)$/i');
        $zipper->close();

        // Open Shapefile
        $shapefile = new ShapefileReader($extractpath.$shapeFilenames[0].'.shp', [
            Shapefile::OPTION_ENFORCE_POLYGON_CLOSED_RINGS => false,
            Shapefile::OPTION_POLYGON_ORIENTATION_READING_AUTOSENSE => true,
        ]);

        // Read all the records;
        $wkt = [];
        try {
            foreach ($shapefile as $i => $geometry) {
                if ($geometry->isDeleted()) {
                    continue;
                }
                // if ($geometry->isZ()) {
                    $rawWkt = $geometry->getWKT();
                    $rawWkt = explode(', ', $rawWkt);
                    $rawWkt = implode('|', $rawWkt);
                    $rawWkt = str_replace(',', '.', $rawWkt);
                    $rawWkt = str_replace('|', ', ', $rawWkt);
                    $wkt[] = $rawWkt;
                // } else {
                //     $wkt[] = $geometry->getWKT();
                // }
                break;
            }
        } catch (\Throwable $th) {
            throw $th;
        } finally {
            $this->deleteDirectory($extractpath);
        }
        return $wkt;
    }
    public function getFeatureByArea(Request $request) {

        $request->validate([
            'srid' => [
                'required'
            ],
            'file' => [
                'bail',
                'required',
                'file',
                'mimes:zip',
                new CompleteShapeFile()
            ]
        ]);
        try {
            $wkt = $this->getWktByShp($request->file);
            if (empty($wkt)) {
                throw new \Exception('Empty Shapefile.');
            }
            $data = $this->getGeometriesByWkt($wkt[0], $request->srid);
            $ids = $data->pluck('id');
            $structure = LayerGroup::select("*")
                ->selectRaw('true as is_showing')
                ->selectRaw('1 as opacity')
                ->where('id', self::STRUCTURE_ID)
                ->first();

            $groups = array_filter($data->pluck('layerGroup')->toArray());
            
            return response()->json(compact('ids', 'groups', 'structure'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    private function getGeometriesByWkt($wkt, $srid) {
        $geometry = "ST_GeomFromText('{$wkt}', {$srid})";

        $distance = 'ST_Distance(geometry, '.$geometry.') as distance';
        $selectable[] = 'id';
        $selectable[] = 'layer_id';
        // $selectable[] = \DB::raw('ST_AsGeoJSON(geometry) AS geometry');;
        $selectable[] = \DB::raw($distance);
        $selectable[] = \DB::raw('ST_Area(geometry) as area');
        $data = Geometry::select($selectable)
                // ->when($request->view === 'filtered', function($query) use($request) {
                //     $query->whereIn('morphable_id', $request->regencies);
                // })
                ->whereHas('layerGroup', function($query) {
                    $query->whereIn('layer_group_id', self::LAYER_GROUP_IDS);
                })
                ->with('layerGroup')
                ->whereRaw("ST_DWithin(geometry, {$geometry}, 0)")
                ->orderBy('distance')
                ->orderBy('area')
                ->get();
        return $data;
    }
    public function getFeature(Request $request, $x, $y) {
        $srid = config('webgis.srid');
        $wkt = "POINT ({$x} {$y})";
        $data = $this->getGeometriesByWkt($wkt, $srid);
        $ids = $data->pluck('id');
        $structure = LayerGroup::select("*")
            ->selectRaw('true as is_showing')
            ->selectRaw('1 as opacity')
            ->where('id', self::STRUCTURE_ID)
            ->first();

        $groups = array_filter($data->pluck('layerGroup')->toArray());
        
        return response()->json(compact('ids', 'groups', 'structure'));
    }
}
