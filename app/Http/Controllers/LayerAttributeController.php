<?php

namespace App\Http\Controllers;

use App\Models\LayerAttribute;
use App\Models\LayerGroup;
use Illuminate\Http\Request;
use App\Helpers\LayerGroupView;
use App\Helpers\Geoserver;

class LayerAttributeController extends MasterController
{
    public function __construct () {
        parent::__construct(LayerAttribute::class);
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $group = LayerGroup::findOrFail($id);
            $this->runDBTransaction(function() use($group) {
                $fields = $this->getFillableFields();
                $fields['layer_group_id'] = $group->id;
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $helper = new LayerGroupView($group);
                $helper->upsertViewQuery();
            });
            $helper = new Geoserver();
            $helper->updateLayer($group);
            return $this->responseSuccess('Success', compact('id'));
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->responseError($e->getResponse());
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->where('layer_group_id', $id)
            ->get();
        $dataTypes = array_values($this->modelClass::DATA_TYPES);
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData', 'dataTypes');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LayerAttribute  $layerAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(LayerAttribute $layerAttribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LayerAttribute  $layerAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(LayerAttribute $layerAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LayerAttribute  $layerAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LayerAttribute $layerAttribute)
    {
        //
        $this->model = $layerAttribute;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $helper = new LayerGroupView($this->model->group);
                $helper->upsertViewQuery();
            });
            $helper = new Geoserver();
            $helper->updateLayer($this->model->group);
            return $this->responseSuccess();
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->responseError($e->getResponse());
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LayerAttribute  $layerAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(LayerAttribute $layerAttribute)
    {
        //
        $this->model = $layerAttribute;
        try {
            $group = $this->model->group;
            $this->deleteModel();
            $helper = new Geoserver();
            $helper->updateLayer($group);
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
