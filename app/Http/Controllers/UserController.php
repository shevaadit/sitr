<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends MasterController
{
    protected $indexable = [
        'id' => 'id',
        'email' => 'email',
        'email_verified_at' => 'email_verified_at',
        'name' => 'name',
        'region_id' => 'region_id',
        'region_type' => 'region_type',
        'image' => 'image',
        // 'name' => 'name',
    ];
    public function __construct () {
        parent::__construct(User::class);
    }
    private function hashPasswordField(&$fields) {
        if (isset($fields['password'])) {
            $fields['password'] = Hash::make($fields['password']);
        }
    }
    public function getRegions(Request $request) {
        if ($request->id && array_key_exists($request->id, User::REGION_TYPES)) {
            return $request->id::select('id', 'name')->get();
        }
        return [];
    }

    public function compilate(User $formData = null) {
        $regionTypes = array_values(User::REGION_TYPES);
        return compact('regionTypes', 'formData');
    }

    public function changePassword($id, Request $request) {
        \DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $fields = [];
            $fields['password'] = Hash::make($request->password);
            $user->fill($fields);
            $user->saveOrFail();
            \DB::commit();
            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }

    public function upload(Request $request, $id) {

        if ( !$request->hasFile('image')) {
            return response()->json(['failed']);
        }

        $this->model    = User::findOrFail($id);
        $file           = $request->file("image");
        $filename       = $file->hashName();
        $filepath       = DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR;

        $isUploaded     = $file->move(public_path($filepath), $filename);

        if ($isUploaded) {
            try {
                $oldpath = $this->model->image;
                $this->model->image = $filepath.$filename;
                $this->runDBTransaction(function() {
                    $this->model->saveOrFail();
                });
                if (!empty($oldpath)) {
                    $this->deleteDirectory(public_path($oldpath));
                }
                return $this->responseSuccess();
            } catch (\Throwable $th) {
                return $this->responseError($th);
            }
        }
        return $this->responseError();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortMaps = [
            'id' => 'id',
            'name' => 'name'
        ];
        //
        $query = $this->modelClass::select($this->getSelectable());
        // $this->sortByIndexable($query);
        $this->sortByRequest($query, $sortMaps);
        $data = $query->paginate(10);

        // $data = $this->modelClass::paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->runDBTransaction(function() use($request) {
                $fields = $this->getFillableFields();
                $this->hashPasswordField($fields);
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->model->id = \DB::getPdo()->lastInsertId();
            }, true);
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        return $user->setAppends(['type']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        return $this->compilate($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $this->model = $user;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() use($request) {
                $fields = $this->getFillableFields();
                $this->hashPasswordField($fields);
                $this->model->fill($fields);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $this->model = $user;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
