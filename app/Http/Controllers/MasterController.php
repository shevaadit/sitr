<?php
namespace App\Http\Controllers;

use App\Models\LayerCategory;
use Illuminate\Http\Request;

class MasterController extends Controller
{

    protected $indexable = [];
    protected $modelClass;
    protected $model;
    protected Request $request;

    public function __construct($modelClass = null) {
        $this->request = app('request');
        $this->modelClass = $modelClass;
    }
    protected function getSelectable() {
        $array = $this->indexable;
        array_walk($array, function(&$val, $key) { $val = \DB::raw("$val as $key"); });
        return $array;
    }
    protected function searchByIndexable(&$query) {
        if (empty($this->request->searchVal)) return $query;

        $searchable = in_array($this->request->searchKey, array_keys($this->indexable));

        return $query->when($searchable, function($query) {
            $query->where(function($query) {
                $searchKey = $this->indexable[$this->request->searchKey];
                $searchVal = "%{$this->request->searchVal}%";
                $query->where($searchKey, 'ilike', $searchVal);
            });
        });
    }
    protected function sortByIndexable(&$query) {
        $sortBy = $this->request->sortBy;
        $sortOrder = $this->request->sortDesc === 'true' ? 'DESC' : 'ASC';

        if (in_array($sortBy, array_keys($this->indexable))) {
            $sortBy = $this->indexable[$sortBy];
            return $query->orderBy($sortBy, $sortOrder);
        }
        return $query;
    }
    protected function sortByRequest(&$query, $sortMaps = []) {
        $sortBy = $this->request->sortBy;
        $sortOrder = $this->request->sortDesc === 'true' ? 'DESC' : 'ASC';

        if (in_array($sortBy, array_keys($sortMaps))) {
            $sortBy = $sortMaps[$sortBy];
        }
        if (empty($sortBy)) return $query;
        return $query->orderBy($sortBy, $sortOrder);

    }
    protected function getFillableFields() {
        return $this->request->only($this->model->getFillable());
    }
    protected function fillModel(array $fields = null) {
        $fields = isset($fields) ? $fields : $this->getFillableFields();
        $this->model->fill($fields);
    }
    private function saveModel(array $fields = null, $getInsertId = false) {
        try {
            return $this->runDBTransaction(function() use($fields) {
                $this->fillModel($fields);
                $this->model->saveOrFail();
            }, $getInsertId);
        } catch (\Throwable $e) {
            throw $e;
        }
    }
    protected function deleteModel() {
        try {
            return $this->runDBTransaction(function() {
                $this->model->delete();
            }, false);
        } catch (\Throwable $e) {
            throw $e;
        }
    }
    protected function storeModel(array $fields = null) {
        return $this->saveModel($fields, true);
    } 
    protected function updateModel(array $fields = null) {
        $this->saveModel($fields);
    } 
}
