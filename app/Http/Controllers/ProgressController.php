<?php

namespace App\Http\Controllers;

use App\Models\Progress;
use App\Models\ProgressSubmission;
use App\Models\User;
use App\Models\Layer;
use App\Models\District;
use App\Models\DistrictGeometry;
use App\Models\Geometry;
use App\Models\Regulation;
use App\Models\LayerGroup;
use Illuminate\Http\Request;

class ProgressController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getRegions(Request $request) {
        if ($request->id === 'regional') {
            return $request->region_type::select('id', 'name')->get();
        } else if ($request->id === 'detailed') {
            return $request->region_type::select('id', 'name')
                ->where('regency_id', $request->regency_id)
                ->get();
        }
        return [];
    }
    public function getDistricts() {
        return District::select('id', 'name')->get();
    }
    public function __construct () {
        parent::__construct(Progress::class);
    }

    protected function getFillableFields() {
        $fields = $this->request->only($this->model->getFillable());
        $fields['geometry'] = \DB::raw("ST_SetSRID(ST_Force2D(ST_GeomFromGeoJSON('".$fields['geometry']."')), 4326)");
        return $fields;
    }
    
    public function compilate(Progress $formData = null) {
        $geojson = null;
        $groups = LayerGroup::select('id', 'name')->get();
        $regulations = Regulation::select('id', 'name')->get();
        $regionTypes = array_values(Progress::REGION_TYPES);
        $types = array_values(Progress::TYPES);
        if ($formData) {
            $geojson = $this->geojson($formData->id);
        }

        return array_filter(
            compact(
                'types',
                'geojson',
                'formData',
                'groups',
                'regulations',
                'regionTypes'
            )
        );
    }

    private function geojson($id) {
        $data = \DB::select("SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geometry)::jsonb
            ) AS feature
            FROM (
                SELECT progress.* 
                FROM progress 
                WHERE progress.id = ?) row) features;"
        , [$id]);

        return json_decode($data[0]->jsonb_build_object);
    }
    public function index()
    {
        //
        $sortMaps = [
            'regulation' => 'regulation_id',
            'region' => 'region_id'
        ];
        $query = $this->modelClass::with('regulation:id,name')
            ->with('region:id,name'); 
        
        $this->sortByRequest($query, $sortMaps);

        $data = $query->paginate(10);
        $data->append('type');

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Progress  $progress
     * @return \Illuminate\Http\Response
     */
    public function show(Progress $progress)
    {
        //
        return ['formData' => $progress];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Progress  $progress
     * @return \Illuminate\Http\Response
     */
    public function edit(Progress $progress)
    {
        //
        return $this->compilate($progress);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Progress  $progress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Progress $progress)
    {
        //
        $this->model = $progress;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Progress  $progress
     * @return \Illuminate\Http\Response
     */
    public function destroy(Progress $progress)
    {
        //
        try {
            $this->runDBTransaction(function() use($progress) {
                ProgressSubmission::where('progress_id', $progress->id)
                    ->delete();
                $progress->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
