<?php

namespace App\Http\Controllers;

use App\Models\ProgressStatus;
use Illuminate\Http\Request;
use App\Helpers\Geoserver;

class ProgressStatusController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct () {
        parent::__construct(ProgressStatus::class);
    }

    
    public function compilate(ProgressStatus $formData = null) {
        return compact('formData');
    }
    
    public function index(Request $request)
    {
        //
        $data = $this->modelClass::orderBy('id')
            ->where('type_id', $request->type)
            ->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $this->model = new $this->modelClass();
        // $this->request = $request;
        // try {
        //     $id = $this->storeModel();
        //     return $this->responseSuccess('Success', compact('id'));
        // } catch (\Throwable $th) {
        //     return $this->responseError($th);
        // }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgressStatus  $progressStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ProgressStatus $progressStatus)
    {
        //
        return ['formData' => $progressStatus];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgressStatus  $progressStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgressStatus $progressStatus)
    {
        //
        return $this->compilate($progressStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgressStatus  $progressStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgressStatus $progressStatus)
    {
        //
        $this->model = $progressStatus;
        $this->request = $request;
        try {
            $this->updateModel();
            $helper = new Geoserver();
            $helper->updateStyleProgress('regional');
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgressStatus  $progressStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgressStatus $progressStatus)
    {
        //
        $this->model = $progressStatus;
        try {
            // $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
