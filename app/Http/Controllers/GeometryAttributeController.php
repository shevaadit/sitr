<?php

namespace App\Http\Controllers;

use App\Models\Geometry;
use App\Models\LayerGroup;
use Illuminate\Http\Request;

class GeometryAttributeController extends MasterController
{
    public function __construct () {
        parent::__construct(Geometry::class);
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->update($request, $id);
    }
    
    public function fetch($id)
    {
        //
        $listData = \DB::select('select
                g.id,
                d.key,
                laa."name" ,
                d.value
            from
                geometries g
            join json_each_text(g."attributes") d on
                true
            join layers l on
                l.id = g.layer_id
            left join layer_attributes laa on d.key = laa."key" AND laa.layer_group_id = l.layer_group_id 
            where
                g.id = ?
            order by
                1,
                2;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function show(Geometry $geometry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function edit(Geometry $geometry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->model = Geometry::findOrFail($id);
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $key = $this->request->key;
                $value = $this->request->value;
                $this->model->fill([
                    'attributes->' . $key => $value
                ]);
                $this->model->saveOrFail();
                // \DB::update("UPDATE geometries 
                //     SET attributes = jsonb_set(attributes::jsonb, '{?}', '\"?\"', true) WHERE id = ?;",
                // [$key, $value, $id]);
            });
            return $this->responseSuccess('Success', compact('id'));    
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Geometry  $geometry
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        $this->model = Geometry::findOrFail($id);
        $this->request = $request;
        $attributes = json_decode($this->model->attributes, true);
        $key = $this->request->key;
        unset($attributes[$key]);
        try {
            $this->runDBTransaction(function() use ($attributes) {
                $this->model->fill([
                    'attributes' => $attributes
                ]);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
