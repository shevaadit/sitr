<?php

namespace App\Http\Controllers;

use App\Models\Layer;
use App\Models\LayerGroup;
use App\Models\LayerCategory;
use App\Models\Geometry;
use Illuminate\Http\Request;
use App\Helpers\LayerGroupView;
use App\Helpers\Geoserver;

class LayerGroupController extends MasterController
{
    private $helper;
    protected $indexable = [
        'id' => 'id',
        'is_published' => 'is_published',
        'name' => 'name',
    ];
    public function __construct () {
        parent::__construct(LayerGroup::class);
        $this->helper = new Geoserver();
    }

    public function compilate(LayerGroup $formData = null) {
        $categories = LayerCategory::all();
        $parents = LayerGroup::select('id', 'name')
            ->when(isset($formData), function($query) use($formData) {
                $query->where('id', '!=', $formData->id);
            })
            ->get();
        return compact('categories', 'parents', 'formData');
    }
    private function setLevelByParent(&$fields) {
        if (empty($fields['parent_id'])) return;
        $parent = LayerGroup::find($fields['parent_id']);
        $fields['level'] = $parent ? $parent->level + 1 : null;
    }
    private function updateParentChildStatus($id) {
        if (empty($id)) return;
        if (LayerGroup::where('parent_id', $id)->exists()) {
            LayerGroup::where('id', $id)
                ->update([
                    'has_child' => true
                ]);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $query = $this->modelClass::select(array_values($this->getSelectable())); 
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);
        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->runDBTransaction(function() {
                $this->updateParentChildStatus($this->model->parent_id);
                $fields = $this->getFillableFields();
                $this->setLevelByParent($fields);
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->updateParentChildStatus($fields['parent_id']);
                $this->model->id = \DB::getPdo()->lastInsertId();
                $helper = new LayerGroupView($this->model);
                $helper->upsertViewQuery();
            }, true);
            $this->helper->insertStyle($this->model);
            $this->helper->publishLayer($this->model);
            $this->helper->applyStyle($this->model->getLayerName(), $this->model->getStyleName());
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LayerGroup  $layerGroup
     * @return \Illuminate\Http\Response
     */
    public function show(LayerGroup $layerGroup)
    {
        //
        return $layerGroup->load('category', 'parent');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LayerGroup  $layerGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(LayerGroup $layerGroup)
    {
        //
        return $this->compilate($layerGroup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LayerGroup  $layerGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LayerGroup $layerGroup)
    {
        //
        $this->model = $layerGroup;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $this->updateParentChildStatus($this->model->parent_id);
                $fields = $this->getFillableFields();
                $this->setLevelByParent($fields);
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->updateParentChildStatus($fields['parent_id']);
                $helper = new LayerGroupView($this->model);
                $helper->upsertViewQuery();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LayerGroup  $layerGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(LayerGroup $layerGroup)
    {
        //
        $this->model = $layerGroup;
        try {
            $this->runDBTransaction(function() {
                Geometry::whereHas('layerGroup', function($query) {
                        $query->where('layer_group_id', $this->model->id);
                    })
                    ->delete();
                Layer::where('layer_group_id', $this->model->id)
                    ->delete();
                $this->model->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
