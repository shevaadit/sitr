<?php

namespace App\Http\Controllers;

use App\Models\IndicatorDocument;
use Illuminate\Http\Request;

class IndicatorDocumentController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $indexable = [
        'id' => 'id',
        'name' => 'name',
        'is_published' => 'is_published',
    ];
    public function __construct () {
        parent::__construct(IndicatorDocument::class);
    }

    protected function getFillableFields() {
        $fields = $this->request->only($this->model->getFillable());
        if ($this->request->hasFile('file')) {
            $file           = $this->request->file("file");
            $filename       = $file->hashName();
            $filepath       = DIRECTORY_SEPARATOR.'document'.DIRECTORY_SEPARATOR;
            $isUploaded = $file->move(public_path($filepath), $filename);

            if (empty($isUploaded)) throw new \Exception("Error Saving File.");
            $fields['file'] = $filepath.$filename;
            $fields['mime'] = $isUploaded->getMimeType();
        }
        return $fields;
    }
    
    public function compilate(IndicatorDocument $formData = null) {
        return array_filter(
            compact(
                'formData'
            )
        );
    }

    public function index()
    {
        //
        // $sortMaps = [
        //     'regulation' => 'regulation_id',
        //     'region' => 'region_id'
        // ];
        $query = $this->modelClass::select($this->getSelectable()); 
        
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IndicatorDocument  $indicatorDocument
     * @return \Illuminate\Http\Response
     */
    public function show(IndicatorDocument $indicatorDocument)
    {
        //
        return $indicatorDocument;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IndicatorDocument  $indicatorDocument
     * @return \Illuminate\Http\Response
     */
    private function loadTags(IndicatorDocument $indicatorDocument) {
        return $indicatorDocument->load("taglist");
    }
    public function edit(IndicatorDocument $indicatorDocument)
    {
        //
        return $this->compilate($indicatorDocument);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IndicatorDocument  $indicatorDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndicatorDocument $indicatorDocument)
    {
        //
        $this->model = $indicatorDocument;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IndicatorDocument  $indicatorDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndicatorDocument $indicatorDocument)
    {
        //
        try {
            $this->runDBTransaction(function() use($indicatorDocument) {
                $this->deleteDirectory($indicatorDocument->file);
                $indicatorDocument->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
