<?php

namespace App\Http\Controllers;

use App\Models\LayerCategory;
use Illuminate\Http\Request;

class LayerCategoryController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $indexable = [
        'id' => 'id',
        'name' => 'name',
    ];
    public function __construct () {
        parent::__construct(LayerCategory::class);
    }

    
    public function compilate(LayerCategory $formData = null) {
        return compact('formData');
    }
    
    public function index()
    {
        //
        $query = $this->modelClass::select(array_values($this->getSelectable())); 
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);
        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LayerCategory  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LayerCategory $layerCategory)
    {
        //
        return ['formData' => $layerCategory];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LayerCategory  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(LayerCategory $layerCategory)
    {
        //
        return $this->compilate($layerCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LayerCategory  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LayerCategory $layerCategory)
    {
        //
        $this->model = $layerCategory;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LayerCategory  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LayerCategory $layerCategory)
    {
        //
        $this->model = $layerCategory;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
