<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class NewsController extends Controller
{
    //
    public function index(Request $request) {
        $tags  = Post::getTags();
        $posts = Post::with('creator:id,name')
            ->when($request->search ?? false, function($query) use ($request) {
                return $query->where('title', 'like', '%'.$request->search.'%');
            })
            ->when($request->tags ?? false, function($query) use ($request) {
                $query->whereHas('taglist', function($query) use($request) {
                    $locale = app()->getLocale();
                    $query->where('tags.slug->'.$locale, $request->tags);
                });
            })
            ->where('is_published', 1)
            ->orderBy('created_at', "desc")
            ->paginate(5);

        $latests = Post::with('creator:id,name')
            ->where('is_published', 1)
            ->orderBy('created_at', "desc")
            ->take(5)
            ->get();
        return view('news', compact('posts', 'latests', 'tags'));
    }
    public function show($id) {
        $links = \Jorenvh\Share\ShareFacade::page(route('news.detail', $id), 'Share title')
            ->facebook()
            ->whatsapp()
            ->linkedin()
            ->twitter()
            ->getRawLinks();
        
        $tags = Post::getTags();
        $post = Post::with('creator:id,name')->where("slug", $id)
            ->where('is_published', 1)
            ->first();
        $post->increment('view_count');
        $latests = Post::with('creator:id,name')
            ->where('is_published', 1)
            ->orderBy('created_at', "desc")
            ->take(5)
            ->get();
        return view('news-detail', compact('post', 'latests', 'tags', 'links'));
    }
}
