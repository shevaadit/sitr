<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;

class FaqController extends Controller
{
    //
    public function index(Request $request) {
        $faqs = Faq::where('issuance', 1)
            ->get();
        return view('faq', compact('faqs'));
    }
}
