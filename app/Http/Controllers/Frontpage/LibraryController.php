<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Models\Document;
use App\Http\Controllers\Controller;

class LibraryController extends Controller
{
    //
    protected const FILETYPES = [
        'pdf' => ['application/pdf'],
        'image' => ['image/png', 'image/jpeg', 'image/gif'],
        'video' => ['video/mp4'],
        'zip' => ['application/zip'],
        'ppt' => ['application/vnd.openxmlformats-officedocument.presentationml.presentation']
    ];
    public function __construct()
    {
    }
    public function getFilter()
    {
        $tags = Document::getTags();
        return compact('tags');
    }
    public function index(Request $request)
    {
        $query = Document::when(array_key_exists($request->type, self::FILETYPES), function ($query) use ($request) {
            $query->whereIn('mime', self::FILETYPES[$request->type]);
        })
            ->when(!empty($request->search), function ($query) use ($request) {
                $query->where('name', 'ilike', "%{$request->search}%");
            })
            ->when(isset($request->tag), function ($query) use ($request) {
                $query->whereHas('tags', function ($query) use ($request) {
                    $query->where('tags.id', $request->tag);
                });
            })
            ->where(function ($query) {
                $query->where('type_id', 1)
                    ->when(auth()->check(), function ($query) {
                        $query->orWhere('type_id', 2);
                    });
            });
        if (!auth()->check()) {
            $query->where(function ($query) {
                $query->where('issuance', 1);
            });
        }
        $data = $query
            ->paginate(10);
        $data->append('image');
        return $data;
    }
}
