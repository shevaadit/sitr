<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Models\RegencyGeometry;
use App\Models\LayerGroup;
use App\Models\Regency;
use App\Models\Layer;
use App\Models\Geometry;
use App\Models\LayerLegend;
use App\Models\Progress;
use App\Models\ProgressStatus;
use App\Models\ProgressSubmission;
use App\Http\Controllers\Controller;

class ProgressRegionalController extends Controller
{
    //
    private const LAYER_GROUP_ID = 2;
    private $layer;

    public function __construct()
    {
      //its just a dummy data object.
    //   dd(!auth()->check());
        // $this->middleware('auth');
        $this->layer = Layer::where('layer_group_id', self::LAYER_GROUP_ID)
            ->orderBy('created_at')
            ->first();
    }
    public function index() {
        return [
            'layer' => $this->layer,
            'group' => $this->layer->group
        ];
    }
    public function progress() {
        $legends = ProgressStatus::progressable()
            ->regional()
            ->orderBy('id', 'asc')
            ->get();
        $total = Progress::where('type_id', 'regional')->count();
        $counts = [];
        foreach ($legends as $key => $value) {
            $counts[$key] = Progress::where('type_id', 'regional')
                ->where('last_progress_status_id', $value->id)
                ->count();
        }
        return compact('legends', 'counts', 'total');
    }
    public function getRegionByStatus($id) {
        return Progress::select('id', 'region_id', 'region_type')
            ->has('regency')
            ->where('type_id', 'regional')
            ->where('last_progress_status_id', $id)
            ->with('regency')
            ->get();
    }
    public function getRegencies() {
        return Regency::select("*")->selectRaw('true as is_showing')->get();
    }
    public function getGroups() {
        return LayerGroup::select("*")->selectRaw('true as is_showing')->get();
    }
    public function getAttributes($id) {
        $progress = Progress::findOrFail($id);
        $listData = [];
        $listData[] = [
            'name' => $progress->typeRegion->name,
            'value' => $progress->region->name
        ];
        $listData[] = [
            'name' => 'Skor',
            'value' => $progress->lastStatus->name
        ];
        return $listData;
    }
    public function getHistories($id) {
        $progress = Progress::findOrFail($id);
        $histories = ProgressSubmission::approved()
            ->where('progress_id', $id)
            ->with(['documents' => function($query) {
                $query->select('id', 'name', 'progress_submission_id')
                    ->when(auth()->check(), function($query) {
                        $query->selectRaw("file as file_url");
                    })
                    ->when(auth()->check() === false, function($query) {
                        $query->selectRaw("'#' as file_url");
                    });
            }])
            ->with('progressStatus:id,name')
            ->get();
        return $histories;
    }
}
