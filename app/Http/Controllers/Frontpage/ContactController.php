<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SystemProperties;

class ContactController extends Controller
{
    //
    public function index(Request $request) {
        $content = SystemProperties::where('key', 'contact_page')
            ->selectRaw("value->>'content' as content")
            ->value('content');
        // $content = html_entity_decode($content);
        return view('contact', compact('content'));
    }
}
