<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gallery;

class GalleryImageController extends Controller
{
    //
    public function index(Request $request)
    {
        $galleries = Gallery::where('is_published', 1)
            ->where('file_type', Gallery::FILE_TYPE_IMAGE)
            // ->where(function ($query) {
            //     $query->where('file_type', Gallery::MEDIA_TYPE_LINK)
            //         ->orWhere(function ($query) {
            //             $query->has('media', 1)
            //                 ->where('file_type', Gallery::MEDIA_TYPE_FILE);
            //         });
            // })
            ->get();
        // $multiGalleries = Gallery::where('is_published', 1)
        //     ->where('media_type', Gallery::MEDIA_TYPE_FILE)
        //     ->has('media', '>', 1)
        //     ->get();
        return view('gallery-image', compact('galleries'));
    }
}
