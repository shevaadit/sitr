<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gallery;

class GalleryVideoController extends Controller
{
    //
    public function index(Request $request) {
        $galleries = Gallery::where('is_published', 1)
            ->where('file_type', Gallery::FILE_TYPE_VIDEO)
            ->get();
        return view('gallery-video', compact('galleries'));
    }
}
