<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Models\Progress;
use App\Models\Regency;
use App\Http\Controllers\Controller;

class ProgressController extends Controller
{
    //

    public function getFeature(Request $request, $x, $y) {
        $srid = config('webgis.srid');

        $point = "ST_GeomFromText('POINT (".$x." ".$y.")',".$srid.")";
        $distance = 'ST_Distance(geometry, '.$point.') as distance';
        $selectable[] = 'id';
        // $selectable[] = 'layer_id';
        $selectable[] = \DB::raw('ST_AsGeoJSON(geometry) AS geometry');;
        $selectable[] = \DB::raw($distance);
        $selectable[] = \DB::raw('ST_Area(geometry) as area');
        $groups = $request->groups ?: [];
        $data = Progress::select($selectable)
            ->when($request->view === 'filtered', function($query) use($request) {
                $query->whereIn('region_id', $request->regencies);
            })
            ->whereRaw("ST_DWithin(geometry, {$point}, 3000)")
            ->orderBy('distance')
            ->orderBy('area')
            ->first();
        $response = [
            'type' => 'Feature',
            'id' => null,
            'properties' => [],
            'geometry' => null
        ];

        if (!empty($data)) {
            $response['id'] = $data->id;
            $response['geometry'] = json_decode($data->geometry);
            // $response['properties']['group_name'] = $data->layerGroup->name;
        }
        
        return response()->json($response);
    }
}
