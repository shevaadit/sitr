<?php

namespace App\Http\Controllers\Frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Partner;
use App\Models\SystemProperties;

class HomeController extends Controller
{
    //
    public function index(Request $request)
    {
        $partners = Partner::where('is_published', 1)
            ->get();
        $latests = Post::with('creator:id,name')
            ->where('is_published', 1)
            ->orderBy('created_at', "desc")
            ->take(6)
            ->get();

        $appVersion = SystemProperties::select("value->version as val")
            ->where('key', SystemProperties::APPS_KEY)
            ->value('val');

        $appUrl = SystemProperties::select("value->url as val")
            ->where('key', SystemProperties::APPS_KEY)
            ->value('val');

        return view('home', compact('latests', 'partners', 'appVersion', 'appUrl'));
    }
}
