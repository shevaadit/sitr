<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Models\Progress;
use App\Models\Indicator;
use App\Models\IndicatorValue;
use App\Models\IndicatorConfig;
use App\Models\IndicatorDocument;
use App\Models\IndicatorConfigDetail;
use App\Http\Controllers\Controller;

class IndicatorController extends Controller
{
    private $indicator;

    public function __construct () {
        $this->indicator = Indicator::where('is_published', 1)
            ->orderBy('id')
            ->firstOrFail();
    }
    public function index() {
        $configs = IndicatorConfigDetail::where('indicator_config_id', $this->indicator->config_id)
            ->where('is_label', 0)
            ->get();
        $total = IndicatorValue::whereHas('config', function($query) {
                $query->where('is_label', 0); 
            })
            ->where('indicator_id', $this->indicator->id)
            ->sum(\DB::raw('value::decimal'));
        $progress = [];
        foreach ($configs as $item) {
            $value = IndicatorValue::whereHas('config', function($query) use($item) {
                $query->where('key', $item->key); 
            })
            ->where('indicator_id', $this->indicator->id)
            ->sum(\DB::raw('value::decimal'));

            $progress[] = [
                'name' => $item->name,
                'color' => $item->color,
                'value' => $value / $total * 100
            ];
        }

        $indicator = $this->indicator;
        $documents = IndicatorDocument::where('is_published', true)
            ->get();
        return compact('configs', 'progress', 'total', 'indicator', 'documents');
    }
    private function getLabels() {
        return IndicatorValue::whereHas('config', function($query) {
            $query->where('is_label', 1); 
        })
        ->where('indicator_id', $this->indicator->id)
        ->orderBy('value')
        ->get()
        ->pluck('value');
    }
    public function getChartData() {
        $viewName = $this->indicator->getLayerName();

        $configs = IndicatorConfigDetail::where('indicator_config_id', $this->indicator->config_id)
            ->where('is_label', 0)
            ->get();

        $labelConfig = IndicatorConfigDetail::where('indicator_config_id', $this->indicator->config_id)
            ->where('is_label', 1)
            ->firstOrFail();

        $labels = $this->getLabels();
        $datasets = [];

        foreach ($configs as $key => $config) {
            $field = "\"{$config->key}\"::decimal as value";
            $values = \DB::table($viewName)
                ->selectRaw($field)
                ->orderBy($labelConfig->key)
                ->get();

            $data = $values->pluck('value');

            $datasets[] = [
                'label' => $config->name,
                'barThickness' => 'flex',
                'minBarLength' => 10,
                'backgroundColor' => $config->color,
                'data' => $data
            ];
        }
        return compact('labels', 'datasets');
    }
    public function getChartDataByKey($key) {

        $values = IndicatorValue::whereHas('config', function($query) use($key) {
              $query->where('key', $key); 
            })
            ->where('indicator_id', $this->indicator->id)
            ->get();

        $config = IndicatorConfigDetail::where('indicator_config_id', $this->indicator->config_id)
            ->where('key', $key)
            ->firstOrFail();

        $data = $values->pluck('value');

        $labels = $this->getLabels();

        $datasets = [[
            'label' => $config->name,
            'backgroundColor' => $config->color,
            'data' => $data
        ]];
        // $datasets = [
        //     'datasets' => [$dataset]
        // ];
        return compact('labels', 'datasets');
    }
}