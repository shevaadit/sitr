<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegencyGeometry;
use App\Models\ZonationRule;
use App\Models\WebgisLayerGroup as LayerGroup;
use App\Models\Layer;
use App\Models\Geometry;
use App\Models\Regency;
use App\Models\SystemProperties;

class WebgisController extends Controller
{
    //
    private const LAYER_GROUP_IDS = [5, 22];
    private $layerIds;
    public function __construct()
    {
        $this->layerIds = Layer::whereIn('layer_group_id', self::LAYER_GROUP_IDS)
            ->pluck('id');
      //its just a dummy data object.
    //   dd(!auth()->check());
        // $this->middleware('auth');
    }
    public function index() {
        // $ids = implode(',', $this->layerIds);
        // $data = \DB::select("SELECT jsonb_build_object(
        //         'type',     'FeatureCollection',
        //         'features', jsonb_agg(feature)
        //     )
        //     FROM (
        //     SELECT jsonb_build_object(
        //         'type',       'Feature',
        //         'id',         id,
        //         'geometry',   ST_AsGeoJSON(geometry)::jsonb,
        //         'properties', to_jsonb(row) - 'geometry' - 'attributes'
        //     ) AS feature
        //     FROM (
        //         SELECT
        //             geometries.*, 
        //             geometries.attributes->>'SCORE_PROG' as score, 
        //             regencies.name as regency_name, 
        //             regencies.id as regency_id
        //         FROM geometries
        //         LEFT JOIN regencies
        //             ON regencies.id::int8 = geometries.morphable_id::int8
        //         WHERE geometries.layer_id = ?) row) features;"
        // , [ "({$ids})" ]);

        // return $data[0]->jsonb_build_object;
        $disclaimer = SystemProperties::select("value->content as val")
        ->where('key', 'DISCLAIMER-WEBGIS')
        ->value('val');
        return compact('disclaimer');
    }
    public function getRegencies() {
        return Regency::select("*")
            ->selectRaw('true as is_showing')
            ->selectRaw("null as groups")
            ->get();
    }
    public function getGroups() {
        return LayerGroup::selectWebgisFields()
            ->where('is_published', 1)
            ->where('level', 1)
            ->withCount('legends')
            ->orderBy('name')
            // ->whereIn('id', self::LAYER_GROUP_IDS)
            // ->with('children')
            ->get();
    }

    private function gettingRulesJoin(&$query) {
        return $query->join('layers as layer', 'geometries.layer_id', '=', 'layer.id')
            ->join('layer_groups as layer_group', 'layer.layer_group_id', '=', 'layer_group.id')
            ->join('zonation_rules as zonation_rule', 'layer_group.id', '=', 'zonation_rule.layer_group_id')
            ->join('zonation_rule_activities as zonation_rule_activity', 'zonation_rule.id', '=', 'zonation_rule_activity.zonation_rule_id');
    }
    // public function getRules(Request $request) {

    public function getZonationRules($id) {

        $geometry = Geometry::select('layer_id')
            ->with('layer:id,layer_group_id')
            ->findOrFail($id);

        $rules = ZonationRule::select('key')
            ->where('layer_group_id', $geometry->layer->layer_group_id)
            ->groupBy('key')
            ->get();

        $ruleKeys = $rules->pluck('key')->toArray();

        $selectable = [
            'zonation_rule_activity.id',
            'zonation_rule_activity.name',
            'zonation_rule_activity.rule'
        ];
        $selectableAttrs = [];
        $groupBys = [];

        foreach($ruleKeys as $index => $key) {
            $alias = $index === array_key_first($ruleKeys) ? 'value' : 'value_'.$index; 
            $selectableAttrs[] = "geometries.attributes->{$key} as {$alias}";
            $groupBys[] = "geometries.attributes->{$key}";
        }
        $query = Geometry::select(array_merge($selectable, $selectableAttrs))
            ->whereIn('geometries.id', [$id]);

        $this->gettingRulesJoin($query);

        $query->where(function($q) use($ruleKeys) {
            foreach($ruleKeys as $index => $key) {
                if ($index === array_key_first($ruleKeys)) {
                    $q->whereRaw("geometries.attributes->>'{$key}' ILIKE zonation_rule.value");
                } else {
                    $q->whereRaw("geometries.attributes->>'{$key}' ILIKE zonation_rule.value");
                }
            }            
        });
        $data = $query->groupBy(array_merge($selectable, $groupBys))
            ->get();
        // if (in_array($ruleKeys, array_keys($attributes))) {
        //     $value_active
        // }
        return compact('data', 'ruleKeys');
    }
    public function getAttributes($id) {
        $listData = \DB::select('select
                g.id,
                d.key,
                laa."name" ,
                d.value
            from
                layer_attributes laa
            join layers l on laa.layer_group_id = l.layer_group_id
            join geometries g on l.id = g.layer_id
            left join json_each_text(g."attributes") d on d.key = laa."key"
            where
                g.id = ? 
            order by
                1,
                2;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return $listData;
    }
    public function getRawAttributes($id) {
        $listData = \DB::select('select
                g.id,
                d.key as name,
                d.value
            from
                geometries g
            left join json_each_text(g."attributes") d on true
            where
                g.id = ? 
            order by
                1,
                2;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return $listData;
    }
    public function getDocuments($id) {
        $listData = \DB::select('select
                rd.*
            from
                regulation_documents rd
            join layers l on l.regulation_id = rd.regulation_id
            join geometries g on g.layer_id = l.id
            where
                g.id = ?;', [$id]);
        // $listData = array_fill(0, 100, $listData[0]);
        return $listData;
    }
    public function getFeature(Request $request, $x, $y) {
        $srid = config('webgis.srid');

        $point = "ST_GeomFromText('POINT (".$x." ".$y.")',".$srid.")";
        $distance = 'ST_Distance(geometry, '.$point.') as distance';
        $selectable[] = 'id';
        $selectable[] = 'layer_id';
        $selectable[] = \DB::raw('ST_AsGeoJSON(geometry) AS geometry');;
        $selectable[] = \DB::raw($distance);
        $selectable[] = \DB::raw('ST_Area(geometry) as area');
        $groups = $request->groups ?: [];
        $data = Geometry::select($selectable)
                ->when($request->view === 'filtered', function($query) use($request) {
                    $query->whereIn('morphable_id', $request->regencies);
                })
                ->whereHas('layer', function($query) use($groups) {
                    $query->whereIn('layer_group_id', $groups);
                })
                ->whereRaw("ST_DWithin(geometry, {$point}, 1)")
                ->orderBy('distance')
                ->orderBy('area')
                ->first();
        $response = [
            'type' => 'Feature',
            'id' => null,
            'properties' => [
                'group_name' => ''
            ],
            'geometry' => null
        ];

        if (!empty($data)) {
            $response['id'] = $data->id;
            $response['geometry'] = json_decode($data->geometry);
            $response['properties']['group_name'] = $data->layerGroup->name;
        }
        
        return response()->json($response);
    }
}
