<?php

namespace App\Http\Controllers;

use App\Models\LayerLegend;
use App\Models\LayerLegendRule;
use App\Models\LayerGroup;
use Illuminate\Http\Request;
use App\Helpers\Geoserver;
use App\Helpers\LayerGroupView;

class LayerLegendController extends MasterController
{
    private $helper;
    public function __construct () {
        parent::__construct(LayerLegend::class);
        $this->helper = new Geoserver();
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $group = LayerGroup::findOrFail($id);
            // $fields = $this->getFillableFields();
            // $fields['layer_group_id'] = $id;
            // $id = $this->storeModel($fields);

            $this->runDBTransaction(function() use($group) {
                $fields = $this->getFillableFields();
                $fields['layer_group_id'] = $group->id;
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $helper = new LayerGroupView($group);
                $helper->upsertViewQuery();
            });
            $this->helper->updateStyle($group);
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->with('rule:id,name')
            ->where('layer_group_id', $id)
            ->orderBy('min')
            ->get();
        $shapes = array_values(LayerLegend::SHAPES);
        $rules = LayerLegendRule::all();
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData', 'rules', 'shapes');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LayerLegend  $layerLegend
     * @return \Illuminate\Http\Response
     */
    public function show(LayerLegend $layerLegend)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LayerLegend  $layerLegend
     * @return \Illuminate\Http\Response
     */
    public function edit(LayerLegend $layerLegend)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LayerLegend  $layerLegend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LayerLegend $layerLegend)
    {
        //
        $this->model = $layerLegend;
        $this->request = $request;
        try {
            $group = LayerGroup::findOrFail($this->model->layer_group_id);
            $this->runDBTransaction(function() use($group) {
                $fields = $this->getFillableFields();
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $helper = new LayerGroupView($group);
                $helper->upsertViewQuery();
            });
            $this->helper->updateStyle($group);
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LayerLegend  $layerLegend
     * @return \Illuminate\Http\Response
     */
    public function destroy(LayerLegend $layerLegend)
    {
        //
        $this->model = $layerLegend;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
