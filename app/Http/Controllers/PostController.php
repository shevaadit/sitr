<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostType;
use Illuminate\Http\Request;

class PostController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    protected $indexable = [
        'id' => 'id',
        'title' => 'title',
        'is_published' => 'is_published',
    ];

    public function __construct () {
        parent::__construct(Post::class);
    }

    public function index()
    {
        //
        $query = $this->modelClass::select($this->getSelectable()); 
        
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate(new Post());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function upload(Request $request, &$fields) {

        if ( !$request->hasFile('file')) {
            throw new \Exception('File not found.');
        }

        $file           = $request->file("file");
        $filename       = $file->hashName();
        $filepath       = '/images/post/';

        $isUploaded     = $file->move(public_path($filepath), $filename);

        if ($isUploaded) {
            $fields['image'] = $filepath . $filename;
            return;
        }
        throw new \Exception('Upload File Failed.');
    }
    public function store(Request $request)
    {
        //
        $post = new Post();

        \DB::beginTransaction();
        try {
            $fields = $request->only($post->getFillable());
            $this->upload($request, $fields);
            $post->fill($fields);
            $post->saveOrFail();

            $post->attachTags($request->taglist, Post::TAGS_TYPE);
            $post->saveOrFail();

            \DB::commit();
            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function compilate(Post $formData = null) {
        $tags = Post::getTags();

        return compact('formData', 'tags');
    }
    public function show(Post $post)
    {
        //
        return $this->loadTags($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \App\Models\Post
     */
    private function loadTags(Post $post) {
        return $post->load("taglist");
    }
    public function edit(Post $post)
    {
        //
        return $this->compilate($this->loadTags($post));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        \DB::beginTransaction();
        try {
            $fields = $request->only($post->getFillable());
            $this->upload($request, $fields);
            $post->fill($fields);
            $post->saveOrFail();

            $post->syncTagsWithType($request->taglist, Post::TAGS_TYPE);
            $post->saveOrFail();

            \DB::commit();

            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        \DB::beginTransaction();
        try {
            $post->delete();
            \DB::commit();
            return $this->responseSuccess();
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->responseError($e);
        }
    }
}
