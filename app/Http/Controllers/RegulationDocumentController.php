<?php

namespace App\Http\Controllers;

use App\Models\Regulation;
use App\Models\RegulationDocument;
use Illuminate\Http\Request;

class RegulationDocumentController extends MasterController
{

    public function __construct () {
        parent::__construct(RegulationDocument::class);
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $header = Regulation::findOrFail($id);
            $this->runDBTransaction(function() use($header) {
                $fields = $this->getFillableFields();
                $fields['regulation_id'] = $header->id;

                $file           = $this->request->file("file");
                $filename       = $file->hashName();
                $filepath       = DIRECTORY_SEPARATOR.'regulation-document'.DIRECTORY_SEPARATOR;
                $isUploaded = $file->move(public_path($filepath), $filename);

                if (empty($isUploaded)) throw new \Exception("Error Saving File.");
                
                $fields['file'] = $filepath.$filename;
                $this->model->fill($fields);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->where('regulation_id', $id)
            ->get();
        // $listData = array_fill(0, 100, $listData[0]);
        return compact('listData');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegulationDocument  $regulationDocument
     * @return \Illuminate\Http\Response
     */
    public function show(RegulationDocument $regulationDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegulationDocument  $regulationDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(RegulationDocument $regulationDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegulationDocument  $regulationDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegulationDocument $regulationDocument)
    {
        //
        $this->model = $regulationDocument;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                if ($this->request->has('file')) {
                    $file           = $this->request->file("file");
                    $filename       = $file->hashName();
                    $filepath       = DIRECTORY_SEPARATOR.'regulation-document'.DIRECTORY_SEPARATOR;
                    $isUploaded = $file->move(public_path($filepath), $filename);

                    if (empty($isUploaded)) throw new \Exception("Error Saving File.");
                    $fields['file'] = $filepath.$filename;
                }
                $this->model->fill($fields);
                $this->model->saveOrFail();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegulationDocument  $regulationDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegulationDocument $regulationDocument)
    {
        //
        $this->model = $regulationDocument;
        try {
            return $this->runDBTransaction(function() {
                $file = $this->model->file;
                unlink(public_path($file));
                $this->model->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
