<?php

namespace App\Http\Controllers;

use App\Models\ProgressSubmission;
use Illuminate\Http\Request;

class ProgressApprovalController extends MasterController
{
    protected $indexable = [
        'id' => 'progress_submissions.id',
        'prev_progress_status_id' => 'progress_submissions.prev_progress_status_id',
        'progress_status_id' => 'progress_submissions.progress_status_id',
    ];
    public function __construct () {
        parent::__construct(ProgressSubmission::class);
    }

    
    public function compilate(ProgressSubmission $formData = null) {
        return compact('formData');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $data = $this->modelClass::select($this->getSelectable())
            ->with('progressStatus:id,name')
            ->has('progress')
            ->with('prevProgressStatus:id,name')
            ->with('progress')
            ->with('progress.regency')
            ->when($this->request->status !== 'all', function($query) {
                $query->where('status', $this->request->status);
            })
            ->paginate(10);
        return $data;
    }
    public function approve($id, Request $request)
    {
        $this->model = $this->modelClass::findOrFail($id);
        $this->request = $request;
        try {
            $this->model->status = 1;
            $this->model->saveOrFail();
            $this->model->progress->last_progress_status_id = $this->model->progress_status_id;
            $this->model->progress->saveOrFail();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    public function refuse($id, Request $request)
    {
        $this->model = $this->modelClass::findOrFail($id);
        $this->request = $request;
        try {
            $this->model->status = 2;
            $this->model->saveOrFail();
            $this->model->progress->last_progress_status_id = $this->model->prev_progress_status_id;
            $this->model->progress->saveOrFail();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgressSubmission  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProgressSubmission $layerCategory)
    {
        //
        return ['formData' => $layerCategory];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgressSubmission  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgressSubmission $layerCategory)
    {
        //
        return $this->compilate($layerCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgressSubmission  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgressSubmission $layerCategory)
    {
        //
        $this->model = $layerCategory;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgressSubmission  $layerCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgressSubmission $layerCategory)
    {
        //
        $this->model = $layerCategory;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
