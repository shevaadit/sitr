<?php

namespace App\Http\Controllers;

use App\Http\Dto\ProgressRegionalDto;
use App\Models\Progress;
use App\Models\ProgressSubmission;
use App\Models\User;
use App\Models\Layer;
use App\Models\Regency;
use App\Models\RegencyGeometry;
use App\Models\Geometry;
use App\Models\Regulation;
use App\Models\LayerGroup;
use Illuminate\Http\Request;

class ProgressRegionalController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function getRegions(Request $request) {
    //     if ($request->id === 'regional') {
    //         return $request->region_type::select('id', 'name')->get();
    //     } else if ($request->id === 'detailed') {
    //         return $request->region_type::select('id', 'name')
    //             ->where('regency_id', $request->regency_id)
    //             ->get();
    //     }
    //     return [];
    // }
    protected $indexable = [
        'id' => 'progress.id',
        'submission_person_name' => 'submission_person_name',
        'submission_person_position' => 'submission_person_position',
        'submission_date' => 'submission_date',
        'region_name' => 'region.name',
        'regulation_name' => 'regulation.name',
        'last_status_name' => 'last_status.name',
        // 'name' => 'name',
    ];
    public function getRegencies()
    {
        $query = Regency::accessable()
            ->select('id', 'name');
        $user = auth()->user(); 
        if ($user->level === 'admin') {
            $query->where('id', $user->region_id);
        }
        return $query->get();
    }
    public function __construct()
    {
        parent::__construct(ProgressRegionalDto::class);
    }

    protected function getFillableFields()
    {
        $fields = $this->request->only($this->model->getFillable());
        $fields['geometry'] = \DB::raw("ST_SetSRID(ST_Force2D(ST_GeomFromGeoJSON('" . $fields['geometry'] . "')), 4326)");
        $fields['type_id'] = 'regional';
        $fields['region_type'] = Regency::class;
        return $fields;
    }

    public function compilate(ProgressRegionalDto $formData = null)
    {
        $geojson = null;
        $groups = LayerGroup::select('id', 'name')->get();
        $regulations = Regulation::select('id', 'name')->get();
        $regionTypes = array_values(ProgressRegionalDto::REGION_TYPES);
        $types = array_values(ProgressRegionalDto::TYPES);
        if ($formData) {
            $geojson = $this->geojson($formData->id);
        }

        return array_filter(
            compact(
                'types',
                'geojson',
                'formData',
                'groups',
                'regulations',
                'regionTypes'
            )
        );
    }

    private function geojson($id)
    {
        $data = \DB::select(
            "SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            )
            FROM (
            SELECT jsonb_build_object(
                'type',       'Feature',
                'id',         id,
                'geometry',   ST_AsGeoJSON(geometry)::jsonb
            ) AS feature
            FROM (
                SELECT progress.* 
                FROM progress 
                WHERE progress.id = ?) row) features;",
            [$id]
        );

        return json_decode($data[0]->jsonb_build_object);
    }

    private function indexingJoin(&$query)
    {
        return $query->join('regulations as regulation', 'progress.regulation_id', '=', 'regulation.id')
            ->join('progress_statuses as last_status', 'progress.last_progress_status_id', '=', 'last_status.id')
            ->join('regency_formatteds as region', 'progress.region_id', '=', 'region.id');
    }
    public function index()
    {
        $query = $this->modelClass::regional()
            ->select($this->getSelectable());
        $user = auth()->user();
        if ($user->level === 'admin') {
            $query->where('region_id', $user->region_id);
        }
        $this->indexingJoin($query);
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        // $data->append('type');

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Dto\ProgressRegionalDto $progress
     * @return \Illuminate\Http\Response
     */
    public function show(ProgressRegionalDto $progress)
    {
        //
        $progress->load('region');
        $progress->load('regulation');
        return $this->compilate($progress);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Dto\ProgressRegionalDto $progress
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgressRegionalDto $progress)
    {
        //
        return $this->compilate($progress);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Dto\ProgressRegionalDto $progress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgressRegionalDto $progress)
    {
        //
        $this->model = $progress;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Dto\ProgressRegionalDto $progress
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgressRegionalDto $progress)
    {
        //
        try {
            $this->runDBTransaction(function () use ($progress) {
                ProgressSubmission::where('progress_id', $progress->id)
                    ->delete();
                $progress->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
