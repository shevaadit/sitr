<?php

namespace App\Http\Controllers;

use App\Models\IndicatorConfigDetail;
use App\Models\IndicatorConfig;
use Illuminate\Http\Request;
use App\Helpers\Geoserver;

class IndicatorConfigDetailController extends MasterController
{
    private $progressRanges = [
        '0 - 20' => [
            'lower' => 0,
            'upper' => 20,
            'opacity' => 0.5,
        ],
        '20 - 40' => [
            'lower' => 21,
            'upper' => 40,
            'opacity' => 0.7,
        ],
        '40 - 60' => [
            'lower' => 41,
            'upper' => 60,
            'opacity' => 0.8,
        ],
        '60 - 80' => [
            'lower' => 61,
            'upper' => 80,
            'opacity' => 0.9,
        ],
        '80 - 100' => [
            'lower' => 81,
            'upper' => 100,
            'opacity' => 1,
        ],
    ];
    private $helper;

    public function __construct () {
        parent::__construct(IndicatorConfigDetail::class);
        $this->helper = new Geoserver();
    }
    
    public function insert($id, Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $header = IndicatorConfig::findOrFail($id);
            $this->runDBTransaction(function() use($header) {
                $fields = $this->getFillableFields();
                $fields['indicator_config_id'] = $header->id;
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->helper->insertStyleIndicator($this->model);
                $this->helper->updateStyleIndicator($this->model, [
                    'ranges' => $this->progressRanges,
                    'config' => $this->model
                ]);
            });
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    
    public function fetch($id)
    {
        //
        $listData = $this->modelClass::select("*")
            ->selectRaw('false as _edited')
            ->where('indicator_config_id', $id)
            ->get();
        return compact('listData');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IndicatorConfigDetail  $indicatorConfigDetail
     * @return \Illuminate\Http\Response
     */
    public function show(IndicatorConfigDetail $indicatorConfigDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IndicatorConfigDetail  $indicatorConfigDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(IndicatorConfigDetail $indicatorConfigDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IndicatorConfigDetail  $indicatorConfigDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndicatorConfigDetail $indicatorConfigDetail)
    {
        //
        $this->model = $indicatorConfigDetail;
        try {
            $this->runDBTransaction(function() {
                $this->fillModel();
                $this->model->saveOrFail();
                $this->helper->updateStyleIndicator($this->model, [
                    'ranges' => $this->progressRanges,
                    'config' => $this->model
                ]);
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IndicatorConfigDetail  $indicatorConfigDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndicatorConfigDetail $indicatorConfigDetail)
    {
        //
        $this->model = $indicatorConfigDetail;
        try {
            $this->deleteModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
