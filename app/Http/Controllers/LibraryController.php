<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;

class LibraryController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $indexable = [
        'id' => 'id',
        'name' => 'name',
        'mime' => 'mime',
        'type_id' => 'type_id',
    ];
    public function __construct () {
        parent::__construct(Document::class);
    }

    protected function getFillableFields() {
        $fields = $this->request->only($this->model->getFillable());
        if ($this->request->hasFile('file')) {
            $file           = $this->request->file("file");
            $filename       = $file->hashName();
            $filepath       = DIRECTORY_SEPARATOR.'document'.DIRECTORY_SEPARATOR;
            $isUploaded = $file->move(public_path($filepath), $filename);

            if (empty($isUploaded)) throw new Exception("Error Saving File.");
            $fields['file'] = $filepath.$filename;
            $fields['mime'] = $isUploaded->getMimeType();
        }
        return $fields;
    }
    
    public function compilate(Document $formData = null) {
        $types = array_values(Document::TYPES);
        $tags = $this->modelClass::getTags();
        return array_filter(
            compact(
                'tags',
                'formData',
                'types',
            )
        );
    }

    public function index()
    {
        //
        // $sortMaps = [
        //     'regulation' => 'regulation_id',
        //     'region' => 'region_id'
        // ];
        $query = $this->modelClass::select($this->getSelectable()); 
        
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            $this->model->attachTags($request->taglist, $this->modelClass::TAGS_TYPE);
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
        return $document->append('type')
            ->load("taglist");;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    private function loadTags(Document $document) {
        return $document->load("taglist");
    }
    public function edit(Document $document)
    {
        //
        return $this->compilate($this->loadTags($document));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
        $this->model = $document;
        $this->request = $request;
        try {
            $this->runDBTransaction(function() {
                $fields = $this->getFillableFields();
                $this->model->fill($fields);
                $this->model->saveOrFail();
                $this->model->attachTags($this->request->taglist, $this->modelClass::TAGS_TYPE);
            });
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
        try {
            $this->runDBTransaction(function() use($document) {
                $this->deleteDirectory($document->file);
                $document->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
