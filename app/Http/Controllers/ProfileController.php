<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Http\Controllers\v2\MasterController as Controller;

class ProfileController extends Controller
{
    //
    public function __construct() {
        parent::__construct(User::class);
    }
    public function profile() {
        $data = $this->getUser()
            ->only('id', 'name', 'image', 'level');
        
        // if ($data['id'] === 1) $data['level'] = 'superadmin';

        return $data;
    }

    public function show()
    {
        $user = auth()->user();

        return $user;
    }

    protected function getFillable()
    {
        return [
            'name',
        ];
    }
    public function update(Request $request)
    {
        $this->model = auth()->user();
        try {
            $fields = $this->getFillableFields();
            $this->saveAvatarIfExists($fields);
            $this->updateModel($fields);

            return $this->responseSuccess();
        } catch (\Exception $e) {
            return $this->responseError($e);
        }
    }
    // public function update(Request $request) {
    //     $user = $this->getUser();
    //     \DB::beginTransaction();
    //     try {
    //         $user->fill($request->only('name'));
    //         $user->saveOrFail();
    //         \DB::commit();
    //         return $this->responseSuccess();
    //     } catch (\Exception $e) {
    //         \DB::rollback();
    //         return $this->responseError($e);
    //     }
    // }
    public function upload(Request $request) {

        if ( !$request->hasFile('image')) {
            return response()->json(['failed']);
        }
        

        $file           = $request->file("image");
        $filename       = $file->hashName();
        $filepath       = '/images/user/';

        $isUploaded     = $file->move(public_path($filepath), $filename);

        if ($isUploaded) {
            User::where("id", auth()->user()->id)
                ->update([
                    "image" => $filepath . $filename
                ]);
            return response([
                'path'   => $filepath . $filename,
            ]);
        }
    }
    private function saveAvatarIfExists(&$fields) {

        if ( !$this->request->hasFile('file')) {
            return;
        }

        $file           = $this->request->file("file");
        $filename       = $file->hashName();
        $filepath       = '/images/user/';
        $isUploaded     = $file->move(public_path($filepath), $filename);

        if ($isUploaded) {
            $fields['image'] = $filepath . $filename;
        }
    }
}
