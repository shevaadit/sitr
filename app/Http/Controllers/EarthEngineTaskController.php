<?php

namespace App\Http\Controllers;

use App\Models\EarthEngineTask;
use Illuminate\Http\Request;

class EarthEngineTaskController extends MasterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $indexable = [
        'id' => 'id',
        'code' => 'code',
        'category' => 'category',
        'filename' => 'filename',
        'type' => 'type',
        'preview_url' => 'preview_url',
        'status' => 'status',
    ];
    public function __construct () {
        parent::__construct(EarthEngineTask::class);
    }
    
    public function compilate(EarthEngineTask $formData = null) {
        return array_filter(
            compact(
                'formData'
            )
        );
    }

    public function index()
    {
        //
        // $sortMaps = [
        //     'regulation' => 'regulation_id',
        //     'region' => 'region_id'
        // ];
        $query = $this->modelClass::select($this->getSelectable()); 
        
        $this->searchByIndexable($query);
        $this->sortByIndexable($query);

        $data = $query->paginate(10);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->compilate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->model = new $this->modelClass();
        $this->request = $request;
        try {
            $id = $this->storeModel();
            return $this->responseSuccess('Success', compact('id'));
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EarthEngineTask  $earthEngineTask
     * @return \Illuminate\Http\Response
     */
    public function show(EarthEngineTask $earthEngineTask)
    {
        //
        return $earthEngineTask;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EarthEngineTask  $earthEngineTask
     * @return \Illuminate\Http\Response
     */
    private function loadTags(EarthEngineTask $earthEngineTask) {
        return $earthEngineTask->load("taglist");
    }
    public function edit(EarthEngineTask $earthEngineTask)
    {
        //
        return $this->compilate($earthEngineTask);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EarthEngineTask  $earthEngineTask
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EarthEngineTask $earthEngineTask)
    {
        //
        $this->model = $earthEngineTask;
        $this->request = $request;
        try {
            $this->updateModel();
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EarthEngineTask  $earthEngineTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(EarthEngineTask $earthEngineTask)
    {
        //
        try {
            $this->runDBTransaction(function() use($earthEngineTask) {
                $this->deleteDirectory($earthEngineTask->file);
                $earthEngineTask->delete();
            });
            return $this->responseSuccess();
        } catch (\Throwable $th) {
            return $this->responseError($th);
        }
    }
}
