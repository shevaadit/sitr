<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonationRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zonation_rules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('layer_group_id');
            $table->string('name');
            $table->string('key');
            $table->string('value');
            // $table->string('regulation_number');
            // $table->string('chapter');
            // $table->string('verse');
            // $table->string('point');
            // $table->string('content');
            // $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zonation_rules');
    }
}
