<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Grammars\PostgresGrammar as Grammar;

class CreateLayerLegendRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Grammar::macro('typeXml', function () {
            return 'xml';
        });
        Schema::create('layer_legend_rules', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_range')->default(false);
            // $table->string('type');
            // $table->addColumn('xml', 'value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layer_legend_rules');
    }
}
