<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRegulationIdToZonationRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zonation_rules', function (Blueprint $table) {
            //
            $table->foreignId('regulation_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zonation_rules', function (Blueprint $table) {
            //
            $table->dropColumn('regulation_id');
        });
    }
}
