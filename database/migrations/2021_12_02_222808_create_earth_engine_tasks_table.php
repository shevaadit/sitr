<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEarthEngineTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earth_engine_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('filename')->nullable();
            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->string('preview_url')->nullable();
            $table->string('status')->nullable();
            $table->foreignId('created_by');
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earth_engine_tasks');
    }
}
