<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShapeToLayerLegendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layer_legends', function (Blueprint $table) {
            //
            $table->string('shape_id', 24)->default('polygon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layer_legends', function (Blueprint $table) {
            //
            $table->dropColumn('polygon');
        });
    }
}
