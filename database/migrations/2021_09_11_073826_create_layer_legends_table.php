<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayerLegendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layer_legends', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('layer_group_id');
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->string('key');
            $table->string('color')->default('#000000');
            $table->string('stroke')->default('#FFFFFF');
            $table->string('shape_id', 24)->default('polygon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layer_legends');
    }
}
