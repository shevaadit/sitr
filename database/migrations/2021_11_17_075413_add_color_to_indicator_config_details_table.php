<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorToIndicatorConfigDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('indicator_config_details', function (Blueprint $table) {
            //
            $table->string('color', 10)->default('#EEEEEE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('indicator_config_details', function (Blueprint $table) {
            //
            $table->dropColumn('color');
        });
    }
}
