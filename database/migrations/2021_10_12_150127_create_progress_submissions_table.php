<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgressSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_submissions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('progress_id');
            $table->foreignId('progress_status_id');
            $table->foreignId('prev_progress_status_id');
            $table->string('submission_person_name');
            $table->string('submission_person_position');
            $table->tinyInteger('status')->default(0);
            $table->text('note')->nullable();
            $table->text('message')->nullable();
            $table->date('checked_date')->nullable();
            $table->foreignId('checked_by')->nullable();
            $table->foreignId('created_by');
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progress_submissions');
    }
}
