<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use ParseCsv\Csv;

class ProgressDetailedStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public static $path = __DIR__.'/../../data/progress_detailed_statuses.csv';

    public static function getCsvData($path = '')
    {
        $csv = new Csv();
        $csv->auto($path);

        return $csv->data;
    }
    public static function getData() {
        return self::getCsvData(self::$path);
    }
    public function run()
    {
        //
        \DB::table('progress_statuses')->insert(self::getData());
    }
}
