<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use ParseCsv\Csv;

class SystemPropertiesSeeder extends Seeder
{
    public static $path = __DIR__.'/../../data/system_properties.csv';

    public static function getCsvData($path = '')
    {
        $csv = new Csv();
        $csv->auto($path);

        return $csv->data;
    }
    public static function getData() {
        return self::getCsvData(self::$path);
    }
    public function run()
    {
        //
        \DB::table('system_properties')->insert(self::getData());
    }
}
