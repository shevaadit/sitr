<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class IndicatorConfigSeeder extends Seeder
{
    public function run()
    {
        //
        try {
            \DB::table('indicator_configs')->insert([['id' => 1, 'name' => 'Indikator 2020', 'created_by' => 1]]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
