<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use ParseCsv\Csv;

class IndicatorConfigDetailSeeder extends Seeder
{

    public static $path = __DIR__.'/../../data/indicator_config_details.csv';

    public static function getCsvData($path = '')
    {
        $csv = new Csv();
        $csv->auto($path);

        return $csv->data;
    }
    public static function getData() {
        return self::getCsvData(self::$path);
    }
    public function run()
    {
        //
        try {
            \DB::table('indicator_config_details')->insert(self::getData());
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
